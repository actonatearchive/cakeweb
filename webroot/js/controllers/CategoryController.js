(function() {
    var app = angular.module("cakeweb");

    app.controller("CategoryController", ['$scope','$http','$timeout', function($scope,$http,$timeout) {

        console.log("CategoryController");
        var pagination = {
            offset: 16,
            limit: 16,
            page_count: 2,
        };
        $scope.categories = [];
        $scope.page = {
            disabled: false,
            wait_for_response: true
        };

        $scope.base_url = baseUrl;

        $scope.getNextPage = function()
        {
            if ($scope.page.wait_for_response === false) {
                console.log("WAIT_FOR_RESPONSE");
                return 0;
            }
            var _cat_id = document.getElementById('category_id').value;
            var page_type = document.getElementById('page_type').value;
            var options = {};
            
            if (page_type === 'category') {
                var sort_by = document.getElementById('sort_by').value;
                options = {
                    category_id: _cat_id,
                    offset: pagination.offset,
                    limit: pagination.limit,
                    type: page_type,
                    sort_by: sort_by
                };    
            } else {
                options = {
                    category_id: _cat_id,
                    offset: pagination.offset,
                    limit: pagination.limit,
                    type: page_type
                };    
            }
            $scope.page.wait_for_response = false;
            console.log("BEFORE wait_for_response: ", $scope.page.wait_for_response);
            console.log("BEFORE Pagination: ",pagination);

            RequestPost(baseUrl + 'items/getNextPage', options)
                .then(function(resp) {
                    $scope.page.wait_for_response = true;//

                    console.log("ResponseData: ", resp.data.length);
                    if (resp.data.length < pagination.limit) {
                        $scope.page.disabled = true;
                        console.log("Pagination disabled.");
                    }
                    resp.data.forEach(function(row) {
                        $scope.categories.push(row);
                    });

                    $scope.$apply();
                    //Calculate it For Next Call
                    pagination.offset = pagination.page_count * pagination.limit;
                    pagination.page_count ++;
                    console.log("RESPONSE: ",resp);
                    console.log("AFTER wait_for_response: ", $scope.page.wait_for_response);

                    console.log("AFTER Pagination: ", pagination);
                });
        };

    }]);
})();
