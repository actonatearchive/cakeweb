(function() {
    var app = angular.module("cakeweb");

    app.controller("ProductController", ['$scope','$http','$timeout', function($scope,$http,$timeout) {
        console.log('ProductController');

        var addons_obj = [];


        $scope.toggleAddOn = function(item_id)
        {
            console.log("ItemID: ",item_id);
            var chk = checkItemIdExists(item_id);
            console.log("ItemExists: ",chk);
            if (chk === true) {
                console.log("Checked True; Removed From Array.");
                //Removed.
                $('#addon_'+item_id).removeClass('active');
                console.log("RemoveClass:", addons_obj);
            } else {
                //Add it to Array
                addons_obj.push({item_id: item_id});

                $('#addon_'+item_id).addClass('active');
                console.log("AddClass:", addons_obj);

            }
        };


        function checkItemIdExists(item_id)
        {
            var flag = false;
            try {
                var i = 0;
                addons_obj.forEach(function(row) {
                    if (item_id === row.item_id) {
                        flag = true;
                        addons_obj.splice(i,1);
                        throw 'STOP_IT';
                    }
                    i++;
                });
            }
            catch(e) {
                console.log("Execp:",e);
                return flag;
            }
            return flag;
        }

        /**
        *   Add Addon
        *
        */
        $scope.addAddon = function()
        {
            //If More than 1 item selected then and then addAddon
            if (addons_obj && addons_obj.length > 0) {
                var options = {
                    items: addons_obj,
                    addon: '1'
                };

                RequestPost(baseUrl + 'users/add_addon', options)
                    .then(function(resp) {
                        console.log("RESPONSE BACKED!", resp);
                        if (resp && resp.status === 'success')
                        {
                            showSuccessMessage('Addons added.');
                            goToUrl(baseUrl + 'users/cart');
                        }
                    })
                    .catch(function(err) {
                        console.log("Error: ",err);

                    });
                console.log("AddOn Added.");
            }

        };
    }]);
})();
