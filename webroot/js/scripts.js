

$(document).ready(function(){
	$('#nav-btn').click(function(){
		$(this).toggleClass('open');
	});
    console.log("Document Ready!!");

    $('.nav-link').click(function(){
        $('.collapse').collapse('hide');
    });

    $('.cs-prolist_col').addClass('animated zoomIn');
});

$(document).on('click', '#protabs a', function(e) {
    e.preventDefault();
    console.log("Protab clicked- ", this);
    $(this).tab('show');

});
$(document).on('click', '#addonstabs a', function(e) {
    e.preventDefault();
    $(this).tab('show');
});
