(function () {

    var app = angular.module("cakeweb", ['infinite-scroll']);

    // console.clear();
    console.log("APP JS Loaded.");
})();



$(document).ready(function () {
    // post();

    /**
    *   Product Detail Page
    */
    $('.datepicker').datepicker({
        forceParse: true,
        format: "dd-mm-yyyy",
        todayHighlight: true,
        autoclose: true
    });

    setTimeout(function () {
        $('.message').fadeOut(5000); //7 sec
    }, 100);
    /**
    *
    *
    */
    $('.jq-add-to-cart').click(function () {
        ele = $(this);

        // console.log("addToCart called()");
        $('#atc-item-id').val(ele.attr('data-id'));
        $('#atc-form').submit();
    });

    /**
    *   Drawer Menu
    *
    */
    // $('.drawermenu').drawermenu({
    //     speed: 100,
    //     position: 'left'
    // });

    /**
    *   Simple Zoom
    */
    // $('[data-spzoom]').spzoom({
    //     // <a href="http://www.jqueryscript.net/zoom/">Zoom</a> window width in pixels
    //     width: 350,
    //     // Zoom window height in pixels
    //     height: 350,
    //     // top, right, bottom, left
    //     position: 'right',
    //     // Zoom window margin (space)
    //     margin: 20,
    //     // Whether to display image title
    //     showTitle: true,
    //     // top, bottom
    //     titlePosition: 'bottom'
    // });

    // footer links turn into accordion
    jQuery('.jq-accordion').click(function () {
        jQuery(this).parent(".cs-footer_col").toggleClass("open");
        jQuery('html, body').animate({ scrollTop: jQuery(this).offset().top - 170 }, 500);
    });

    var aurl = window.location.href;
    if (aurl.indexOf('items/index') !== -1) {
        changeTimeLeft();

        /**
        *   Time Left
        *
        */
        setInterval(function () {
            changeTimeLeft();
        }, 100 * 10);
    }

});

function changeTimeLeft() {
    var base_hour = parseInt(document.getElementById('base_time_left').value); //

    var tmp_base_hour = parseInt(base_hour) - 1;
    var base_min = 60;//
    var base_sec = 60;//
    var d = new Date();
    var hour = d.getHours();
    if (hour < base_hour) {
        hour = (parseInt(tmp_base_hour - hour)).toString();
        var min = (base_min - parseInt(d.getMinutes())).toString();
        var sec = (base_sec - parseInt(d.getSeconds())).toString();

        // console.log("Hour: ", hour);
        document.getElementById('time_left').innerHTML = str_pad(2, hour, '0') + " : " + str_pad(2, min, '0') + " : " + str_pad(2, sec, '0');
    }
}

//Recursive Strategy
function str_pad(width, string, padding) {
    return (width <= (string.length)) ? string : str_pad(width, padding + string, padding);
}

//JS Functions
function setCity(_id) {
    var thisBaseUrl = baseUrl;
    if (_id && _id !== undefined) {
        var opt = {
            city_id: _id
        };
        console.log("Options: ", thisBaseUrl);
        RequestPost(thisBaseUrl + "home/set_city", opt)
            .then(function (resp) {
                console.log("[Response]:", resp);
                if (resp && resp.status === 'success') {
                    // console.log("BaseUrl: ", baseUrl);
                    goToUrl(baseUrl);

                }
            })
            .catch(function (err) {
                console.log("Oops!! Failed.", err);
            });
    }
}

function beforeSetCity() {
    var tmp = $('#selectCityDrop').val();
    if (tmp && tmp !== undefined || tmp !== '') {
        setCity(tmp);
    }
}

/**
*   Product Detail Page
*
*   Date: 29th Aug 2017
*/
function changeVariant(item_id, new_price, new_discount_price) {
    var before_item_id = document.getElementById('before_item_id').value;

    //Remove Active Class
    $('#variant_' + before_item_id).removeClass('selected');
    $('#variant_' + item_id).addClass('selected');

    document.getElementById('before_item_id').value = item_id;
    // console.log("BeforeItemId: ",before_item_id);
    // console.log("newItemId:", item_id);

    document.getElementById('item_price').innerHTML = new_price;
    document.getElementById('item_discount_price').innerHTML = new_discount_price;
    $('#add_to_cart').attr('data-id', item_id);
    $('#m_add_to_cart').attr('data-id', item_id);
    // console.log("Price:", new_price);
    // console.log("Discount Price:", new_discount_price);

}

//ShowDatePicker
function showDatePicker() {
    $('.datepicker').datepicker('show');
}

/**
*   Check Pincode
*   Date: 29th Aug 2017
*/
function checkPincode() {
    var pincode = document.getElementById('pincode').value;
    var item_id = document.getElementById('parent_item_id').value;

    var options = {
        item_id: item_id,
        pincode: pincode
    };
    RequestPost(baseUrl + 'api/pincode_check', options)
        .then(function (resp) {
            if (resp.status === 'success') {
                showSuccessMessage('Delivery to that pincode is available.');
            } else {
                showErrorMessage('Delivery to that pincode is not available.');
            }
            console.log("Response: ", resp);
        });
    // console.log("Pincode: ", pincode);
}
/**
*   check validation
*/
function checkValidation() {
    // console.log("checkValidation()");
    var delivery_date = $('#delivery_date').attr('value');
    // console.log("Delivery Date: ", delivery_date);
    if (delivery_date && delivery_date === "" || (delivery_date.trim()).length <= 2) {
        return showErrorMessage('Invalid delivery date.');
    }
}

/**
*   Check EggLess
*/
function checkEggLess(event) {
    var checkbox = event.target;
    if (checkbox.checked) {
        //Checkbox has been checked
        $('#base_egg_less').val(1);
    } else {
        //Checkbox has been unchecked
        $('#base_egg_less').val(0);
    }
}

/**
*   Check MidNight
*/
function checkMidNight(event) {
    var checkbox = event.target;
    if (checkbox.checked) {
        //Checkbox has been checked
        $('#base_midnight').val(1);
    } else {
        //Checkbox has been unchecked
        $('#base_midnight').val(0);
    }
}

/**
*   Change Delivery Date
*/
function changeDeliveryDate(event) {
    var dd = event.target;
    var date_val = $(dd).val();
    // console.log('date',date_val);
    // var date_arr = date_val.split('-');
    // var new_date_format = date_arr[2] + '-' + date_arr[1] + '-' + date_arr[0];
    $('#base_delivery_date').val(date_val);
}

/**
*   Change Message on Cake
*/
function changeMessageOnCake(event) {
    var moc = event.target;
    $('#base_message_cake').val($(moc).val());
}

/**
*   Change Message on Cake
*/
function changeOptionalMessage(event) {
    var om = event.target;
    $('#base_optional_message').val($(om).val());
}

/**
*   Check Delivery Address
*/
function checkDeliveryAddress(e) {
    // e.preventDefault();
    // alert();
    var address = {
        fullname: '',
        mobile: '',
        address1: '',
        landmark: '',
        city: '',
        pincode: ''
    };

    address.fullname = document.getElementById('inputFullName').value;
    address.mobile = document.getElementById('d_mobile').value;
    address.address1 = document.getElementById('d_address1').value;
    address.address2 = document.getElementById('d_address2').value;
    address.landmark = document.getElementById('d_landmark').value;
    address.city = document.getElementById('d_city').value;
    address.pincode = document.getElementById('d_pincode').value;


    if (address.fullname && address.fullname === "" || (address.fullname.trim()).length <= 2) {
        document.getElementById('inputFullName').focus();
        // return false;
        return showErrorMessage("Too short Full Name.");
    }

    if (address.mobile && isNaN(address.mobile)) {
        document.getElementById('d_mobile').focus();
        return showErrorMessage("Mobile No. should be digit.");
    }

    if (address.mobile && address.mobile === "" || (address.mobile.trim()).length < 10 || (address.mobile.trim()).length > 15) {
        document.getElementById('d_mobile').focus();
        return showErrorMessage("Mobile No. should be 10 characters long.");
    }

    if (address.address1 && address.address1 === "" || (address.address1.trim()).length <= 5) {
        document.getElementById('d_address1').focus();
        return showErrorMessage("Too short Address.");
    }

    if (address.landmark && address.landmark === "" || (address.landmark.trim()).length <= 5) {
        document.getElementById('d_landmark').focus();
        return showErrorMessage("Too short Landmark.");
    }

    if (address.landmark && address.landmark === "" || (address.landmark.trim()).length > 30) {
        document.getElementById('d_landmark').focus();
        return showErrorMessage("Landmark should not be more than 30 characters long.");
    }

    if (address.city && address.city === "" || (address.city.trim()).length <= 2) {
        document.getElementById('d_city').focus();
        return showErrorMessage("Too short city name.");
    }

    if (address.pincode && address.pincode === "" || (address.pincode.trim()).length < 6 || (address.pincode.trim()).length > 6) {
        document.getElementById('d_pincode').focus();
        return showErrorMessage("Pincode must be 6 characters long.");
    }


    $('#del_fullname').val(address.fullname);
    $('#del_mobile').val(address.mobile);
    $('#del_line1').val(address.address1);
    $('#del_landmark').val(address.landmark);
    $('#del_city').val(address.city);
    $('#del_pincode').val(address.pincode);

    // console.log("Object: ",address);
}

function checkSenderDetails() {
    var address = {
        fullname: '',
        mobile: '',
        city: '',
        username: ''
    };
    address.fullname = document.getElementById('p_fullname').value;
    address.mobile = document.getElementById('p_mobile').value;
    address.city = document.getElementById('p_city').value;
    address.username = document.getElementById('p_username').value;

    // console.log("SenderDetails: ", address);

    if (address.fullname && address.fullname === "" || (address.fullname.trim()).length <= 2) {
        document.getElementById('p_fullname').focus();
        showErrorMessage("Too short Full Name.");
        return false;
    }

    if (address.mobile && isNaN(address.mobile)) {
        document.getElementById('p_mobile').focus();
        showErrorMessage("Mobile No. should be digit.");
        return false;
    }

    if (address.mobile && address.mobile === "" || (address.mobile.trim()).length < 10 || (address.mobile.trim()).length > 15) {
        document.getElementById('p_mobile').focus();
        showErrorMessage("Mobile No. should be 10 characters long.");
        return false;
    }

    if (address.city && address.city === "" || (address.city.trim()).length <= 2) {
        document.getElementById('p_city').focus();
        return showErrorMessage("Too short city name.");
    }

}

function changePassword() {
    var pass = $('#jq-password').val();
    var newpass = $('#jq-newpassword').val();
    var retypepass = $('#jq-retypepassword').val();

    if (newpass == retypepass) {
        $.post(baseUrl + "users/changeUserPassword",
            {
                password: pass,
                newpassword: newpass
            },
            function (data, status) {
                data = JSON.parse(data);
                if (data.status == "success") {
                    showSuccessMessage(data.message);
                    $('#changePassword').modal('toggle');
                    $('#jq-password').val('');
                    $('#jq-newpassword').val('');
                    $('#jq-retypepassword').val('');
                } else {
                    // $('#changePassword').effect("shake");
                    showErrorMessage(data.message);
                }
            });
    } else {
        // $('#changePassword').effect("shake");
        showErrorMessage("Both password should be same.");
    }
}