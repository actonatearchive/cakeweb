
    function select_bank(bank) {
      $("#netbanking_selection").val(bank);
      //$('#save-netbanking').click();

    }


    $(function($) {

      $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

      $('.c-number').payment('formatCardNumber');
      $('.c-exp').payment('formatCardExpiry');
      $('.c-cvc').payment('formatCardCVC');
      $('.d-number').payment('formatCardNumber');
      $('.d-exp').payment('formatCardExpiry');
      $('.d-cvc').payment('formatCardCVC');

      $.fn.toggleInputError = function(erred) {
        this.parent('.form-group').toggleClass('has-error', erred);
        temp=this.parent('.form-group');
        if(erred)
          temp.children('.error-msg').show();
        else temp.children('.error-msg').hide();

        return this;

      };

      $('#save-cod').click(function() {
        $('#offline_pay_form').submit();
      });

      $('#save-netbanking').click(function(){
       $('#bankcode').val($('#netbanking_selection').val());
       $('#pg').val('NB');
       $('#store_card_token').val('');
        if($('#netbanking_selection').val()==""){
          $("#select-bank-error").show();
        }else{
          $("#select-bank-error").hide();
        $("#pay_now_form_id").submit();
        }

      });

      $('.save-card').click(function(e) {
        e.preventDefault();
        date="/";
        nameReg =  new RegExp("^[a-zA-Z ]+$");
        temp_c_type=$(this).attr("data");
        $('#store_card_token').val('');
        if(temp_c_type=="DC"){
          if($('#debit_card_select').val()==""){
            $("#debit-card-error").show();
            return;
          }
          $("#debit-card-error").hide();


          $("#ccnum").val($("#d-number").val().replace(/\s+/g, ""));



          $("#ccvv").val($("#d-cvc").val());
          var cardType = $.payment.cardType($('#d-number').val());
          $('.d-brand').text(cardType);
          $('.d-number').toggleInputError(!$.payment.validateCardNumber($('.d-number').val()));

          if(!nameReg.test($("#d-name").val()) || !$("#d-name").val().match(/\S/)){
            $("#d-name-error").show();
            return;
          }
          $("#d-name-error").hide();
          $("#ccname").val($("#d-name").val());

          $('.d-exp').toggleInputError(!$.payment.validateCardExpiry($('.d-exp').payment('cardExpiryVal')));
          $('.d-cvc').toggleInputError(!$.payment.validateCardCVC($('.d-cvc').val(), cardType));

          date=$('#d-exp').val().split("/");
          temp_cc_type = $("#debit_card_select").val();
          $('#cc-brand').val(temp_cc_type);
          $('#bankcode').val(temp_cc_type);
          if ($('#d-save').prop("checked")) {
          $("#store_card").val(1);
          }else{
            $("#store_card").val(0);

          }
        }else if(temp_c_type=="CC"){


          $("#ccnum").val($("#c-number").val().replace(/\s+/g, ""));
          $("#ccvv").val($("#c-cvc").val());

          var cardType = $.payment.cardType($('#c-number').val());
          $('.c-brand').text(cardType);

          $('.c-number').toggleInputError(!$.payment.validateCardNumber($('.c-number').val()));
          if(!nameReg.test($("#c-name").val()) || !$("#c-name").val().match(/\S/)){
            $("#c-name-error").show();
            return;
          }
          $("#c-name-error").hide();
          $("#ccname").val($("#c-name").val());

          $('.c-exp').toggleInputError(!$.payment.validateCardExpiry($('.c-exp').payment('cardExpiryVal')));
          $('.c-cvc').toggleInputError(!$.payment.validateCardCVC($('.c-cvc').val(), cardType));

          date=$('#c-exp').val().split("/");
          //temp_cc_type = $("input[name='credit_card_select']:checked").val();
          temp_cc_type='CC';
          if(cardType=='visa' || cardType=='mastercard' || cardType== 'amex' || cardType=='dinersclub'){
            temp_cc_type='CC';
          }else if(cardType== 'amex'){
            temp_cc_type='AMEX';
          }else if(cardType=='dinersclub'){
            temp_cc_type='DINR';
          }
          $('#cc-brand').val(temp_cc_type);
          $('#bankcode').val(temp_cc_type);
        //   if ($('#c-save').prop("checked")) {
        //   $("#store_card").val(1);
        //   }else{
        //     $("#store_card").val(0);
          //
        //   }
        }
        $('#pg').val(temp_c_type);


        $('#ccexpmon').val($.trim(date[0]));
        $('#ccexpyr').val("20"+$.trim(date[1]));
        $('.validation').removeClass('text-danger text-success');
        $('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');
        if($('.has-error').length==0){

          var formData = JSON.stringify($("#pay_now_form_id").serializeObject());


          $("#pay_now_form_id").submit();
        }else{
          //alert("Enter all valid details.");
        }
      });

      $('.credit_card_select').click(function(){
        $('#cc-brand').val($(this).val());
       // $('#bankcode').val($(this).val());
      });

      $('#debit_card_select').change(function(){
        $('#cc-brand').val($('#debit_card_select').val());
          $('#bankcode').val($('#debit_card_select').val());
      });

      //cardtoken
      var id="";
      $('.cards').click(function(){
        console.log("cards");
        id=$(this).attr('id');

        $('.scvv').hide();
        $('.select').css('color','black');
        $('.select').html('<i class="fa fa-circle-o" aria-hidden="true">');
        $('#'+id+' > .select').css('color','#009add');
        $('#'+id+' > .select').html('<i class="fa fa-check-circle" aria-hidden="true">');
        $('#'+id+' > .scvv').show();
        $('#cvv-'+id).payment('formatCardCVC');
        $('#card-token-submit').show();



      });

      $('#card-token-submit').click(function(){
        if($('#cvv-'+id).val()=="" ){
          alert("Enter CVV number.");
          return;
        }
        if($('#cvv-'+id).val().length<3 || $('#cvv-'+id).val().length>4){
          alert("Enter 3-4 digit cvv number");
          return;
        }
        $('#ccvv').val($('#cvv-'+id).val());
        $('#store_card_token').val($('#cvv-'+id).attr('data'));
        //alert($('#ccvv').val());
        $("#pay_now_form_id").submit();
      });

      $('.c-number').keyup(function(){

        var cardType = $.payment.cardType($('.c-number').val());
          $('.c-brand').text(cardType);
          if(cardType=='visa' || cardType=='mastercard' || cardType== 'amex' || cardType=='dinersclub'){
            $('#c-number-error').hide();
              $('#auto-card-detect').attr('class','');
              $('#auto-card-detect').addClass('icon-card-auto');
              $('#auto-card-detect').addClass('icon-'+cardType);
          }else{
            $('#c-number-error').show();
            $('#auto-card-detect').attr('class','');
          }

      });

      $('.d-number').keyup(function(){
          var cardType = $.payment.cardType($('.d-number').val());
          console.log("Card Type: ", cardType);
          var valCardType = "";
          if (cardType === 'visa') {
              valCardType = "VISA";
          }
          if (cardType === 'mastercard') {
              valCardType = "MAST";
          }
          if (cardType === 'maestro') {
              valCardType = "MAES";
          }

          if (cardType === 'rupay') {
              valCardType = "RUPAY";
          }

          if (cardType !== undefined && cardType !== null){
            $('#d-number-error').hide();
              $('#auto-debitcard-detect').attr('class','');
              $('#auto-debitcard-detect').addClass('icon-card-auto');
              $('#auto-debitcard-detect').addClass('icon-'+cardType);
              $('#debit_card_select').val(valCardType);
          } else {
            $('#d-number-error').show();
            $('#auto-debitcard-detect').attr('class','');
          }

      });

  });

  $(document).ready(function() {
      $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
          e.preventDefault();
          $(this).siblings('a.active').removeClass("active");
          $(this).addClass("active");
          var index = $(this).index();
          $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
          $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
      });
      $('#pay_by_paytm').click(function () {
        $('#pay_paytm_form_id').submit();
      });
      $('#pay_by_mobikwik').click(function () {
        $('#pay_mobikwik_form_id').submit();
      });

      $(".jq-use-ls-money").click(function () {
        $('.ls-show-loader').show();
        var checked = $(this).is(':checked');
        if(checked){
          //console.log(baseUrl+"payment/index/"+order_id+"/1");
          location.replace(baseUrl+"payment/index/"+order_id+"/1");
        } else {
          //console.log(baseUrl+"payment/index/"+order_id+"/0");
          location.replace(baseUrl+"payment/index/"+order_id+"/0");
        }
      });
  });
