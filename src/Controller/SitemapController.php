<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Sitemap
 * @package   Sitemap
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Sitemap Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Sitemap
 * @package  Sitemap
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.cakestudio.in/
 */
 class SitemapController extends AppController
 {
    public $components = ['Special','Query'];

     /**
     *   Initialization
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
    *
    *
    */
    public function index()
    {
        $this->viewBuilder()->layout('ajax');

        $cities = $this->Query->getData(
            'Cities',
            [],
            [
                'id',
                'name',
                'modified'
            ]
        );

        $categories = $this->Query->getData(
            'ItemCategories',
            [
                'ItemCategories.addon_category' => 0
            ],
            [
                'ItemCategories.id',
                'ItemCategories.url_slag',
                'ItemCategories.modified'
            ]
        );

        $item_cities = $this->Query->getData(
            'ItemCities',
            [],
            [
                'ItemCities.id',
                'ItemCities.modified',
                'Items.id',
                'Items.url_slag',
                'Cities.id',
                'Cities.name'
            ],
            ['Items.name'],
            false,
            [
                'Items',
                'Cities'
            ]
        );

        $this->set('cities', $cities);
        $this->set('categories', $categories);
        $this->set('item_cities', $item_cities);
    }

 }
