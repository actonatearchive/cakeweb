<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Checkout
 * @package   Checkout
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;

/**
 * Checkout Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Checkout
 * @package  Checkout
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */
 class CheckoutController extends AppController
 {
    public $components = ['Special','CakeWebCart','UserAddress','Query','Order','PayU'];

     /**
     *   Initialization
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(
            [
                'sucesss',
                'failure'
            ]
        );
    }

    /**
    *   Checkout
    *
    */
    public function index($order_id = null)
    {
        $this->viewBuilder()->layout("base_layout");
        if ($order_id != null) {

            $order = $this->Order->getOrderDetails($order_id);
            if ($order == null) {
                $this->Flash->error(__("Order is alredy placed. Please try again later."));
                return $this->redirect(
                    [
                        'controller'=>'users',
                        'action'=>'cart'
                    ]
                );  
            }
            // pr($order);die();   
            // $cart_items = $this->CakeWebCart->getUserCart(); //pass - user_id
            $cart_total = $this->CakeWebCart->getCartTotal(); //pass - user_id
    
            $user_addresses = [];
            if ($this->user_id != null) {
                //--getUserAddresses--
                $user_addresses = $this->UserAddress->getUserAddresses($this->user_id);
            }
            
            $this->loadComponent('UserWallet');
        $this->set('points',$this->UserWallet->getUserPoints($this->user_id));
            // $this->set('cart_items', $cart_items);
            $this->set('order', $order);
            $this->set('cart_total', $cart_total);
            $this->set('user_addresses', $user_addresses);
            
        }
    }

    /**
    *   Process
    *
    * @return redirect
    */
    public function process($use_points = false)
    {
        if ($this->user_id != null) {
            //Create New Order Here

            $data = [];
            $cart_total = $this->CakeWebCart->getCartTotal();
            $data['total_amount'] = $cart_total['sub_total'];
            $data['additional_charges'] = $cart_total['delivery_charges'];
            $data['eggless'] = $cart_total['eggless'];

            if ($use_points == 1) {
                $this->loadComponent('UserWallet');
                $data['discount_value'] = $this->UserWallet->getUserPoints($this->user_id);
            }
            $order = $this->Order->createOrder($this->user_id, $data);
            
            $this->log("Order Details:");
            $this->log($order);
            //And redirect Back to checkout @Address selection Page.

            return $this->redirect(['controller' => 'checkout', 'action' => 'index', $order['id']]);
        } else {
            $this->Flash->error(__("Please login before continue."));
            return $this->redirect(['controller'=> 'users', 'action'=>'cart']);
        }
    }

    /**
    *   Process PayU
    *
    * @return redirect
    */
    public function processPayU($order_id = null)
    {
        if ($order_id == null) {
            $this->Flash->error(__("PG Error occured. Please try again."));
            return $this->redirect(
                [
                  'controller'=>'home',
                  'action'=>'index'
                ]
            );
        }

        $this->loadComponent('Query');

        if ($this->request->is('post') ) {
            $data = $this->request->data;
            $this->log("ProcessPayU Data:");
            $this->log($data);

            $temp = $this->Query->getDataById(
                'Transactions',
                [
                    'code' => $data['txnid']
                ],
                [
                    'id',
                    'order_id'
                ]
            );

            $transaction['order_id'] = $temp['order_id'];
            $transaction['id'] = $temp['id'];
            $transaction['hash'] = $data['hash'];
            $transaction['transaction_ref'] = $data['mihpayid'];
            $transaction['payment_mode'] = $data['mode'];
            $transaction['payment_through'] = 'payu';

            $result = $this->PayU->response($data);

            if ($result == 'fail' ) {
                //TransactionFailure
                $transaction['status'] = 0;
                if ($this->Order->orderFailure($transaction)) {
                    //orderFailure
                    return $this->redirect(
                        [
                          'controller'=>'checkout',
                          'action'=>'failure',
                          $transaction['order_id']
                        ]
                    );
                }
            } else {
                //TransactionSuccess
                $transaction['status'] = 1;
                $transaction['order_hash'] = $result;

                if ($this->Order->orderSuccess($transaction)) {
                    return $this->redirect(
                          [
                          'controller'=>'checkout',
                          'action'=>'success',
                          $transaction['order_id']
                          ]
                      );
                } else {
                    //Send Email Maybe some error occured.
                    //OR ANY DEBUG MESSAGES GOES HERE
                    $this->log("ORDER_FAIL. Due to Some Unknown reasons.");
                }
            }
        }

        ///INVALID_METHOD
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        $this->Flash->error(__("Some error occured try again"));
        return $this->redirect(
            [
                'controller'=>'home',
                'action'=>'index'
              ]
        );
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    }
    
    public function success($order_id = null)
    {
        $this->viewBuilder()->layout("base_layout");
        
        $order = $this->Order->getOrderData($order_id);
        $this->set('order', $order);
    }


    public function failure($order_id = null)
    {
        $this->viewBuilder()->layout("base_layout");
        
        $order = $this->Order->getOrderData($order_id);
        $this->set('order', $order);
    }

    public function payment($order_id = null)
    {
        $this->viewBuilder()->layout("base_layout");
        if ($order_id != null) {

            $order = $this->Order->getOrderDetails($order_id);
            if ($order == null) {
                $this->Flash->error(__("Order is alredy placed. Please try again later."));
                return $this->redirect(
                    [
                        'controller'=>'users',
                        'action'=>'cart'
                    ]
                );  
            }
            // pr($order);die();   
            // $cart_items = $this->CakeWebCart->getUserCart(); //pass - user_id
            $cart_total = $this->CakeWebCart->getCartTotal(); //pass - user_id
            
            //Create Transaction
            $trans = [];
            //$trans['id'] = null;
            $trans['order_id'] = $order['id'];
            $trans['code'] = $this->Special->getOrderCode("T");

            //Making this object for Orders Table.
            $tmp_order['id'] = $order['id'];
            
            if ($save_trans = $this->Order->saveTransaction($trans)) {
                $tmp_order['transaction_id'] = $save_trans['id'];
                if ($this->Order->updateOrder($tmp_order) == false) {
                    $this->Flash->error(__("Unable to update your order details. Please try again later."));
                    return $this->redirect(
                        [
                            'controller'=>'users',
                            'action'=>'cart'
                        ]
                    );
                }
            } else {
                $this->Flash->error(__("Unable to create your transaction. Please try again later."));
                return $this->redirect(
                    [
                        'controller'=>'users',
                        'action'=>'cart'
                    ]
                );    
            }            


            //Make Payment Gateway Object here
            // pr($order);die();
            // //Paytm
            if ($order['grand_total'] > 0) {
                $this->loadComponent('Paytm');
 
                // for paytm
                $this->Paytm->params['ORDER_ID'] = $trans['code'];
                $this->Paytm->params['CUST_ID'] = $order['user_id'];
                $this->Paytm->params['TXN_AMOUNT'] = $order['grand_total'];
                $this->Paytm->params['EMAIL'] = $order['user']['username'];
                $this->Paytm->params['MOBILE_NO'] = $order['user']['mobile'];
                $this->Paytm->params['ORDER_DETAILS'] = $order['code'];
                $baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] &&
                !in_array(strtolower($_SERVER['HTTPS']), array('off','no')))
                ? 'https' : 'http';
                $baseUrl .= '://'.$_SERVER['HTTP_HOST'];
                $this->Paytm->params['CALLBACK_URL'] = $baseUrl . Router::url(
                    [
                      'controller' => 'checkout',
                      'action' => 'process-paytm',
                    ]
                );
                
                
                $paytmOutObj=$this->Paytm->process();
                $this->set('paytmOutObj', $paytmOutObj);
                // $log .= print_r($paytmOutObj, true);

            }

            // pr($paytmOutObj);die();

            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    
            // $this->set('cart_items', $cart_items);
            $this->set('order', $order);
            $this->set('cart_total', $cart_total);
            
        }
    }

    /**
     * Extra Functions
     * 0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-
     */


    /**
    *   Add/Update Address
    *
    * @return redirect
    */
    public function addNewAddress()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            if (isset($data['address_id']) && (empty($data['address_id']) || $data['address_id'] == '')) {
                unset($data['address_id']);
            }

            if (!isset($data['address_id'])) {
                $data['address_id'] = $this->Special->UUIDv4();
            }
            
            //User_ID
            $data['id'] = $data['address_id'];
            $data['user_id'] = $this->user_id;

            $new_address = $this->UserAddress->createAddress($data);

            // return $this->redirect(['controller' => 'checkout', 'action' => 'index','#'=>'address-tab']);
            return $this->redirect($this->Special->getSession('lastUrl'));
        }
    }

    /**
    *   Delete Address
    *
    * @return redirect
    */
    public function deleteAddress($address_id = null)
    {
        if ($address_id != null) {
            $address = [];
            $address['id'] = $address_id;
            $address['is_active'] = 0;

            $ret_data = $this->Query->setData('UserAddresses',$address);

            // return $this->redirect(['controller' => 'checkout', 'action' => 'index','#'=>'address-tab']);
            return $this->redirect($this->Special->getSession('lastUrl'));            
        }
    }


    /**
     *  Update Address
     * 
     * @return json
     */
    public function updateAddress()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            // pr($data);die();
            if($data['shipping_address_id']== null || $data['shipping_address_id'] == ''){
                
                $this->Flash->error(
                    __(
                        'Select Address.'
                    )
                );
                return $this->redirect($this->referer());            
                
            }
            $this->Order->updateOrder(
                [
                    'id' => $data['order_id'],
                    'shipping_address_id' => $data['shipping_address_id']
                ]
                );
            $this->loadComponent('UserWallet');
            
            if(isset($data['apply_points'])){
                $points = $this->UserWallet->getUserPoints($this->user_id);
                $this->Order->updateOrder(
                    [
                        'id' => $data['order_id'],
                        'discount_value' => $points,
                        'reward_points' => $points
                    ]
                );
            } else {
                $points = 0;
                $this->Order->updateOrder(
                    [
                        'id' => $data['order_id'],
                        'discount_value' => $points,
                        'reward_points' => $points
                    ]
                );
            }

            return $this->redirect(['controller'=>'checkout','action'=>'payment',$data['order_id']]);            


            // $res = new ResponseObject();
            // $res -> status = 'success' ;
            // $res -> message = 'Order successfully updated.' ;
            // $this -> response -> body(json_encode($res));
            // return $this -> response ;
        }
    }


    /**
     * Process Paytm Payment
     *
     * @return array
     */
    public function processPaytm()
    {

        $this->loadComponent('Paytm');
        $this->loadComponent('Order');
        $log = '';
       

        if ($this->request->is('post') ) {
            $data = $this->request->data;
             $this->log("response: ");
             $this->log($data);
            


            //$order = $this->Order->getOrder(["id"],
            //["code"=>$data['ORDERID']]);
            
            $temp = $this->Query->getDataById(
                'Transactions',
                [
                    'code' => $data['ORDERID']
                ],
                [
                    'id',
                    'order_id'
                ]
            );
            $result = $this->Paytm->response($data);
            $log = print_r($data, true);
            $log .= print_r($result, true);
            $log .= print_r($temp, true);
            
            if ($temp == null) {
                //email divya
                
                $this->set('code', $this->error_code);
                $this->set('message', 'Some error occured try again');
                $this->Flash->error(__("Some error occured try again"));
                return $this->redirect(
                    [
                    'controller'=>'home',
                    'action'=>'index'
                    ]
                );
            }
            $transaction['order_id'] = $temp['order_id'];
            $transaction['id'] = $temp['id'];
            $transaction['code'] = $data['ORDERID'];
            $transaction['hash'] = $data['CHECKSUMHASH'];
            $transaction['order_hash'] = null;
            $transaction['transaction_ref'] = $data['TXNID'];
            $transaction['payment_mode'] = $data['PAYMENTMODE'];
            $transaction['payment_through']='paytm';
            if ($result != 0 ) {
                $transaction['status'] = 1;
                $log .= print_r($transaction, true);
                if ($this->Order->orderSuccess($transaction) ) {
                    
                    return $this->redirect(
                        [
                        'controller'=>'checkout',
                        'action'=>'success',
                        $transaction['order_id']
                        ]
                    );
                } else {
                    //email divya
                   
                }
            } else {
                $transaction['status'] = 0;
                $log .= print_r($transaction, true);

                if ($this->Order->orderFailure($transaction) ) {
                    
                    return $this->redirect(
                        [
                        'controller'=>'checkout',
                        'action'=>'failure',
                        $transaction['order_id']
                        ]
                    );
                } else {
                    //email divya
                   
                }

            }

            return $this->redirect(
                [
                  'controller'=>'checkout',
                  'action'=>'failure',
                  $transaction['order_id']
                ]
            );
        }

        $this->set('code', $this->error_code);
        $this->set('message', 'Some error occured try again');
        $this->Flash->error(__("Some error occured try again"));
        return $this->redirect(
            [
                'controller'=>'home',
                'action'=>'index'
              ]
        );
    }
   

    /**
     * Process COD
     *
     * @param uuid $order_id Order Id
     *
     * @return array
     */
    public function processCod($order_id=null)
    {
        $this->loadComponent('Order');
        $log = '';
        if ($order_id == null) {
            //email divya
            $this->set('code', $this->error_code);
            $this->set('message', 'Some error occured try again');
            $this->Flash->error(__("Some error occured try again"));
            return $this->redirect(
                [
                  'controller'=>'home',
                  'action'=>'index'
                ]
            );
        }
        // $cod_config = $this->Special->getConfigs('cod');
        // if ($cod_config['cod_available'] == false ) {
        //     $this->Flash->error(__("Cash on delivery not available"));
        //     return $this->redirect($this->referer());
        // }

        $temp = $this->Order->getOrderDetails($order_id);

        if ($temp == null ) {
            $this->set('code', $this->error_code);
            $this->set('message', 'Some error occured try again');
            $this->Flash->error(__("Some error occured try again"));
            return $this->redirect(
                [
                'controller'=>'home',
                'action'=>'index'
                ]
            );
        }
        
        $transaction['order_id'] = $order_id;
        $transaction['id'] = $temp['transaction_id'];
        $transaction['hash'] = null;
        $transaction['transaction_ref'] = "COD";
        $transaction['payment_mode'] = "COD";
        $transaction['payment_through']='COD';
        $transaction['total_amount'] = $temp['total_amount'];
        if ($temp['grand_total'] == 0) {
            $transaction['transaction_ref'] = "CWCredits";
            $transaction['payment_mode'] = "CWCredits";
            $transaction['payment_through']='CWCredits';
        }

        $transaction['status'] = 1;
        $transaction['order_hash'] = null;
        $log .= print_r($temp, true);

        $log .= print_r($transaction, true);
        if ($this->Order->orderSuccess($transaction) ) {
            
            return $this->redirect(
                [
                'controller'=>'checkout',
                'action'=>'success',
                $transaction['order_id']
                  ]
            );
        } else {
              //email divya
              
              $this->set('code', $this->error_code);
              $this->set('message', 'Some error occured try again');
              $this->Flash->error(__("Some error occured try again"));
              return $this->redirect(
                  [
                    'controller'=>'home',
                    'action'=>'index'
                  ]
              );
        }

       
        $this->set('code', $this->error_code);
        $this->set('message', 'Some error occured try again');
        $this->Flash->error(__("Some error occured try again"));
        return $this->redirect(
            [
                'controller'=>'home',
                'action'=>'index'
              ]
        );
    }

}
?>
