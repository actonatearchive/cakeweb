<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $components = ['City','Special'];


    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Special');
        
         $this->loadComponent('Auth', [
                'authError' => "Please login to continue.",
                'loginAction' => [
                  'controller' => 'Users',
                  'action' => 'account',
                  '#'=>'signin',
                  'plugin' => false
                ],
                'loginRedirect' => [
                    'controller' => 'Users',
                    'action' => 'profile',
                    'plugin' => false
                ],
                'logoutRedirect' => [
                    'controller' => 'home',
                    'action' => 'index',
                    'plugin' => false
                ],
                'authenticate' => [
                    'Form' =>
                    [
                        'passwordHasher' => [
                            'className' => 'Fallback',
                            'hashers' => [
                                'Default',
                                'Weak' => ['hashType' => 'sha1']
                            ]
                        ],
                        'fields' => [
                            'username' => 'username',
                            'password' => 'password'
                        ]
                    ]
                ]
            ]);
        
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');


        if ($this->Special->isCookieExists('selectedCity')) {
            $city = $this->Special->getCookie('selectedCity');
            $this->selectedCity = $city; //saveselectedCity[This is Global Variable]
            $this->set('selectedCity', $city);
            if (!isset($city['id'])) {
                //if id is not present
                //Probably cookie hijacked by user
                return $this->redirect(['controller'=>'home','action'=>'city']);
            }
        } else {
            // $this->request->controller
            $url = Router::url(null, true); //complete url
            if (!preg_match(
                '/city'.
                '/i', $url)) {
                //If City is not setted.
                return $this->redirect(['controller'=>'home','action'=>'city']);
            }
        }
        // die('me');
        
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        // Note: These defaults are just to get started quickly with development
        // and should not be used in production. You should instead set "_serialize"
        // in each action as required.
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        $this->loadComponent('CakeWebCategory');
        
        
        

        $category_list=$this->CakeWebCategory->getCategoryList();
        $this->set('menu_list',$category_list);
        $this->loadComponent('CakeWebCart');

        //-===============SiteMap=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        $full_url = Router::url(null, true); //complete url
        if (preg_match(
            '/sitemap'.
            '/i', $full_url)) {

            $arr_city = $this->City->getCitiesData(
                [
                    'Cities.name LIKE' => '%vadodara%',
                    'Cities.disabled' => 0
                ],
                [
                    'id',
                    'name',
                    'zip_prefix',
                    'shipping_handling_charges'
                ]
            );
            //-------------------------------------------------------
            $city['id'] = $arr_city[0]['id'];
            $city['name'] = $arr_city[0]['name'];
            $city['zip_prefix'] = $arr_city[0]['zip_prefix'];
            $city['shipping_handling_charges'] = $arr_city[0]['shipping_handling_charges'];
            $this->Special->setCookie('selectedCity',$city);
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--

        $cart_items = $this->CakeWebCart->getUserCart();
        $this->set('cart_items', $cart_items);

        $cart_count = $this->CakeWebCart->getCartItemCount();
        $this->set('cart_item_count', $cart_count);

        $cart_total = $this->CakeWebCart->getCartTotal();
        $this->set('cart_total', $cart_total);

        
        
        //$baseUrl = $this->
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        $script_config = Configure::read('scripts_config');
        $this->set('script_configs', $script_config);
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        //Default PageTitle
        $this->set('page_title','Order Birthday, Wedding, Anniversary Cakes Online');
        //Default Meta Tags
        $this->set('meta_keywords', 'Online Cake Shop');
        $this->set('meta_description', 'Order birthday, wedding, anniversary cakes online.');

        
    }

    public function beforeFilter(Event $event)
    {
        if ($this->Auth->user()) {
            $this->user_id = $this->Auth->user('id');
            $this->user = $this->Auth->user();
            $this->set('user', $this->user);
        } else {
            $this->user_id = null;
            $this->user = null;
            $this->set('user', null);
        }

        $this->set('user_id', $this->user_id);


        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

        $server_config = Configure::read('server');
        if ($server_config['MAINTENANCE_MODE'] == true) {
            $this->request->trustProxy = true;
            $clientIp = $this->request->clientIp();

            foreach ($server_config['MAINTENANCE_MODE_ALLOWED_IPS'] as $key => $value) {
                if ($value != $clientIp) {
                    $flg = 1;
                } else {
                    $flg = 0;
                    break;
                }
            }
            if ($flg === 1) {
                echo html_entity_decode($server_config['MAINTENANCE_MODE_TEMPLATE']);
                die();
            }

        }
        //-===-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        // Refer URL
        $url = Router::url(null, true); //complete url

        if (!preg_match(
            '/login|logout|auth|register|socialLogin|account|images|signup|signin|forgot_password'.
            '|Address'.
            '|add-to-cart|remove-from-cart|resetPassword|changeQty|img/i',
            $url
        )) {
            $this->Special->setSession('lastUrl', $url);
        }


        // //================
        // // User Cart

        // $this->cartitem_count = $this->Cart->getCartItemCount();
        // $this->set('cartitem_count', $this->cartitem_count);


        // //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        // $script_config = Configure::read('scripts_config');
        // $this->set('script_configs', $script_config);
        // //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-        
    }
}


/**
 *   ResponseObject
 *
 * @category ResponseObject
 * @package  ResponseObject
 * @author   Mohammed Sufyan <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.sufyan.co.in/
 */
class ResponseObject
{
    var $data = array();
    var $status = "success";
    var $message = "";
}