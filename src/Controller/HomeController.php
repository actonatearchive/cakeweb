<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Home
 * @package   Home
 * @author    Divya Khanani <divya.khanani@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Home Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Home
 * @package  Home
 * @author   Divya Khanani <divya.khanani@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.cakestudio.in/
 */
 class HomeController extends AppController
 {
    public $components = ['City','Special','Query','CakeWebHome'];

     /**
     *   Initialization
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }


    /**
     * Index Method
     *
     * Used for Home Page View.
     *
     * @return void
     */
    public function index()
    {
        //
        // $items = $this->Item->getFeaturedItems();
        // $this->set('featured_items', $items);

        $this->viewBuilder()->layout("base_layout");
        // pr($this->selectedCity);die();

        $featured = $this->CakeWebHome->getFeaturedCakes('featured', $this->selectedCity['id']);
        // $featured_more = $this->CakeWebHome->getFeaturedCakes('Gift-&-more', $this->selectedCity['id']);
        
        $special = $this->CakeWebHome->getFeaturedCakes('special', $this->selectedCity['id']);
        $best_seller = $this->CakeWebHome->getFeaturedCakes('best_seller', $this->selectedCity['id']);

        $banners = $this->CakeWebHome->getBanners($this->selectedCity['id']);

        // $this->log("[HOMEPAGE] Hello World!");

        $this->set('featured', $featured);
        $this->set('special', $special);
        // pr($pre_cakes);die();
        $this->set('best_seller', $best_seller);
        // $this->set('featured_more', $featured_more);
        // $this->set('festival_cakes', $festival_cakes);
        // $this->set('gifts_cakes', $gifts_cakes);

        $this->set('banners', $banners);
        // $this->set('Item',"herr");
        

        $this->set('page_title', 'Order Birthday, Wedding, Anniversary Cakes Online');
        // $this->log("[HOMEPAGE] Hello World!");

        // die();
    }


    /**
    *   Refund Policy
    *
    * @return void
    */
    public function contact()
    {
        $this->viewBuilder()->layout("base_layout");
        $this->set('page_title', 'Terms And Conditions');
    }
    /**
     * City Method
     *
     * Used for Home Page View.
     *
     * @return void
     */
    public function city()
    {
        //Layout
        $this->viewBuilder()->layout("base_layout");

        //Component Methods
        $cities = $this->City->getAllActiveCities();
        // $trending_cities = $this->City->getTrendingCities();


        //Setters
        $this->set('cities', $cities);
        // $this->set('trending_cities', $cities);
        $this->set('page_title', 'Select city');
    }

    /**
    * Set City
    *
    * @return json
    */
    public function setCity()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $city = $this->City->getCityData($data['city_id'],
                [
                    'id',
                    'name',
                    'zip_prefix',
                    'shipping_handling_charges'
                ]
            );
            $this->Special->setCookie('selectedCity',$city);
            $tmp = $this->Special->getCookie('selectedCity');
            if (isset($tmp['id'])) {
                $res = new ResponseObject();
                $res -> status = 'success' ;
                $res -> data = $data;
                $res -> message = 'City setted.' ;
                $this -> response -> body(json_encode($res));
                return $this -> response ;
            } else {
                $res = new ResponseObject();
                $res -> status = 'error' ;
                $res -> message = 'Unable to set city.' ;
                $this -> response -> body(json_encode($res));
                return $this -> response ;
            }
        }
    }

    /**
    *   Catch City
    * @return redirect
    */
    public function catchCity($city_name = null)
    {
        //$this->Special->isCookieExists('selectedCity')
        if ($city_name != "") {
            // pr($city_name);die('s');
            $arr_city = $this->City->getCitiesData(
                [
                    'Cities.name LIKE' => '%'.$city_name.'%',
                    'Cities.disabled' => 0
                ],
                [
                    'id',
                    'name',
                    'zip_prefix',
                    'shipping_handling_charges'
                ]
            );

            $city = [];
            if ($arr_city) {
                $city['id'] = $arr_city[0]['id'];
                $city['name'] = $arr_city[0]['name'];
                $city['zip_prefix'] = $arr_city[0]['zip_prefix'];
                $city['shipping_handling_charges'] = $arr_city[0]['shipping_handling_charges'];
                $this->Special->setCookie('selectedCity',$city);
            }
            return $this->redirect(['controller' => 'home', 'action' => 'index']);
        } else {
            return $this->redirect($this->referer());
        }

    }

    public function contents($page = null)
    {
        $this->viewBuilder()->layout("base_layout");

        if ($page != null) {
            
            $content = $this->Query->getAllDataById('Contents',[
                'alias'=> $page
            ]);
            
            $this->set('content', $content);
            if ($content == null){
                $this->redirect(['action'=>'index','controller'=>'home']);
            
            }

        } else {

            $this->redirect(['action'=>'index','controller'=>'home']);
        }
        
    }


 }
