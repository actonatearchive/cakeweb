<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Items
 * @package   Items
 * @author    Divya Khanani <divya.khanani@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Items Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Items
 * @package  Items
 * @author   Divya Khanani <divya.khanani@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.cakeweb.in/
 */
 class ItemsController extends AppController
 {
    public $components = ['Special','CakeWebCategory'];

     /**
     *   Initialization
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();

    }

    /**
     * Category Method
     *
     * Used for Category View.
     *
     * @return void
     */
    public function category($category = null, $sub_cat = null)
    {
        $this->viewBuilder()->layout("base_layout");
        // pr($this->request->params);
        $sort_by = $this->request->getQuery('sort_by');

		if (empty($sort_by)) {
			$sort_by = "a_z";
        }

        $festivals = $this->CakeWebCategory->getFestivalCategories();

        if ($sub_cat == null) {

            if (empty($festivals)) {
                $sub_cat = 'valentine-cakes';
            } else {
                $sub_cat = $festivals[0]['url_slag'];
            }

        }
        if ($category == 'festival-cakes') {
            $categories = $this->CakeWebCategory->getCategory($sub_cat);
        } else {
            $categories = $this->CakeWebCategory->getCategory($category);
        }

        // pr($categories);
        // $pre_cakes = $this->CakeStudioHome->getFeaturedCakes('premium', $this->selectedCity['id']);
        // $birth_cakes = $this->CakeStudioHome->getFeaturedCakes('birthday', $this->selectedCity['id']);
        // $design_cakes = $this->CakeStudioHome->getFeaturedCakes('designer', $this->selectedCity['id']);

        $category_listing = $this->CakeWebCategory->getCategoryCakes($categories['id'], $this->selectedCity['id'], 16, 0, $sort_by);


        // pr($category_listing); die('ok');
        // $this->set('premium_cakes', $pre_cakes);
        // $this->set('birthday_cakes', $birth_cakes);
        // $this->set('designer_cakes', $design_cakes);
        $this->set('category', $categories);
        $this->set('category_listing', $category_listing);
        $this->set('url_slag', $category);
        $this->set('sub_cat', $sub_cat);
        $this->set('festivals', $festivals);
        $this->set('sort_by', $sort_by);

        // $this->set('banners', $banners);
        $this->set('page_title', $categories['name']." in ".$this->selectedCity['name']);

    }

    /**
    * Product Detail Page
    *
    * @return void
    */
    public function index($url_slag = null, $child_id = null)
    {
        $this->viewBuilder()->layout("base_layout");

        $item = $this->CakeWebCategory->getProductDetails($url_slag, $this->selectedCity['id']);
        // pr($item);

        //If All checks are passed then item will be viewable.
        //Checks
        //Item Found?
        if ($item == false) {
            $this->redirectMsg("That item is not found.");
        }

        //Is Item Disable?
        if ($item['is_disabled'] == 1 || $item['is_disabled'] == 2) {
            //Not Available
            $this->redirectMsg("That item is temporarily not available at your location.");
        }


        $temp_item = [];

        foreach ($item['child_items'] as $key => $value) {
            if ($value['variant_url'] == $child_id)
            {
                $temp_item['price'] = $value['price'];
                $temp_item['discount_price'] = $value['discount_price'];
                $temp_item['id'] = $value['id'];
                $temp_item['variant_name'] = $value['variant_name'];
            }
        }

        //If Variant URL Slag is not supplied.
        if (empty($temp_item)) {
            $temp_item['price'] = $item['child_items'][0]['price'];
            $temp_item['discount_price'] = $item['child_items'][0]['discount_price'];
            $temp_item['id'] = $item['child_items'][0]['id'];
            $temp_item['variant_name'] = $item['child_items'][0]['variant_name'];

        }

        $time_config = Configure::read('time');


        //MainItem
        $this->set('item', $item);
        $this->set('default_price', $temp_item['price']);
        $this->set('default_id', $temp_item['id']);
        $this->set('default_discount_price', $temp_item['discount_price']);
        // $this->set('today_time_remain', $time_config['TODAY_DELIVERY_TIME']);
        $this->set('today_time_remain', 19);
        

        //PageTitle
        $this->set('page_title', $item['name']." in ".$this->selectedCity['name']." (".$temp_item['variant_name'].")");
        $this->set('meta_keywords', $item['keyword']);
        $this->set('meta_description', $item['short_desc']);
    }

    /**
    *   Search Page
    *
    * @return void
    */
    public function search()
    {
        $data['search_name']="";
        $category_listing=[];
        if ($this->request->is('post')) {

            $data = $this->request->data;
            // pr($data);die();

            $category_listing = $this->CakeWebCategory->getSearchCakes(
                $data['search_name'], $this->selectedCity['id']
            );

            // $this->set('category', $categories);

            // $this->set('search', $data['search_name']);

            $this->set('page_title', 'Search results for  \''.$data['search_name'].' \'');
        }
        $this->set('search', $data['search_name']);
            $this->set('category_listing', $category_listing);
        
        // pr($data['search']);die();
        $this->viewBuilder()->layout("base_layout");
        

    }


    private function redirectMsg($msg = 'Oops! An error occured.')
    {
        $this->Flash->error(
            __(
                $msg
            )
        );
        return $this->redirect(['controller'=>'home','action'=>'index']);
    }


    /**
    *   Get Next Page
    *
    * @return json
    */
    public function getNextPage()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            if ($data['type'] == 'category') {
                $category_listing = $this->CakeWebCategory->getCategoryCakes($data['category_id'], $this->selectedCity['id'], $data['limit'], $data['offset'], $data['sort_by']);
            } else if($data['type'] == 'search') {
                $category_listing = $this->CakeWebCategory->getSearchCakes($data['category_id'], $this->selectedCity['id'], $data['limit'], $data['offset']);
            }

            if (sizeof($category_listing) > 0) {
                $res = new ResponseObject();
                $res -> status = 'success' ;
                $res -> data = $category_listing;
                $res -> message = 'Records found.' ;
                $this -> response -> body(json_encode($res));
                return $this -> response ;
            } else {
                $res = new ResponseObject();
                $res -> status = 'error' ;
                $res -> message = 'No more records found.' ;
                $this -> response -> body(json_encode($res));
                return $this -> response ;
            }

        } else {
            $res = new ResponseObject();
            $res -> status = 'error' ;
            $res -> message = 'Invalid Method.' ;
            $this -> response -> body(json_encode($res));
            return $this -> response ;
        }
    }

 }
