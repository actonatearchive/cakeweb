<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   CakeWebHome
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * Special Component
 *
 * @category Component
 * @package  CakeWebHome
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
class CakeWebHomeComponent extends Component
{
    //Load Other Components
    public $components = ['Special','Query'];


    /**
     *  Getter Method For Cakes
     *
     * @param $city_id uuid    City ID
     * @param $limit   integer Limit of records
     *
     * @return array
     */
    public function getFeaturedCakes($type = null, $city_id = null, $limit = 8) {
        $resultsArray = array();
        $item_img_model = "item/primary_photo/";
        $category_id = [];
        $this->child_city_id = $city_id;
        $condition = [
            'ItemCities.city_id' => $city_id,
            'Items.item_id is' => null,
            'Items.disabled' => 0,
            'Items.is_addon' => 0
        ];
        if ($type == 'featured') {
            $condition['Items.featured']=1;
        } else if ($type == 'special') {
            $condition['Items.special']=1;
        } else if ($type == 'best_seller') {
            $condition['Items.best_seller']=1;
        } else {
            $condition['Items.featured']=1;
            $condition['ItemCategories.url_slag']=$type;
            
        }


        $items = $this->Query->getData(
            'Items',
            $condition,
            [
                'Items.id',
                'Items.name',
                'Items.variant_name',
                'Items.primary_photo',
                'Items.primary_photo_directory',
                'Items.url_slag',
                'ItemCities.city_id',
                'ItemCities.price',
                'ItemCities.discount_price',
                'ItemCities.discount_percentage'

            ],
            [
                'Items.name'
            ],
            $limit,
            [
                'ItemCities',
                'ItemCategories',
                'ChildItems' => function($q) {
                    $q->select(
                        [
                           'ChildItems.id',
                           'ChildItems.name',
                           'ChildItems.variant_name',
                           'ChildItems.primary_photo',
                           'ChildItems.primary_photo_directory',
                           'ItemCities.id',
                           'ItemCities.city_id',
                           'ItemCities.item_id',
                           'ItemCities.price',
                           'ItemCities.discount_price',
                           'ItemCities.discount_percentage'
                        ]
                    );
                    $q->order('ChildItems.sequence ASC');

                    $q->where(
                        [
                            'ItemCities.city_id' => $this->child_city_id
                        ]
                    );

                    $q->contain(['ItemCities']);
                    return $q->autoFields(false);
                }
            ],
            false
        );

        // pr($items); //die();
        $resultsArray = array();
        foreach ($items as $key => $value) {
        $tmp = [];

        $tmp['id'] = $value['id'];
        $tmp['name'] = $value['name'];
        $tmp['url_slag'] = $value['url_slag'];

        $tmp['medium_image'] = $this->Special->getBaseImageUrl()
                            .$item_img_model
                            .$value['primary_photo_directory']
                            ."/medium_"
                            .$value['primary_photo'];

        // $tmp['price'] = $value['item_city']['price'];
        // $tmp['discount_price'] = $value['item_city']['discount_price'];
        // $tmp['discount_percentage'] = $value['item_city']['discount_percentage'];

        //Create childItems array;
        $tmp['child_items'] = array();
        $k = 0;

        // if ($value['item_city']['city_id'] == $this->child_city_id) {
        //     //First Child Item Clone Parent
        //     $tmp['child_items'][$k]['id'] = $value['id'];
        //     $tmp['child_items'][$k]['name'] = $value['name'];
        //     $tmp['child_items'][$k]['medium_image'] = $tmp['medium_image'];
        //     $tmp['child_items'][$k]['variant_name'] = $value['variant_name'];
        //     $tmp['child_items'][$k]['price'] = $value['item_city']['price'];
        //     $tmp['child_items'][$k]['discount_price'] = $value['item_city']['discount_price'];
        //     $tmp['child_items'][$k]['discount_percentage'] = $value['item_city']['discount_percentage'];
        //
        //     $variant_url = str_replace(" ","",$value['variant_name']);
        //     $variant_url = str_replace("/","-",$variant_url);
        //     $tmp['child_items'][$k]['variant_url'] = $variant_url;
        //
        //     $k++;
        // }

        if (sizeof($value['child_items']) > 0)
        {
            foreach ($value['child_items'] as $key2 => $val2) {

                if ($k == 0) {
                    $tmp['price'] = $val2['item_city']['price'];
                    $tmp['discount_price'] = $val2['item_city']['discount_price'];
                    $tmp['discount_percentage'] = $val2['item_city']['discount_percentage'];
                }

                $tmp['child_items'][$k]['id'] = $val2['id'];
                $tmp['child_items'][$k]['name'] = $val2['name'];
                $tmp['child_items'][$k]['variant_name'] = $val2['variant_name'];
                $tmp['child_items'][$k]['medium_image'] = $this->Special->getBaseImageUrl()
                                    .$item_img_model
                                    .$val2['primary_photo_directory']
                                    ."/medium_"
                                    .$val2['primary_photo'];

                $tmp['child_items'][$k]['price'] = $val2['item_city']['price'];
                $tmp['child_items'][$k]['discount_price'] = $val2['item_city']['discount_price'];
                $tmp['child_items'][$k]['discount_percentage'] = $val2['item_city']['discount_percentage'];

                $variant_url = str_replace(" ","",$val2['variant_name']);
                $variant_url = str_replace("/","-",$variant_url);
                $tmp['child_items'][$k]['variant_url'] = $variant_url;

                $k++;
            }
        }
        array_push($resultsArray, $tmp);
        }
        // pr($resultsArray); die();
        //OLD
        // pr($resultsArray);
        // pr($items); die();
        // $items = $this->Query->getData(
        //     'ItemCities',
        //     [
        //         'Items.item_category_id' => $category_id,
        //         'Items.featured' => 1,
        //         'ItemCities.city_id' => $city_id //Where Conditions
        //     ],
        //     [
        //         'Items.id', //Fields
        //         'Items.name',
        //         'Items.primary_photo_directory',
        //         'Items.primary_photo',
        //         'ItemCities.city_id',
        //         'ItemStocks.id',
        //         'ItemStocks.price',
        //         'ItemStocks.discount_price',
        //         'ItemStocks.discount_percentage'
        //     ],
        //     [
        //         'Items.name' //Order By
        //     ],
        //     $limit, //Limit
        //     [
        //         'Items', //Contains
        //         'ItemStocks'
        //     ]
        // );

        // pr($items);die();

        //OLD
        // foreach ($items as $key => $value) {
        //     $tmp = [];
        //     $tmp['full_image'] = $this->Special->getBaseImageUrl()
        //                         .$item_img_model
        //                         .$value['item']['primary_photo_directory']
        //                         ."/"
        //                         .$value['item']['primary_photo'];
        //
        //     $tmp['medium_image'] = $this->Special->getBaseImageUrl()
        //                         .$item_img_model
        //                         .$value['item']['primary_photo_directory']
        //                         ."/medium_"
        //                         .$value['item']['primary_photo'];
        //
        //     $tmp['small_image'] = $this->Special->getBaseImageUrl()
        //                         .$item_img_model
        //                         .$value['item']['primary_photo_directory']
        //                         ."/small_"
        //                         .$value['item']['primary_photo'];
        //
        //     $tmp['id'] = $value['item']['id'];
        //     $tmp['name'] = $value['item']['name'];
        //     $tmp['price'] = $value['item_stock']['price'];
        //     $tmp['discount_price'] = $value['item_stock']['discount_price'];
        //     $tmp['discount_percentage'] = $value['item_stock']['discount_percentage'];
        //
        //     //Push into Array
        //     array_push($resultsArray, $tmp);
        // }
        return $resultsArray;
    }


    /**
    *   Get Banners For City
    *
    * @param $city_id uuid    City ID
    *
    * @return array
    */
    public function getBanners($city_id = null)
    {
        $resultsArray = array();
        $banner_img_model = "banner/banner_filename/";
        $banners = $this->Query->getData(
            'BannerCities',
            [
                'BannerCities.city_id' => $city_id //Where Conditions
            ],
            [
                'Banners.id',
                'Banners.banner_filename',
                'Banners.file_dir',
                'BannerCities.id',
                'Banners.link'
            ],
            [],
            10,
            [
                'Banners'
            ]
        );

        foreach ($banners as $key => $value) {
            $tmp = [];
            $tmp['full_image'] = $this->Special->getBaseImageUrl()
                                .$banner_img_model
                                .$value['banner']['file_dir']
                                ."/"
                                .$value['banner']['banner_filename'];

            $tmp['link'] = $value['banner']['link'];
            //Push into Array
            array_push($resultsArray, $tmp);
        }

        return $resultsArray;
    }


    /**
    *  Check City For Item
    *
    * @return array
    */
    public function getItemByCity($item_id = null)
    {
        $tmp = $this->Query->getData(
            'ItemCities',
            [
                'ItemCities.item_id' => $item_id
            ],
            [
                'ItemCities.id',
                'Cities.name',
                'Cities.zip_prefix'
            ],
            [

            ],
            false,
            [
                'Cities'
            ]
        );
        return $tmp;
    }
}

?>
