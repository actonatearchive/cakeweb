<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   UserWallet
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2014-2016 Copyright (c) LetsShave Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * Constants Variables
 *
 * @category Constants
 * @package  UserWallet
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
interface Enum
{
    //---- constants in place of enumerations
    const OPERATION_FAILED = "Operation failed. Please Try again.";
    const USER_NOT_EXISTS = "Oops! User does not exists.";
    const SUCCESS = true;
    const FAILED = false;
    const ZERO_POINTS = 0.00;
    const INVALID_ARGUMENT = "INVALID ARGUMENT.";
}


/**
 * UserWallet Component
 *
 * @category Component
 * @package  UserWallet
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */

class UserWalletComponent extends Component implements Enum
{
    public $components = ['Special','User'];

    /**
     *   Give User Points ( LS Money )
     *
     * @param uuid     $user_id        User ID
     * @param decimal  $points         Points/Ls Money
     * @param string   $reason         Reason for this transaction.
     * @param uuid     $transaction_id Transaction ID
     * @param datetime $expires        Expires particular entry At.
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function giveUserPoints($user_id = null, $points = 0.00, $reason = null,
        $transaction_id = null, $expires = null
    ) {

        if ($expires == null) {
            //validation or defaults presets
            // if (env('LETSSHAVE_ENV') == "production") {
            //     $expires = date('Y-m-d h:m:s', strtotime('+15 years'));
            // } else {
                $expires = date('Y-m-d h:m:s', strtotime('+10 years'));
            // }
        }
            $transaction_type = ($points < 0 ? 0 : 1); //0 - debit, 1 - credit

        if ($this->User->userExistsByID($user_id)) {
            if ($this->_insertUserWalletLog(
                $user_id, $points, $transaction_type,
                $reason, $transaction_id, $expires
            )
            ) {
                return Enum::SUCCESS;
            } else {
                return Enum::OPERATION_FAILED;
            }
        } else {
            return Enum::USER_NOT_EXISTS;
        }
    }

    /**
     *   Get User Points ( LS Money )
     *
     * @param uuid $user_id User ID
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getUserPoints($user_id = null)
    {
        if ($this->User->userExistsByID($user_id)) {
            return $this->_calcPoints($user_id);
        } else {
            return Enum::USER_NOT_EXISTS;
        }
    }


    /**
     *   Get All Points ( LS Money )
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getAllPoints()
    {
        return $this->_calcAllPoints();
    }


    /**
     *   Get All User Wallet Logs
     *
     * @param uuid    $user_id User ID
     * @param integer $format  Type (Json/Array)
     *
     * @return array
     */
    public function getUserWalletLogs($user_id = null, $format = 0)
    {
        try {
            //Model->TableName
            $user_wallets = TableRegistry::get('UserWallets');

            $logs = $user_wallets->find()
                ->where(
                    [
                        'user_id' => $user_id
                    ]
                )
                ->toArray();

            if ($format === 0) {
                return $logs;
            } else if ($format == 1) {
                return json_encode($logs);
            } else {
                return Enum::INVALID_ARGUMENT;
            }
        } catch(Exception $err) {
            return Enum::FAILED;
        }
    }


    /**
     *   Calculate Overall user Points ( LS Money )
     *
     * @param uuid $user_id User ID
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _calcPoints($user_id = null)
    {
        try {
            //Model->TableName
            $user_wallets = TableRegistry::get('UserWallets');
            $curr_date = date('Y-m-d h:m:s');

            $query = $user_wallets->find();
            $points = $query->select(
                [
                    'sum'=> $query->func()->sum('points')
                ]
            )
                ->where(
                    [
                    'user_id' => $user_id,
                    'expires >=' => $curr_date
                    ]
                )
                ->toArray();
            if (!isset($points[0]['sum'])) {
                return Enum::ZERO_POINTS;
            } else {
                return $points[0]['sum'];
            }

        } catch(Exception $err) {
            return Enum::FAILED;
        }
    }


    /**
     *   Calculate Overall Points ( LS Money )
     *
     * @param uuid $user_id User ID
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _calcAllPoints($user_id = null)
    {
        try {
            //Model->TableName
            $user_wallets = TableRegistry::get('UserWallets');
            $curr_date = date('Y-m-d h:m:s');

            $query = $user_wallets->find();
            $points = $query->select(
                [
                    'sum'=> $query->func()->sum('points')
                ]
            )
                ->where(
                    [
                    'expires >=' => $curr_date
                    ]
                )
                ->toArray();
            if (!isset($points[0]['sum'])) {
                return Enum::ZERO_POINTS;
            } else {
                return $points[0]['sum'];
            }

        } catch(Exception $err) {
            return Enum::FAILED;
        }
    }


    /**
     *  Insert User Wallet Log
     *    DATE: 3rd March 2017
     *
     * @param uuid     $user_id          User ID
     * @param decimal  $points           Points/Ls Money
     * @param integer  $transaction_type 0 - Debit/1 - Credit
     * @param string   $reason           Reason
     * @param uuid     $transaction_id   Transaction ID
     * @param datetime $expires          Expires At
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _insertUserWalletLog($user_id = null, $points = 0.00,
        $transaction_type = null, $reason = null, $transaction_id = null,
        $expires = null
    ) {

            //Model->TableName
            $user_wallets = TableRegistry::get('UserWallets');

            //Creating User Wallet Entity
            $wallet_log = $user_wallets->newEntity();

            //Insert Wallet Log
            $wallet_log->id = $this->Special->UUIDv4();
            $wallet_log->user_id = $user_id;
            $wallet_log->points = $points;
            $wallet_log->transaction_type = $transaction_type;
            $wallet_log->transaction_id = $transaction_id;
            $wallet_log->reason = $reason;
            $wallet_log->expires = $expires;

        if ($user_wallets->save($wallet_log)) {
            return Enum::SUCCESS;
        } else {
            return Enum::FAILED;
        }
    }

}
?>
