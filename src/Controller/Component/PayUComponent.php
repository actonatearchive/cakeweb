<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   PayU
 * @author    Divya Khanani <divya.khanani@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\Network\Http\Client;
use Cake\Core\Configure;


/**
 * PayU Component
 *
 * @category Component
 * @package  PayU
 * @author   Divya Khanani <divya.khanani@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */

class PayUComponent extends Component
{

    public $MERCHANT_KEY;
    public $SALT;
    public $PAYU_BASE_URL;
    // public $MERCHANT_KEY = "gtKFFx";
    // public $SALT = "eCwWELxi";
    // public $PAYU_BASE_URL = "https://test.payu.in";
    public $service_url = 'https://info.payu.in/merchant/postservice.php?form=2';
    public $params = [
      "hash"=>"",
      "action"=>"",
      "key"=>"",
      "txnid"=>"",
      "amount"=>"",
      "productinfo"=>"",
      "firstname"=>"",
      "lastname"=>"",
      "surl"=>"",
      "furl"=>"",
      "phone"=>"",
      "email"=>"",
      "udf1"=>"",
      "udf2"=>"",
      "udf3"=>"",
      "udf4"=>"",
      "udf5"=>"",
      "udf6"=>"",
      "udf7"=>"",
      "udf8"=>"",
      "udf9"=>"",
      "udf10"=>""

    ];

    /**
     *    PayU construct
     *    DATE: 31th March 2017
     *
     * @return void
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function __construct()
    {
        $payu_config = Configure::read('payu');

        $this->MERCHANT_KEY = $payu_config['PAYU_MERCHANT_KEY']; //env('PAYU_MERCHANT_KEY');
        $this->SALT = $payu_config['PAYU_SALT'] ;//env('PAYU_SALT');
        $this->PAYU_BASE_URL = $payu_config['PAYU_BASE_URL']; //env('PAYU_BASE_URL');
    }
    /**
     *    PayU
     *    DATE: 14th March 2017
     *
     * @return array
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function process()
    {
        $params = $this->params;
        $action = '';
        $hash = '';
        $params['key'] = $this->MERCHANT_KEY;

        // Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email";
        $hashSequence .= "|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty($params['hash']) && sizeof($params) > 0) {
            if (empty($params['key'])
                || empty($params['txnid'])
                || empty($params['amount'])
                || empty($params['firstname'])
                || empty($params['email'])
                || empty($params['phone'])
                || empty($params['productinfo'])
                || empty($params['surl'])
                || empty($params['furl'])
            ) {
                return 0;
            } else {
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string
                    .= isset($params[$hash_var]) ? $params[$hash_var] : '';
                    $hash_string .= '|';
                }
                $hash_string .= $this->SALT;
                $hash = strtolower(hash('sha512', $hash_string));
                $action = $this->PAYU_BASE_URL . '/_payment';
            }
        } elseif (!empty($params['hash'])) {
            $hash = $params['hash'];
            $action = $this->PAYU_BASE_URL . '/_payment';
        }

        $params['hash'] = $hash;
        $params['action'] = $action;

        $payuInputObj = json_decode(json_encode($params), false);

        return $payuInputObj;
    }

    /**
     *    PayU getSavedCards
     *    DATE: 6th April 2017
     *
     * @param uuid $user_id Users Id
     *
     * @return array
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function getSavedCards($user_id)
    {
        $merchantKey = $this->MERCHANT_KEY;
        $salt = $this->SALT;
        //$service_url = 'https://test.payu.in/merchant/postservice.php?form=2';

        //form=1 : for ARRAY response output
        //form=2 : for JSON response output
        //$var2="745d72e2fd9b7e88824fef4e7ed7dac1a"; //card token

        $var1= $merchantKey.":".$user_id;

        $command = 'get_user_cards'; //e.g. verify_payment

        $string = $merchantKey.'|'.$command.'|'.$var1.'|'.$salt;

        $hash = hash('sha512', $string);
        $curl_post_data = array(
              'key' => $merchantKey,
              'command' => $command,
              'var1'=>$var1,
              'hash' => $hash,
              'salt'=>$salt

        );
        $http = new Client();
        $response = $http->post(
            $this->service_url,
            $curl_post_data,
            [
                    'headers' =>
                    [
                    'Content-Type' => 'application/json'
                    ]
                  ]
        );


        $de=json_decode($response->body());
        $decoded = json_decode(json_encode($de), true);
        $string = $merchantKey.'|save_user_card|'.$var1.'|'.$salt;
        $hash = hash('sha512', $string);
        $res = [
          'save_card_hash'=>$hash,'save_card_id'=>$var1,'saved_card'=>$decoded
        ];
        return $res;
    }

    /**
     *    PayU deleteUserCards
     *    DATE: 6th April 2017
     *
     * @param uuid $user_id    Users Id
     * @param uuid $card_token card token
     *
     * @return array
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function deleteUserCards($user_id,$card_token)
    {
        $merchantKey = $this->MERCHANT_KEY;
        $salt = $this->SALT;
        //$service_url = 'https://test.payu.in/merchant/postservice.php?form=2';

        //form=1 : for ARRAY response output
        //form=2 : for JSON response output
        //$var2="745d72e2fd9b7e88824fef4e7ed7dac1a"; //card token

        $var1= $merchantKey.":".$user_id;
        $var2 =$card_token;
        $command = 'delete_user_card'; //e.g. verify_payment

        $string = $merchantKey.'|'.$command.'|'.$var1.'|'.$salt;

        $hash = hash('sha512', $string);
        $curl_post_data = array(
              'key' => $merchantKey,
              'command' => $command,
              'var1'=>$var1,
              'var2'=>$var2,
              'hash' => $hash,
              'salt'=>$salt

        );
        $http = new Client();
        $response = $http->post(
            $this->service_url,
            $curl_post_data,
            [
                    'headers' =>
                    [
                    'Content-Type' => 'application/json'
                    ]
                  ]
        );


        $de=json_decode($response->body());
        $decoded = json_decode(json_encode($de), true);
        $string = $merchantKey.'|'.$command.'|'.$var1.'|'.$salt;
        $hash = hash('sha512', $string);
        $res = [
          'save_card_hash'=>$hash,'save_card_id'=>$var1,'saved_card'=>$decoded
        ];
        return $res;
    }

    /**
     *    PayU
     *    DATE: 14th March 2017
     *
     * @param array $transaction details of transaction

     * @return array
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function reconstructHash($transaction)
    {
        $SALT = $this->SALT;
        $merc_hash_vars_seq
            = explode(
                '|',
                "key|txnid|amount|productinfo|firstname|email|udf1|".
                "udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
            );
        //generation of hash after transaction is
        // = salt + status + reverse order of variables
        $merc_hash_vars_seq = array_reverse($merc_hash_vars_seq);

        $merc_hash_string = $SALT . '|' . $transaction['status'];

        foreach ($merc_hash_vars_seq as $merc_hash_var) {
            $merc_hash_string .= '|';
            if (isset($transaction[$merc_hash_var]) ) {
                if ($merc_hash_var == 'amount') {
                    $tmp="".sprintf('%0.2f', $transaction[$merc_hash_var])."";
                } else {
                    $tmp=$transaction[$merc_hash_var];
                }
            } else {

                $tmp='';
            }
            $merc_hash_string.=$tmp;
        }


        $merc_hash =strtolower(hash('sha512', $merc_hash_string));
        return $merc_hash;

    }


    /**
     *    PayU
     *    DATE: 14th March 2017
     *
     * @param array $transaction response data
     *
     * @return array
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function response($transaction)
    {
        $log = '';
        $log .= print_r($transaction, true);
        if ($transaction['status']=="success") {
            $hash = $this->reconstructHash($transaction);
            $log .= print_r($hash, true);

            if ($transaction['hash'] == $hash) {

                $log_ = 'key: processPayU_response';
                $log_ .= ',data: '.print_r($log, true);
                $log_ .= ',msg: success';
                $this->log($log_, 'debug');

                return $hash;
            } else {
                $log_ = 'key: processPayU_response';
                $log_ .= ',data: '.print_r($log, true);
                $log_ .= ',msg: fail : hash not match';
                $this->log($log_, 'error');

                return 'fail';
            }
        }

        $log_ = 'key: processPayU_response';
        $log_ .= ',data: '.print_r($log, true);
        $log_ .= ',msg: fail : error';
        $this->log($log_, 'error');

        return 'fail';
    }
}
