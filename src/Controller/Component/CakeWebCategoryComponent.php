<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   CakeWebCategory
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * CakeWebCategory Component
 *
 * @category Component
 * @package  CakeWebCategory
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
class CakeWebCategoryComponent extends Component
{
    //Load Other Components
    public $components = ['Special','Query'];

    public $child_city_id = null;

    /**
     *  Getter Method For Cakes
     *
     * @param $url_slag uuid URL Slag
     *
     * @return array
     */
    public function getCategory($url_slag = null)
    {
        $tmp = $this->Query->getDataById(
            'ItemCategories',
            [
                'url_slag' => $url_slag
            ],
            [
                'id',
                'name',
                'url_slag'
            ],
            false,
            [
                'ChildCategories'
            ]
        );

        return $tmp;
    }

    /**
    *   Get Festival Categories
    *
    * @return array
    */
    public function getFestivalCategories()
    {
        $festival_cat = $this->Query->getData(
            'ItemCategories',
            [
                'ItemCategories.is_festival' => 1,
                'ItemCategories.disabled' => 0 //0 - Enable, 1 - Disabled
            ],
            [
                'ItemCategories.id',
                'ItemCategories.name',
                'ItemCategories.url_slag'
            ],
            [
                'ItemCategories.modified DESC'
            ]
        );

        // pr($festival_cat); die();
        return $festival_cat;
    }

    /**
     *  Getter Method For Cakes
     *
     * @param $city_id uuid    City ID
     * @param $limit   integer Limit of records
     *
     * @return array
     */
    public function getCategoryCakes($category_id = null, $city_id = null,
        $limit = 16, $offset = 0, $sort_by = 'a_z'
    ) {
        $resultsArray = array();
        $item_img_model = "item/primary_photo/";

        $sort_order1 = [];
        $sort_order2 = ['ChildItems.sequence ASC'];
        
        if ($sort_by == 'a_z') {
            $sort_order1 = ['Items.name ASC'];
            // array_push($sort_order, 'Items.name ASC');
        } else if ($sort_by == 'z_a') {
            $sort_order1 = ['Items.name DESC'];            
        } else if ($sort_by == 'low_high') {
            $sort_order2[] = ['ItemCities.discount_price ASC'];            
        } else if ($sort_by == 'high_low') {
            $sort_order2[] = ['ItemCities.discount_price DESC'];        
        }

        // pr($sort_by);
        // pr($sort_order);

        $this->child_city_id = $city_id;
        $items = $this->Query->getData(
            'Items',
            [
                'Items.item_category_id' => $category_id,
                // 'Items.featured' => 1,
                'ItemCities.city_id' => $city_id,
                'Items.item_id is' => null,
                'Items.disabled' => 0
            ],
            [
                'Items.id',
                'Items.name',
                'Items.variant_name',
                'Items.primary_photo',
                'Items.primary_photo_directory',
                'Items.url_slag',
                'ItemCities.price',
                'ItemCities.discount_price',
                'ItemCities.discount_percentage'

            ],
            $sort_order1,
            $limit,
            [
                'ItemCities',
                'ChildItems' => function($q) {
                    $q->select(
                        [
                           'ChildItems.id',
                           'ChildItems.name',
                           'ChildItems.variant_name',
                           'ChildItems.primary_photo',
                           'ChildItems.primary_photo_directory',
                           'ItemCities.id',
                           'ItemCities.city_id',
                           'ItemCities.item_id',
                           'ItemCities.price',
                           'ItemCities.discount_price',
                           'ItemCities.discount_percentage'
                        ]
                    );

                    $q->where(
                        [
                            'ItemCities.city_id' => $this->child_city_id
                        ]
                    );

                    // $q->order(
                    //     $sort_order2
                    // );

                    $q->contain(['ItemCities']);
                    return $q->autoFields(false);
                }
            ],
            $offset
        );

        $resultsArray = array();
        foreach ($items as $key => $value) {
        $tmp = [];

        $tmp['id'] = $value['id'];
        $tmp['name'] = $value['name'];
        $tmp['url_slag'] = $value['url_slag'];

        $tmp['medium_image'] = $this->Special->getBaseImageUrl()
                            .$item_img_model
                            .$value['primary_photo_directory']
                            ."/medium_"
                            .$value['primary_photo'];

        $tmp['price'] = $value['item_city']['price'];
        $tmp['discount_price'] = $value['item_city']['discount_price'];
        $tmp['discount_percentage'] = $value['item_city']['discount_percentage'];

        //Create childItems array;
        $tmp['child_items'] = array();
        $k = 0;
        //First Child Item Clone Parent
        // $tmp['child_items'][$k]['id'] = $value['id'];
        // $tmp['child_items'][$k]['name'] = $value['name'];
        // $tmp['child_items'][$k]['medium_image'] = $tmp['medium_image'];
        // $tmp['child_items'][$k]['variant_name'] = $value['variant_name'];
        // $tmp['child_items'][$k]['price'] = $value['item_city']['price'];
        // $tmp['child_items'][$k]['discount_price'] = $value['item_city']['discount_price'];
        // $tmp['child_items'][$k]['discount_percentage'] = $value['item_city']['discount_percentage'];
        //
        // $variant_url = str_replace(" ","",$value['variant_name']);
        // $variant_url = str_replace("/","-",$variant_url);
        // $tmp['child_items'][$k]['variant_url'] = $variant_url;
        //
        // $k++;
        if (sizeof($value['child_items']) > 0)
        {
            foreach ($value['child_items'] as $key2 => $val2) {

                if ($k == 0) {
                    $tmp['price'] = $val2['item_city']['price'];
                    $tmp['discount_price'] = $val2['item_city']['discount_price'];
                    $tmp['discount_percentage'] = $val2['item_city']['discount_percentage'];
                }

                $tmp['child_items'][$k]['id'] = $val2['id'];
                $tmp['child_items'][$k]['name'] = $val2['name'];
                $tmp['child_items'][$k]['variant_name'] = $val2['variant_name'];
                $tmp['child_items'][$k]['medium_image'] = $this->Special->getBaseImageUrl()
                                    .$item_img_model
                                    .$val2['primary_photo_directory']
                                    ."/medium_"
                                    .$val2['primary_photo'];

                $tmp['child_items'][$k]['price'] = $val2['item_city']['price'];
                $tmp['child_items'][$k]['discount_price'] = $val2['item_city']['discount_price'];
                $tmp['child_items'][$k]['discount_percentage'] = $val2['item_city']['discount_percentage'];

                $variant_url = str_replace(" ","",$val2['variant_name']);
                $variant_url = str_replace("/","-",$variant_url);
                $tmp['child_items'][$k]['variant_url'] = $variant_url;

                $k++;
            }
        }
        array_push($resultsArray, $tmp);
        }
        // pr($resultsArray);die();
        return $resultsArray;
    }


    /**
    *  Get Product Details
    *
    * @return array
    */
    public function getProductDetailsLegacy($url_slag = null, $city_id = null)
    {
        $resultsArray = array();
        $item_img_model = "item/primary_photo/";

        $tmp = $this->Query->getDataById(
            'ItemCities',
            [
                'ItemCities.city_id' => $city_id, //Where Conditions
                'Items.item_id is' => null, //Always fetch Parent
                'Items.url_slag' => $url_slag
            ],
            [
                'Items.id',
                'Items.name',
                'Items.variant_name',
                'Items.short_desc',
                'Items.long_desc',
                'ItemStocks.id',
                'ItemStocks.price',
                'Items.primary_photo',
                'ItemStocks.discount_price',
                'Items.primary_photo_directory',
                'ItemStocks.discount_percentage',
                'Cities.name',
                'Cities.shipping_handling_charges',
                'Cities.disabled',
                'ItemCategories.id',
                'ItemCategories.name',
                'ItemCategories.url_slag',
                'ItemCategories.disabled'

            ],
            [
                'Items.name'
            ],
            [
                'ItemStocks',
                'Cities',
                'Items.ItemCategories',
                'Items.ChildItems' => function($q) {
                    $q->select(
                        [
                            'ChildItems.id',
                            'ChildItems.name',
                            'ChildItems.variant_name',
                            'ChildItems.primary_photo',
                            'ChildItems.primary_photo_directory',
                            'ItemStocks.price',
                            'ItemStocks.discount_price',
                            'ItemStocks.discount_percentage'
                        ]
                    );
                    $q->contain(['ItemStocks']);
                    return $q->autoFields(false);
                }
            ]
        );

        // pr($tmp);die();
        $item = [];

        if (!empty($tmp)) {

            $item['id'] = $tmp['Items']['id'];
            $item['name'] = $tmp['Items']['name'];

            $item['medium_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$tmp['Items']['primary_photo_directory']
                                ."/medium_"
                                .$tmp['Items']['primary_photo'];

            $item['large_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$tmp['Items']['primary_photo_directory']
                                ."/large_"
                                .$tmp['Items']['primary_photo'];

            $item['full_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$tmp['Items']['primary_photo_directory']
                                ."/"
                                .$tmp['Items']['primary_photo'];

            $item['price'] = $tmp['ItemStocks']['price'];
            $item['discount_price'] = $tmp['ItemStocks']['discount_price'];
            $item['discount_percentage'] = $tmp['ItemStocks']['discount_percentage'];
            $item['shipping_handling_charges'] = $tmp['Cities']['shipping_handling_charges'];
            $item['is_disabled'] = $tmp['Cities']['disabled']; //if it is 1 then redirect user to homepage
            $item['short_desc'] = $tmp['Items']['short_desc'];
            $item['long_desc'] = $tmp['Items']['long_desc'];

            //if category is set
            if (isset($tmp['Items']['item_category'])) {
                $item['category'] = array();
                $cat = $tmp['Items']['item_category'];
                $item['category']['id'] = $cat['id'];
                $item['category']['name'] = $cat['name'];
                $item['category']['is_disabled'] = $cat['disabled'];
                $item['category']['url_slag'] = $cat['url_slag'];

            }

            $tmp['child_items'] = array();
            $k = 0;
            //First Child Item Clone Parent
            $item['child_items'][$k]['id'] = $item['id'];
            $item['child_items'][$k]['name'] = $item['name'];
            $item['child_items'][$k]['medium_image'] = $item['medium_image'];
            $item['child_items'][$k]['variant_name'] = $tmp['Items']['variant_name']; //First Variant
            $item['child_items'][$k]['price'] = $item['price'];
            $item['child_items'][$k]['discount_price'] = $item['discount_price'];
            $item['child_items'][$k]['discount_percentage'] = $item['discount_percentage'];

            $variant_url = str_replace(" ","",$tmp['Items']['variant_name']);
            $variant_url = str_replace("/","-",$variant_url);
            $item['child_items'][$k]['variant_url'] = $variant_url;

            $k++;

            foreach ($tmp['Items']['child_items'] as $key => $value) {
                $item['child_items'][$k]['id'] = $value['id'];
                $item['child_items'][$k]['name'] = $value['name'];
                $item['child_items'][$k]['variant_name'] = $value['variant_name']; //First Variant
                $item['child_items'][$k]['medium_image'] = $item['medium_image'];
                $item['child_items'][$k]['price'] = $value['item_stock']['price'];
                $item['child_items'][$k]['discount_price'] = $value['item_stock']['discount_price'];
                $item['child_items'][$k]['discount_percentage'] = $value['item_stock']['discount_percentage'];

                $variant_url = str_replace(" ","",$value['variant_name']);
                $variant_url = str_replace("/","-",$variant_url);
                $item['child_items'][$k]['variant_url'] = $variant_url;

                $k++;
            }
            // pr($item); //Just For Debug Purpose
            return $item;
        } else {
            //Item Not Found [404]
            return false;
        }
    }


    public function getProductDetails($url_slag = null,$city_id = null)
    {
        $item_img_model = "item/primary_photo/";
        $this->child_city_id = $city_id;
        $tmp = $this->Query->getDataById(
            'Items',
            [
                'ItemCities.city_id' => $city_id,
                'Items.item_id is' => null,
                'Items.url_slag' => $url_slag,
                'Items.disabled' => 0
            ],
            [
                'Items.id',
                'Items.name',
                'Items.variant_name',
                'Items.short_desc',
                'Items.primary_photo',
                'Items.primary_photo_directory',
                'Items.keyword',
                'Items.shipping_charges',
                'ItemCities.id',
                'ItemCities.city_id',
                'ItemCities.item_id',
                'ItemCities.price',
                'ItemCities.discount_price',
                'ItemCities.discount_percentage',
                'Cities.name',
                'Cities.shipping_handling_charges',
                'Cities.disabled',
                'ItemCategories.id',
                'ItemCategories.name',
                'ItemCategories.url_slag',
                'ItemCategories.disabled'
            ],
            [
                'Items.name'
            ],
            [
                'ItemCities',
                'ItemCities.Cities',
                'ItemCategories',
                'ChildItems' => function($q) {
                    $q->select(
                        [
                           'ChildItems.id',
                           'ChildItems.name',
                           'ChildItems.variant_name',
                           'ChildItems.primary_photo',
                           'ChildItems.primary_photo_directory',
                           'ItemCities.id',
                           'ItemCities.city_id',
                           'ItemCities.item_id',
                           'ItemCities.price',
                           'ItemCities.discount_price',
                           'ItemCities.discount_percentage'
                        ]
                    );
                    $q->order('ChildItems.sequence ASC');

                    $q->where(
                        [
                            'ItemCities.city_id' => $this->child_city_id
                        ]
                    );

                    $q->contain(['ItemCities']);
                    return $q->autoFields(false);
                }
            ]
        );


        // pr($tmp);
        $item = [];

        if (!empty($tmp)) {
        //
            $item['id'] = $tmp['id'];
            $item['name'] = $tmp['name'];
            $item['medium_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$tmp['primary_photo_directory']
                                ."/medium_"
                                .$tmp['primary_photo'];

            $item['large_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$tmp['primary_photo_directory']
                                ."/large_"
                                .$tmp['primary_photo'];

            $item['full_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$tmp['primary_photo_directory']
                                ."/"
                                .$tmp['primary_photo'];

            //-------------------23rd Oct 2017------------------
            if ($tmp['shipping_charges'] == -1) {
                // City Level Charges
                $item['shipping_handling_charges'] = $tmp['item_city']['city']['shipping_handling_charges'];
            } else {
                // Item Level Charges
                $item['shipping_handling_charges'] = $tmp['shipping_charges'];
            }
            //----------------------------------------------------

            $item['is_disabled'] = $tmp['item_city']['city']['disabled']; //if it is 1 then redirect user to homepage
            $item['short_desc'] = $tmp['short_desc'];
            $item['keyword'] = $tmp['keyword'];

            //if category is set
            if (isset($tmp['item_category'])) {
                $item['category'] = array();
                $cat = $tmp['item_category'];
                $item['category']['id'] = $cat['id'];
                $item['category']['name'] = $cat['name'];
                $item['category']['is_disabled'] = $cat['disabled'];
                $item['category']['url_slag'] = $cat['url_slag'];
            }
        //
            $item['child_items'] = array();
            $k = 0;
            //First Child Item Clone Parent
        //     $item['child_items'][$k]['id'] = $item['id'];
        //     $item['child_items'][$k]['name'] = $item['name'];
        //     $item['child_items'][$k]['medium_image'] = $item['medium_image'];
        //     $item['child_items'][$k]['variant_name'] = $tmp['variant_name']; //First Variant
        //     $item['child_items'][$k]['price'] = $item['price'];
        //     $item['child_items'][$k]['discount_price'] = $item['discount_price'];
        //     $item['child_items'][$k]['discount_percentage'] = $item['discount_percentage'];
        // //
        //     $variant_url = str_replace(" ","",$tmp['variant_name']);
        //     $variant_url = str_replace("/","-",$variant_url);
        //     $item['child_items'][$k]['variant_url'] = $variant_url;
        // //
        //     $k++;
        //
            foreach ($tmp['child_items'] as $key => $value) {
                if ($k == 0) {
                    $item['price'] = $value['item_city']['price'];
                    $item['discount_price'] = $value['item_city']['discount_price'];
                    $item['discount_percentage'] = $value['item_city']['discount_percentage'];
                }

                $item['child_items'][$k]['id'] = $value['id'];
                $item['child_items'][$k]['name'] = $value['name'];
                $item['child_items'][$k]['variant_name'] = $value['variant_name']; //First Variant
                $item['child_items'][$k]['medium_image'] = $item['medium_image'];
                $item['child_items'][$k]['price'] = $value['item_city']['price'];
                $item['child_items'][$k]['discount_price'] = $value['item_city']['discount_price'];
                $item['child_items'][$k]['discount_percentage'] = $value['item_city']['discount_percentage'];

                $variant_url = str_replace(" ","",$value['variant_name']);
                $variant_url = str_replace("/","-",$variant_url);
                $item['child_items'][$k]['variant_url'] = $variant_url;

                $k++;
            }
        //     // pr($item); //Just For Debug Purpose
            return $item;
        } else {
            //Item Not Found [404]
            return false;
        }
    }



    /**
     *  Getter Method For Cakes
     *
     * @param $city_id uuid    City ID
     * @param $limit   integer Limit of records
     *
     * @return array
     */
    public function getSearchCakes($search = null, $city_id = null,
        $limit = 16, $offset = 0
    ) {
        $resultsArray = array();
        $item_img_model = "item/primary_photo/";

        $this->child_city_id = $city_id;
        $items = $this->Query->getData(
            'Items',
            [
                'Items.name LIKE' => '%'.$search.'%',
                // 'Items.featured' => 1,
                'ItemCities.city_id' => $city_id,
                'Items.item_id is' => null,
                'Items.disabled' => 0
            ],
            [
                'Items.id',
                'Items.name',
                'Items.variant_name',
                'Items.primary_photo',
                'Items.primary_photo_directory',
                'Items.url_slag',
                'ItemCities.price',
                'ItemCities.discount_price',
                'ItemCities.discount_percentage'

            ],
            [
                'Items.name'
            ],
            $limit,
            [
                'ItemCities',
                'ChildItems' => function($q) {
                    $q->select(
                        [
                           'ChildItems.id',
                           'ChildItems.name',
                           'ChildItems.variant_name',
                           'ChildItems.primary_photo',
                           'ChildItems.primary_photo_directory',
                           'ItemCities.id',
                           'ItemCities.city_id',
                           'ItemCities.item_id',
                           'ItemCities.price',
                           'ItemCities.discount_price',
                           'ItemCities.discount_percentage'
                        ]
                    );
                    $q->order('ChildItems.sequence ASC');

                    $q->where(
                        [
                            'ItemCities.city_id' => $this->child_city_id
                        ]
                    );

                    $q->contain(['ItemCities']);
                    return $q->autoFields(false);
                }
            ],
            $offset
        );

        $resultsArray = array();
        foreach ($items as $key => $value) {
        $tmp = [];

        $tmp['id'] = $value['id'];
        $tmp['name'] = $value['name'];
        $tmp['url_slag'] = $value['url_slag'];

        $tmp['medium_image'] = $this->Special->getBaseImageUrl()
                            .$item_img_model
                            .$value['primary_photo_directory']
                            ."/medium_"
                            .$value['primary_photo'];

        $tmp['price'] = $value['item_city']['price'];
        $tmp['discount_price'] = $value['item_city']['discount_price'];
        $tmp['discount_percentage'] = $value['item_city']['discount_percentage'];

        //Create childItems array;
        $tmp['child_items'] = array();
        $k = 0;
        //First Child Item Clone Parent
        // $tmp['child_items'][$k]['id'] = $value['id'];
        // $tmp['child_items'][$k]['name'] = $value['name'];
        // $tmp['child_items'][$k]['medium_image'] = $tmp['medium_image'];
        // $tmp['child_items'][$k]['variant_name'] = $value['variant_name'];
        // $tmp['child_items'][$k]['price'] = $value['item_city']['price'];
        // $tmp['child_items'][$k]['discount_price'] = $value['item_city']['discount_price'];
        // $tmp['child_items'][$k]['discount_percentage'] = $value['item_city']['discount_percentage'];
        //
        // $variant_url = str_replace(" ","",$value['variant_name']);
        // $variant_url = str_replace("/","-",$variant_url);
        // $tmp['child_items'][$k]['variant_url'] = $variant_url;
        //
        // $k++;
        if (sizeof($value['child_items']) > 0)
        {
            foreach ($value['child_items'] as $key2 => $val2) {
                if ($k == 0) {
                    $tmp['price'] = $val2['item_city']['price'];
                    $tmp['discount_price'] = $val2['item_city']['discount_price'];
                    $tmp['discount_percentage'] = $val2['item_city']['discount_percentage'];
                }

                $tmp['child_items'][$k]['id'] = $val2['id'];
                $tmp['child_items'][$k]['name'] = $val2['name'];
                $tmp['child_items'][$k]['variant_name'] = $val2['variant_name'];
                $tmp['child_items'][$k]['medium_image'] = $this->Special->getBaseImageUrl()
                                    .$item_img_model
                                    .$val2['primary_photo_directory']
                                    ."/medium_"
                                    .$val2['primary_photo'];

                $tmp['child_items'][$k]['price'] = $val2['item_city']['price'];
                $tmp['child_items'][$k]['discount_price'] = $val2['item_city']['discount_price'];
                $tmp['child_items'][$k]['discount_percentage'] = $val2['item_city']['discount_percentage'];

                $variant_url = str_replace(" ","",$val2['variant_name']);
                $variant_url = str_replace("/","-",$variant_url);
                $tmp['child_items'][$k]['variant_url'] = $variant_url;

                $k++;
            }
        }
        array_push($resultsArray, $tmp);
        }
        // pr($resultsArray);die();
        return $resultsArray;
    }

    public function getCategoryList()
    {
        $items = $this->Query->getData(
            'ItemCategories',
            [
                'ItemCategories.show_home' => 1
            ],
            [
                'ItemCategories.id',
                'ItemCategories.name',
                'ItemCategories.url_slag',
                'ItemCategories.sequence'

            ],
            [
                'ItemCategories.sequence'
            ],
            false,
            [
                'ChildCategories' => function($q) {
                    $q->select(
                        [
                            'ChildCategories.id',
                            'ChildCategories.name',
                            'ChildCategories.url_slag',
                            'ChildCategories.sequence'
                        ]
                    );
                    $q->order('ChildCategories.sequence ASC');

                    $q->where(
                        [
                            'ChildCategories.show_home' => 1
                        ]
                    );
                    return $q->autoFields(false);
                }
            ]
        );
        return $items;
    }

}
?>
