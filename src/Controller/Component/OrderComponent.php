<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   Order
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * Order Component
 *
 * @category Component
 * @package  Order
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */
class OrderComponent extends Component
{
    //Load Other Components
    public $components = ['Special','CakeWebCart','Query','SMS','Email','UserWallet'];

    /**
    *  Create Order
    *
    * @return array
    */
    public function createOrder($user_id = null, $data = [])
    {
        $data['id'] = $this->Special->UUIDv4();
        $data['code'] = $this->Special->getOrderCode("CW");
        $data['user_id'] = $user_id;
        $data['status'] = 0;

        //Coupons Data If any (Future)

        //---------------------
        $order = $this->Query->setData('Orders',$data); //OrderCreated

        //Let's Add it's OrderItems
        $cart_items = $this->CakeWebCart->getUserCart();

        foreach ($cart_items as $key => $value) {
            $this->addOrderItems($order['id'], $value);
        }

        return $order;
    }

    public function addOrderItems($order_id = null, $item = [])
    {
        $order_item = [];
        $order_item['id'] = $this->Special->UUIDv4();
        $order_item['quantity'] = $item['qty'];
        $order_item['item_params'] = json_encode($item['item_params']);
        $order_item['order_id'] = $order_id;
        $order_item['item_id'] = $item['item_id'];
        $order_item['price'] = $item['price'];
        $order_item['item_image'] = $item['medium_image'];
        $order_item['item_name'] = $item['name'];
        $order_item['item_variant_name'] = $item['variant_name'];
        

        $ret_data = $this->Query->setData('OrderItems', $order_item);
        return $ret_data;
    }


    public function getOrderDetails($order_id = null)
    {
        $order_details = $this->Query->getDataById(
            'Orders',
            [
                'Orders.id' => $order_id,
                'Orders.status' => 0
            ],
            [
                'Orders.id',
                'Orders.code',
                'Orders.total_amount',
                'Orders.additional_charges',
                'Orders.discount_value',
                'Orders.eggless',
                'Orders.shipping_address_id',
                'Orders.reward_points',
                'Orders.billing_address_id',
                'Orders.user_id',
                'Users.username',
                'Users.first_name',
                'Users.last_name',
                'Users.mobile',
                'Orders.transaction_id'
            ],
            [],
            [
                'Users'
            ]
        );
        if (!empty($order_details)) {
            $order_details['grand_total'] = $this->Special->getOrderGrandTotal($order_details);
        } else {
            return false;
        }
        return $order_details;
    }


    public function getOrderData($order_id = null)
    {
        $order_details = $this->Query->getDataById(
            'Orders',
            [
                'Orders.id' => $order_id
            ],
            [
                'Orders.id',
                'Orders.code',
                'Orders.total_amount',
                'Orders.additional_charges',
                'Orders.reward_points',
                
                'Orders.discount_value',
                'Orders.eggless',
                'Orders.shipping_address_id',
                'Orders.billing_address_id',
                'Orders.payment_mode',
                'Orders.payment_through',
                
                'Orders.user_id',
                'Users.username',
                'Users.first_name',
                'Users.last_name',
                'Users.mobile'
                // 'ShippingAddresses.pin_code',
                // 'ShippingAddresses.mobile'
            ],
            [],
            [
                'OrderItems',
                'Users'
            ]
        );
        if (!empty($order_details)) {
            $order_details['grand_total'] = $this->Special->getOrderGrandTotal($order_details);
        } else {
            return false;
        }

        if (isset($order_details['payment_mode'])) {
            $temp_str = $this->getCardType($order_details['payment_mode']);
            $order_details['payment_type'] 
            = ($temp_str == "" ) ? $order_details['payment_through'] : $temp_str ;
        }
        return $order_details;
    }

    public function getUserOrders($user_id = null)
    {
        $order_details_list = $this->Query->getAllData(
            'Orders',
            [
                'Orders.user_id' => $user_id,
                'Orders.status !=' => 0
            ],
            [],
            false,
            [
                'ShippingAddresses',
                'OrderItems'
            ]
        );
        foreach ($order_details_list as &$order_details) {
           if (!empty($order_details)) {
                $order_details['grand_total'] = $this->Special->getOrderGrandTotal($order_details);
            } else {
                return false;
            }
            if (isset($order_details['payment_mode'])) {
                $temp_str = $this->getCardType($order_details['payment_mode']);
                $order_details['payment_type'] 
                = ($temp_str == "" ) ? $order_details['payment_through'] : $temp_str ;
            }
        }
        
        return $order_details_list;
    }
    /**
    *   Get Card Type
    *
    * @return string
    */
    public function getCardType($type = null)
    {
        if ($type == null) return false;

        if ($type == 'CC') {
            return "Credit Card";
        } else if ($type == 'DC') {
            return "Debit Card";
        } else if ($type == 'NB') {
            return "Net Banking";
        } else if ($type == 'COD') {
            return "Cash on Delivery";
        } else if ($type == 'CWMoney') {
            return "Cake Web points";
        }
        return "";
    }
    /**
     *   Create and Update transaction
     *
     * @param array $transactionDetails Subscription Details array
     *
     * @return boolean
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function saveTransaction($transactionDetails = [])
    {
        if (isset($transactionDetails['id']) == false
            || $transactionDetails['id']== null
        ) {
            $transactionDetails['id'] = $this->Special->UUIDv4();
        }

        //Model->TableName
        // $transactions = TableRegistry::get('Transactions');
        // //Creating OrderSubcriptions Entity
        // $transaction = $transactions->newEntity($transactionDetails);
        // $transaction->id = $transactionDetails['id'];


        $trans_arr = (array) $transactionDetails;

        $ret = $this->Query->setData(
            'Transactions',
            $trans_arr
        );

        if (!empty($ret)) {
            return $ret;
        }

        return false;
    }

    /**
    *   Update Order
    *
    * @return order
    */
    public function updateOrder($order_data = [])
    {
        $order = $this->Query->setData(
            'Orders',
            $order_data
        );

        if (!empty($order)) {
            return $order;
        }
        return false;
    }


    /**
     * Order Failure
     *
     * @param array $transactionData Transaction Data
     *
     * @return array
     */
    public function orderFailure($transactionData = [])
    {
        // save Transaction
        $transaction=$this->saveTransaction($transactionData);
        if ($transaction) {
            $order = $transactionData;
            $order['transaction_id'] = $transaction['id'];
            $order['admin_remarks'] = "At transaction failure";
            $order['id'] = $transactionData['order_id'];
            unset($order['hash']);
            unset($order['code']);
            unset($order['order_id']);

            $order_result=$this->updateOrder($order);
            if ($order_result != null) {
                return true;
            }
            return false;
        }
    }


    /**
     * Order Success
     *
     * @param array $transactionData Transaction Data
     *
     * @return array
     */
    public function orderSuccess($transactionData = [])
    {
        // save Transaction
        $transaction = $this->saveTransaction($transactionData);

        if ($transaction) {
            $order = $transactionData;
            $order['transaction_id'] = $transaction['id'];
            $order['hash'] = $transactionData['order_hash'];
            unset($order['code']);
            unset($order['order_id']);

            $order['order_placed_at'] = date('Y-m-d H:i:s');
            $order['id'] = $transactionData['order_id'];
            $order_result=$this->updateOrder($order);

            $order_details = $this->Query->getDataById(
                'Orders',
                [
                    'Orders.id' => $order['id']
                ],
                [
                    'Orders.id',
                    'Orders.user_id',
                    'Orders.code',
                    'Orders.transaction_id',
                    'Orders.total_amount',
                    'Orders.additional_charges',
                    'Orders.reward_points',
                    
                    'Orders.discount_value',
                    'Orders.eggless',
                    'Orders.payment_through',
                    'Users.username',
                    'Users.mobile',
                    'Orders.order_placed_at',
                    'ShippingAddresses.id',
                    'ShippingAddresses.name',
                    'ShippingAddresses.line1',
                    'ShippingAddresses.line2',
                    'ShippingAddresses.landmark',
                    'ShippingAddresses.city_name',
                    'ShippingAddresses.pin_code',
                    'ShippingAddresses.mobile'
                ],
                [],
                [
                    'Users',
                    'ShippingAddresses',
                    'OrderItems'
                ]
            );
            // $this->log("ORDER_DETAILS");
            // $this->log($order_details);
            //Delete User Cart
            $this->CakeWebCart->deleteCart();
            if( $order_details['reward_points'] > 0 ){
                $this->UserWallet->giveUserPoints(
                    $order_details['user_id'],
                    - $order_details['reward_points'],
                    'Used in order:'.$order_details['code'],
                    $order_details['transaction_id']
                );
            }
            if (!empty($order_details)) {
                //Send Mail and SMS Here...
                $order_data = [];
                $order_data['code'] = $order_details['code'];
                $order_data['grand_total'] = $this->Special->getOrderGrandTotal($order_details);

                $this->log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                $this->log("MAIL_SENT!");
                $this->log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                try{
                    $this->Email->send(
                        'order',
                        $order_details['user']['username'],
                        'Thank you for your order',
                        [
                            'order' => $order_details
                        ],
                        [
                            // 'divya.khanani@actonate.com',
                            'viral84gajjar@gmail.com' //BCC
                        ]
                    );
                } catch( Exception $e ) {

                }
                

                // $this->log("ORDER_DETAILS");
                // $this->log($order_details);
                $this->log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                if (strlen($order_details['user']['mobile']) == 10) {
                    $order_data['mobile'] = $order_details['user']['mobile'];
                    $this->SMS->sendOrderConfirmation($order_data);
                }
                $this->log("SMS_SENT!");
                $this->log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");

            }
            return true;
        }
        return false;
    }
}
