<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   CakeWebCart
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * CakeWebCart Component
 *
 * @category Component
 * @package  CakeWebCart
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
class CakeWebCartComponent extends Component
{
    //Load Other Components
    public $components = ['Special','Query'];


    /**
     * Create Cart Method
     *
     * @return void
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function createCart()
    {
        $cartItems = array();
        if (!$this->Special->checkSession('CartItems')) {
            $this->Special->setSession('CartItems', $cartItems);
        }
        return $this->Special->getSession('CartItems');
    }

    /**
     * Get User Cart
     *
     * @param uuid $user_id User ID
     *
     * @return array cartItems
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getUserCart($user_id = null)
    {
        $selected_city = $this->Special->getCookie('selectedCity');
        $city_id = $selected_city['id'];
        if ($user_id === null) {
            //Null getFromCookies
            return $this->getCartItemsFromSession($city_id);
        } else {
            //getFrom Database
            return $this->getCartItemsFromDB($user_id, $city_id);
        }
    }

    /**
     * Get Cart Items From Table `axi_cart_items`
     *
     * @param uuid $user_id User ID
     *
     * @return array cartItems
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartItemsFromDB($user_id = null)
    {
        $resultsArray = array();

        $selected_city = $this->Special->getCookie('selectedCity');
        $city_id = $selected_city['id'];

        $item_img_model = 'item/primary_photo/';
        $tmp = $this->Query->getData(
            'CartItems',
            [
                'CartItems.user_id' => $user_id,
                'ItemCities.city_id' => $city_id
            ],
            [
                'CartItems.id',
                'CartItems.addon',

                'CartItems.item_params',
                'CartItems.quantity',
                'Items.id',
                'Items.name',

                'Items.url_slag',
                'Items.variant_name',
                'Items.primary_photo',
                'Items.primary_photo_directory',
                'ItemCities.price',
                'ItemCities.discount_price',
                'ItemCities.discount_percentage',
                'ItemCities.id',
                'ItemCities.shipping_charges',
                'Cities.name',
                'Cities.shipping_handling_charges',
                'ParentItem.id',
                // 'ParentItem.variant_name',
                'ParentItem.primary_photo',
                'ParentItem.primary_photo_directory',
                'ParentItem.shipping_charges'
            ],
            ['CartItems.addon'],
            false,//NoLimit
            [
                'Items',
                'Items.ParentItem',
                'Items.ItemStocks',
                'Items.ItemCities',
                'Items.ItemCities.Cities'
            ]
        );
        // pr($tmp);die();
        foreach ($tmp as $key => $value) {
            $item = [];
            $item['item_id'] = $value['item']['id'];
            $item['name'] = $value['item']['name'];
            $item['url_slag'] = $value['item']['url_slag'];
            
            $item['variant_name'] = $value['item']['variant_name'];
            $item['addon'] = $value['addon'];
            $item['qty'] = $value['quantity'];
            $item['price'] = $value['item']['item_city']['discount_price'];
            $item['medium_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$value['item']['parent_item']['primary_photo_directory']
                                ."/medium_"
                                .$value['item']['parent_item']['primary_photo'];

            $item['small_image'] = $this->Special->getBaseImageUrl()
                                .$item_img_model
                                .$value['item']['parent_item']['primary_photo_directory']
                                ."/small_"
                                .$value['item']['parent_item']['primary_photo'];
            $item['item_params'] = json_decode($value['item_params'], true);
            if ($value['item']['item_city']['shipping_charges'] == -1) {
                $item['shipping_charges'] = $value['item']['item_city']['city']['shipping_handling_charges'];
            } else {
                $item['shipping_charges'] = $value['item']['item_city']['shipping_charges'];
            }
            array_push($resultsArray, $item);
        }

        // pr("[CartItems] FROM DB");
        // pr($resultsArray);
        return $resultsArray;
    }


    /**
     * Get Cart Items From Table `axi_cart_items`
     *
     * @param uuid $user_id User ID
     *
     * @return array cartItems
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartItemsFromSession()
    {
        $resultsArray = array();

        $items = $this->Special->getSession('CartItems');

        $selected_city = $this->Special->getCookie('selectedCity');
        $city_id = $selected_city['id'];
        if (empty($items)) {
            return false;
        }
        $item_ids = array();
        foreach ($items as $val) {
            array_push($item_ids, $val['item_id']);
        }
        // pr("ITEM_IDS");
        // pr($item_ids);
        $item_img_model = 'item/primary_photo/';
        $tmp = $this->Query->getData(
            'Items',
            [
                'Items.id IN' => $item_ids,
                'ItemCities.city_id' => $city_id
            ],
            [
                'Items.id',
                'Items.name',
                'Items.item_id',
                'Items.url_slag',
                'Items.variant_name',
                'ItemCities.price',
                'ItemCities.discount_price',
                'ItemCities.discount_percentage',
                'ItemCities.id',
                'ItemCities.shipping_charges',
                'Cities.name',
                'Cities.shipping_handling_charges',
                'ParentItem.id',
                // 'ParentItem.variant_name',
                'ParentItem.primary_photo',
                'ParentItem.primary_photo_directory',
                'ParentItem.shipping_charges'
            ],
            [],
            false,//NoLimit
            [
                'ParentItem',
                'ItemStocks',
                'ItemCities',
                'ItemCities.Cities'
            ]
        );
        // pr($tmp);die();
        // pr('[getCartItems] WITHOUT_LOGIN_DATA');
        foreach ($tmp as $key => $value) {
            foreach($items as $key2 => $value2) {
                if ($value['id'] == $value2['item_id']) {
                    $item = [];
                    $item['item_id'] = $value['id'];
                    $item['name'] = $value['name'];
                    $item['url_slag'] = $value['url_slag'];
                    
                    $item['variant_name'] = $value['variant_name'];
                    $item['addon'] = $value2['addon'];
                    $item['qty'] = $value2['qty'];
                    $item['price'] = $value['item_city']['discount_price'];

                    $item['medium_image'] = $this->Special->getBaseImageUrl()
                                        .$item_img_model
                                        .$value['parent_item']['primary_photo_directory']
                                        ."/medium_"
                                        .$value['parent_item']['primary_photo'];

                    $item['small_image'] = $this->Special->getBaseImageUrl()
                                        .$item_img_model
                                        .$value['parent_item']['primary_photo_directory']
                                        ."/small_"
                                        .$value['parent_item']['primary_photo'];

                    $item['item_params'] = $value2['item_params'];
                    if ($value['parent_item']['shipping_charges'] == -1) {
                        //City Level Shipping Charges
                        $item['shipping_charges'] = $value['item_city']['city']['shipping_handling_charges'];
                    } else {
                        //Item Level Shipping charges
                        $item['shipping_charges'] = $value['parent_item']['shipping_charges'];
                    }
                    array_push($resultsArray, $item);
                }
            }
        }
        return $resultsArray;
    }

    /**
     *   Sync Cart Items with Table axi_cart_items
     *       $user_id is compulsory required.
     *
     * @param uuid $user_id User ID
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function syncCartItemsWithDB($user_id = null)
    {
        if ($user_id === null) {
            return false;
        }

        //Let's Merge Table Data and Cookie Array (Unique Values)
        //Don't Pass UserID to Get Cookies
        $arr_cookies = $this->getUserCart();

        // pr("WITHOUT_LOGIN_DATA");
        // pr($arr_cookies);
        //Pass UserID to Get TableData
        $arr_data = $this->getUserCart();

        // pr("LOGIN_DATA");
        // pr($arr_data);

        $arr_tmp = array(); //Temp Array to merge
        $temp = array();

        //First Get Cookies Items
        if (!empty($arr_cookies)) {
            foreach ($arr_cookies as $val) {
                $temp['item_id'] = $val['item_id'];
                $temp['qty'] = $val['qty'];
                $temp['addon'] = $val['addon'];
                $temp['item_params'] = $val['item_params'];
                array_push($arr_tmp, $temp);
            }
        }
        //Second Get Table Data Items
        foreach ($arr_data as $val) {
            $temp['item_id'] = $val['item_id'];
            $temp['qty'] = $val['qty'];
            $temp['addon'] = $val['addon'];
            $temp['item_params'] = $val['item_params'];
            array_push($arr_tmp, $temp);
        }

        //Now we got Both Merged.
        //Let's Find Unique Values Only.
        // $result = $this->Special->uniqueArray($arr_tmp, 'item_id');
        // pr("[syncCartItemsWithDB] Results Merged.");
        // pr($result);
        //Also Add same array in CartItems cookie.
        // $this->Cookie->write('CartItems', $result);
        $this->Special->setSession('CartItems', $result);
        return $this->insertCartItems($user_id, $result);
    }

    /**
     *   Insert Cart Items in Table: axi_cart_items
     *
     * @param uuid  $user_id   User ID
     * @param array $arr_items Unique Cart Items
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function insertCartItems($user_id = null, $arr_items = array())
    {
        //First Remove User ID Cart Items From axi_cart_items
        //Then Insert New Items

        //Model->TableName
        $cartitems = TableRegistry::get('CartItems');

        $cartitems->deleteAll(
            [
                'user_id' => $user_id
            ]
        );
        // pr("INSERT_CART_ITEMS");
        // pr($arr_items);
        //Existing Items Remove Now Let's Insert New Cart Items
        $crtItems = array();
        foreach ($arr_items as $val) {
            $tmp = array();
            $tmp['item_id'] = $val['item_id'];
            $tmp['user_id'] = $user_id;
            $tmp['quantity'] = $val['qty'];
            $tmp['addon'] = $val['addon'];
            $tmp['item_params'] = json_encode($val['item_params']);
            array_push($crtItems, $tmp);
        }
        // pr("INSERT_DATA");
        // pr($crtItems);
        $entities = $cartitems->newEntities($crtItems);
        if ($result = $cartitems->saveMany($entities)) {
            return true;
        }

        return false;
    }


    /**
     *   Remove Cart Item From Table axi_cart_items
     *
     * @param uuid $item_id Item ID
     * @param uuid $user_id User ID
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _removeCartItem($item_id = null, $user_id = null)
    {
        $cartitems = TableRegistry::get('CartItems');

        if ($item_id != null && $user_id != null) {
            $cartitems->deleteAll(
                [
                    'user_id' => $user_id,
                    'item_id' => $item_id
                ]
            );

            return true;
        }
        return false;
    }



    /**
     * Delete Cart Method
     *
     * @param uuid $user_id User ID
     *
     * @return void
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function deleteCart($user_id = null)
    {
        if ($user_id != null) {
            $cartitems = TableRegistry::get('CartItems');

            // CartItems
            if ($this->Special->checkSession('CartItems')) {
                $this->Special->deleteSession('CartItems');
            }

            //Remove User Cart Items From Db
            $cartitems->deleteAll(
                [
                'user_id' => $user_id
                ]
            );

            return true;
        } else {
            // CartItems
            if ($this->Special->checkSession('CartItems')) {
                $this->Special->deleteSession('CartItems');
            }
        }
        return false;
    }


    /**
     * Add Items To Cart Method
     * This will also create cart if it is not exists
     *
     * @param uuid    $item_id ItemID
     * @param integer $qty     Item Quantity
     * @param uuid    $user_id User ID
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function addItem($item_id = null, $qty = 1, $user_id = null, $addon = 0, $item_params = [])
    {
        $cartItems = array();

        $tmp_item = $this->Query->getDataById(
            'Items',
            [
                'id' => $item_id
            ]
        );
        if ($tmp_item == null) {
            return false;
        }
        //  pr('[AddItem] ItemID:');
        //  pr($item_id);
        $cartItems = $this->createCart();
        //  pr('[AddItem] CartItems:');
        //  pr($cartItems);
        //  die();
        if (sizeof($cartItems) < $this->getCartLimit()) {
            $tmp['item_id'] = $item_id;
            $tmp['addon'] = $addon;
            $tmp['item_params'] = $item_params;
            $chk = false;

            if (!empty($cartItems)) {
                $chk = $this->_checkItemsQty($cartItems, $item_id, $qty);
            }
            if (true) { //$chk == false
                $tmp['qty'] = $qty;
                array_push($cartItems, $tmp);
                // pr("[AddItem] AFTER PUSHING CartItems");
                // pr($cartItems);
            } else {
                $cartItems = $chk; //Updated Array Items
            }

            // $this->Cookie->write('CartItems', $cartItems);
            $this->Special->setSession('CartItems', $cartItems);

            //If User is logged In Then This will be called.
            if ($user_id != null) {
                // pr("USER_IS_LOGGED_IN [SessionCartItems]");
                // pr($this->Special->getSession('CartItems'));
                return $this->syncCartItemsWithDB($user_id);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     *  Check whether that item is already exists
     *  If it is already exists then Increase it's Qty.
     *
     * @param array   $arr_items Array Items
     * @param uuid    $item_id   Item ID
     * @param integer $qty       Quantity.
     *
     * @return integer
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _checkItemsQty($arr_items = array(), $item_id = null, $qty = 1)
    {
        $i = 0;
        foreach ($arr_items as $val) {
            if ($val['item_id'] == $item_id) {
                $arr_items[$i]['qty'] = 1;//$val['qty'] + $qty;
                return $arr_items;
            }
            $i++;
        }
        return false;
    }


    /**
     *   Delete Item From Cart
     *
     * @param uuid $item_id Item ID
     * @param uuid $user_id User ID - Optional
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function deleteItem($item_id = null, $user_id = null)
    {
        $cartItems = array();

        //getCurrent Items From Cart
        if ($user_id != null) {
            $cartItems = $this->getUserCart();
        } else {
            $cartItems = $this->getUserCart();
        }

        $newCartItems = array();
        foreach ($cartItems as $val) {
            if ($item_id != $val['item_id']) {
                $newCartItems[] = $val;
            }
        }
        // $this->Cookie->write('CartItems', $newCartItems);
        $this->Special->setSession('CartItems', $newCartItems);

        //If User is logged In Then This will be called.
        if ($user_id != null) {
            $this->_removeCartItem($item_id, $user_id);
            return $this->syncCartItemsWithDB($user_id);
        }
        return false;
    }


    /**
     *   Maximum No. of Items Cart Limit
     *
     * @return integer
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartLimit()
    {
        // $tmp = $this->Special->getConfigs('global');
        // if (isset($tmp['cart_items_limit'])) {
        //     return $tmp['cart_items_limit'];
        // } else {
            return 15;
        // }
    }

    /**
     *   No. of Items in Cart
     *
     * @param uuid $user_id User ID
     *
     * @return integer
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartItemCount($user_id = null)
    {
        if ($user_id == null) {
            $tmp = $this->getUserCart();
            if (!empty($tmp)) {
                return sizeof($tmp);
            } else {
                return 0;
            }
        } else {
            return sizeof($this->getUserCart());
        }
    }

    /**
    *   Get Cart Total
    *
    */
    public function getCartTotal($user_id = null)
    {
        $items = $this->getUserCart();

        $city = $this->Special->getCookie('selectedCity');
        $total = [];
        $total['grand_total'] = 0;
        $total['delivery_charges'] = 0;
        $total['discount'] = 0;
        $total['sub_total'] = 0;
        $total['eggless'] = 0;
        $total['midnight'] = 0;
        if (!empty($items)) {
            $flg = false;
            foreach ($items as $key => $value) {
                $total['sub_total'] += $value['price'];

                if ($value['item_params']['eggless'] == 1) {
                    $total['eggless'] += 100;
                }

                if ($value['item_params']['midnight'] == 1) {
                    $total['midnight'] = 250;
                    $flg = true;
                    $total['delivery_charges'] += $total['midnight'];                
                } else {
                    $total['delivery_charges'] += $value['shipping_charges'];                    
                }

                //------------------------------------------------------
                // $total['delivery_charges'] += $value['shipping_charges'];
            }

            // if ($flg == false) {
            //     //Normal
            //     $total['delivery_charges'] = $city['shipping_handling_charges'];
            // } else {
            //     //MidNight order
            //     $total['delivery_charges'] = 250; //isMidNight Order
            // }
            //+ $total['midnight']
            $total['grand_total'] = $total['sub_total'] + $total['eggless'] + $total['discount'] + $total['delivery_charges'] ;
        }


        return $total;
    }
}
?>
