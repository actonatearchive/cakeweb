<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   Paytm
 * @author    Divya Khanani <divya.khanani@actonate.com>
 * @copyright 2014-2016 Copyright (c) LetsShave Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;
error_reporting(0);

use Cake\Controller\Component;
use Cake\Network\Http\Client;
use Cake\Core\Configure;


/**
 * Paytm Component
 *
 * @category Component
 * @package  Paytm
 * @author   Divya Khanani <divya.khanani@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */

class PaytmComponent extends Component
{
    // public $PAYTM_DOMAIN="pguat.paytm.com";

    public $PAYTM_DOMAIN;

    public $PAYTM_MERCHANT_MID;
    public $PAYTM_MERCHANT_KEY;
    public $PAYTM_MERCHANT_WEBSITE;

    public $PAYTM_REFUND_URL="";
    public $PAYTM_STATUS_QUERY_URL="";
    public $PAYTM_TXN_URL="";
    public $params = [
      "MID"=>"",
      "ORDER_ID"=>"",
      "CUST_ID"=>"",
      //"INDUSTRY_TYPE_ID"=>"Retail116",
      "INDUSTRY_TYPE_ID"=> "Retail109",
      "CHANNEL_ID"=>"WEB",
      "TXN_AMOUNT"=>"",
      "EMAIL"=>"",
      "MOBILE_NO"=>"",
      "WEBSITE"=>"",
      "PAYTM_TXN_URL"=>"",
      "ORDER_DETAILS"=>"",
      "CALLBACK_URL"=>""

    ];

    /**
     *    Paytm construct
     *    DATE: 31th March 2017
     *
     * @return void
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function __construct()
    {
        $paytm_config = Configure::read('paytm');

        $this->PAYTM_DOMAIN=$paytm_config['PAYTM_DOMAIN'];

        $this->PAYTM_MERCHANT_MID = $paytm_config['PAYTM_MERCHANT_MID'];
        $this->PAYTM_MERCHANT_KEY = $paytm_config['PAYTM_MERCHANT_KEY'];
        $this->PAYTM_MERCHANT_WEBSITE = $paytm_config['PAYTM_MERCHANT_WEBSITE'];
        $this->params['INDUSTRY_TYPE_ID'] = $paytm_config['PAYTM_INDUSTRY_TYPE_ID'];
        $this->params['CHANNEL_ID'] = $paytm_config['PAYTM_CHANNEL_ID'];
        
    }

    public function verify_status($var1=null)
    {
        if($var1!=null){
        // REST API URL
        $var1['CHECKSUMHASH'] = urlencode($var1['CHECKSUMHASH']);
        $service_url = 'https://' . $this->PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/getTxnStatus';
        $service_url .= '?JsonData='.(json_encode($var1));
        $this->log($service_url);
        $curl = curl_init($service_url);
        $this->log($curl);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        if(!$curl_response = curl_exec($curl)){
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        }
        $de=json_decode($curl_response);
        $this->log('verify_status Response :.. ');
        
        $this->log($curl_response);
        $decoded = json_decode(json_encode($de),true);
        if(isset($decoded['STATUS']) && $decoded['STATUS']=='TXN_SUCCESS'){

            return true;
        }

        }
        return array();
    }
        
    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @return array
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function process()
    {

        $this->PAYTM_REFUND_URL
            ='https://' . $this->PAYTM_DOMAIN . '/oltp/HANDLER_INTERNAL/REFUND';
        $this->PAYTM_STATUS_QUERY_URL
            ='https://' . $this->PAYTM_DOMAIN . '/oltp/HANDLER_INTERNAL/TXNSTATUS';
        $this->PAYTM_TXN_URL
            ='https://' . $this->PAYTM_DOMAIN . '/oltp-web/processTransaction';
        $checkSum = "";
        $paramList = array();

        // Create an array having all required parameters for creating checksum.

        $paramList["MID"] = $this->PAYTM_MERCHANT_MID;
        $paramList["ORDER_ID"] = $this->params['ORDER_ID'];
        $paramList["ORDER_DETAILS"] = $this->params['ORDER_DETAILS'];

        $paramList["CUST_ID"] = $this->params['CUST_ID'];
        $paramList["INDUSTRY_TYPE_ID"] = $this->params['INDUSTRY_TYPE_ID'];
        $paramList["CHANNEL_ID"] = $this->params['CHANNEL_ID'];
        $paramList["TXN_AMOUNT"] = $this->params['TXN_AMOUNT'];
        $paramList["EMAIL"] = $this->params['EMAIL'];
        $paramList["MOBILE_NO"] = $this->params['MOBILE_NO'];
        $paramList["WEBSITE"] = $this->PAYTM_MERCHANT_WEBSITE;
        $paramList["CALLBACK_URL"] = $this->params['CALLBACK_URL'];
        
        $checkSum
            = $this->getChecksumFromArray($paramList, $this->PAYTM_MERCHANT_KEY);
        $paramList["PAYTM_TXN_URL"] = $this->PAYTM_TXN_URL;
        $paramList["CHECKSUMHASH"] = $checkSum;
        $this->log("request: ");
        
        $this->log($paramList);
        return $paramList;
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param array $data response data
     *
     * @return array
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    public function response($data)
    {
        
        $paytmChecksum = "";
        $paramList = array();
        $isValidChecksum = "FALSE";
        $paramList = $data;
        $paytmChecksum
            = isset($paramList["CHECKSUMHASH"]) ? $paramList["CHECKSUMHASH"] : "";
        //Sent by Paytm pg

        $isValidChecksum
            = $this->verifyChecksumE(
                $paramList, $this->PAYTM_MERCHANT_KEY, $paytmChecksum
            ); //will return TRUE or FALSE string.
        
        if ($isValidChecksum === true ) {
            
             // echo "<b>Checksum matched and following
             // are the transaction details:</b>" . "<br/>";
            $paramList_=array("MID" => $this->PAYTM_MERCHANT_MID , "ORDERID" => $paramList['ORDERID']);  
            $paramList_['CHECKSUMHASH']=$this->getChecksumFromArray($paramList_,$this->PAYTM_MERCHANT_KEY);
            $this->log('paramList');
            
            $this->log($paramList_);
            $isValidChecksum=$this->verify_status($paramList_);
            if ($isValidChecksum === true ) {
                
                return 1;

                // echo "<b>Transaction status is success</b>" . "<br/>";
                // Process your transaction here as success transaction.
                // Verify amount & order id received from Payment gateway
                // with your application's order id and amount.

            } else {
                return 0;
                // echo "<b>Transaction status is failure</b>" . "<br/>";
            }

            if (isset($paramList) && count($paramList) > 0 ) {
                foreach ( $paramList as $paramName => $paramValue ) {
                    echo "<br/>" . $paramName . " = " . $paramValue;
                }
            }
        } else {
             return 0;
             echo "<b>Checksum mismatched.</b>";
             // Process transaction as suspicious.
        }
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param string $input input
     * @param string $ky    key
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function encryptE($input, $ky)
    {
        $key = $ky;
        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $input = $this->pkcs5PadE($input, $size);
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $iv = "@@@@&&&&####$$$$";
        mcrypt_generic_init($td, $key, $iv);
        $data = mcrypt_generic($td, $input);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $data = base64_encode($data);
        return $data;
    }


    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param string $crypt crypt string
     * @param string $ky    key
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function decryptE($crypt, $ky)
    {

        $crypt = base64_decode($crypt);
        $key = $ky;
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $iv = "@@@@&&&&####$$$$";
        mcrypt_generic_init($td, $key, $iv);
        $decrypted_data = mdecrypt_generic($td, $crypt);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $decrypted_data = $this->pkcs5UnpadE($decrypted_data);
        $decrypted_data = rtrim($decrypted_data);
        return $decrypted_data;
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param string $text      text
     * @param string $blocksize size
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function pkcs5PadE($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }


    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param string $text text
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function pkcs5UnpadE($text)
    {
        $pad = ord($text{strlen($text) - 1});
        if ($pad > strlen($text)) {
            return false;
        }
        return substr($text, 0, -1 * $pad);
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param number $length length
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function generateSaltE($length)
    {
        $random = "";
        srand((double) microtime() * 1000000);

        $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
        $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
        $data .= "0FGH45OP89";

        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }

        return $random;
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param string $value vclue
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function checkStringE($value)
    {
        $myvalue = ltrim($value);
        $myvalue = rtrim($myvalue);
        if ($myvalue == 'null') {
            $myvalue = '';
        }
        return $myvalue;
    }


    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param arrayList $arrayList list
     * @param string    $key       key
     * @param number    $sort      sort method
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function getChecksumFromArray($arrayList, $key, $sort=1)
    {
        if ($sort != 0) {
            ksort($arrayList);
        }
        $str = $this->getArray2Str($arrayList);
        $salt = $this->generateSaltE(4);
        $finalString = $str . "|" . $salt;
        $hash = hash("sha256", $finalString);
        $hashString = $hash . $salt;
        $checksum = $this->encryptE($hashString, $key);
        return $checksum;
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param arrayList $arrayList     list
     * @param string    $key           key
     * @param string    $checksumvalue string
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function verifyChecksumE($arrayList, $key, $checksumvalue)
    {
        $arrayList = $this->removeCheckSumParam($arrayList);
        ksort($arrayList);
        $str = $this->getArray2Str($arrayList);
        $paytm_hash = $this->decryptE($checksumvalue, $key);
        $salt = substr($paytm_hash, -4);

        $finalString = $str . "|" . $salt;

        $website_hash = hash("sha256", $finalString);
        $website_hash .= $salt;

        $validFlag = "FALSE";
        if ($website_hash == $paytm_hash) {
            return true;
        } else {
            return false;
        }

    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param arrayList $arrayList list
     *
     * @return string
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function getArray2Str($arrayList)
    {
        $paramStr = "";
        $flag = 1;
        foreach ($arrayList as $key => $value) {
            if ($flag) {
                $paramStr .= $this->checkStringE($value);
                $flag = 0;
            } else {
                $paramStr .= "|" . $this->checkStringE($value);
            }
        }
        return $paramStr;
    }


    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param arrayList $paramList list
     * @param string    $key       key
     *
     * @return arrayList
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function redirect2PG($paramList, $key)
    {
        $hashString = $this->getchecksumFromArray($paramList);
        $checksum = $this->encryptE($hashString, $key);
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param arrayList $arrayList list
     *
     * @return arrayList
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function removeCheckSumParam($arrayList)
    {
        if (isset($arrayList["CHECKSUMHASH"])) {
            unset($arrayList["CHECKSUMHASH"]);
        }
        return $arrayList;
    }


    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param arrayList $requestParamList list
     *
     * @return arrayList
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function getTxnStatus($requestParamList)
    {
        return $this->callAPI(PAYTM_STATUS_QUERY_URL, $requestParamList);
    }

    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param arrayList $requestParamList list
     *
     * @return arrayList
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function initiateTxnRefund($requestParamList)
    {
        $CHECKSUM
            = $this->getChecksumFromArray($requestParamList, PAYTM_MERCHANT_KEY, 0);
        $requestParamList["CHECKSUM"] = $CHECKSUM;
        return $this->callAPI(PAYTM_REFUND_URL, $requestParamList);
    }


    /**
     *    Paytm
     *    DATE: 14th March 2017
     *
     * @param url       $apiURL           url
     * @param arrayList $requestParamList list
     *
     * @return arrayList
     * @author Divya Khanani <divya.khanani@actonate.com>
     */
    function callAPI($apiURL, $requestParamList)
    {
        $jsonResponse = "";
        $responseParamList = array();
        $JsonData =json_encode($requestParamList);
        $postData = 'JsonData='.urlencode($JsonData);
        $ch = curl_init($apiURL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData))
        );
        $jsonResponse = curl_exec($ch);
        $responseParamList = json_decode($jsonResponse, true);
        return $responseParamList;
    }
}

?>
