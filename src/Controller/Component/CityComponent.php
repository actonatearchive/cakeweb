<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   City
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * Special Component
 *
 * @category Component
 * @package  City
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
class CityComponent extends Component
{
    //Load Other Components
    public $components = ['Special','Query'];

    /**
     *  Getter Method For Cities Table
     *
     * @param $conditions array Pass conditions (where)
     * @param $fields     array Pass Fields (select)
     * @param $order      array Order By
     *
     * @return array
     */
    public function getCitiesData($conditions = [], $fields = ['id'], $order = [])
    {
        //Model->TableName
        $cities = TableRegistry::get('Cities');

        $tmp = $cities->find()
                ->where($conditions)
                ->select($fields)
                ->order($order)
                ->toArray();
        return $tmp;
    }

    /**
     *  Getter Method For Cities Table
     *
     * @param $id     uuid  ID (city Id)
     * @param $fields array Pass Fields (select)
     * @param $order  array Order By
     *
     * @return array
     */
    public function getCityData($id = null, $fields = ['id'], $order = [])
    {
        //Model->TableName
        $cities = TableRegistry::get('Cities');

        $tmp = $cities->findById($id)
                ->select($fields)
                ->order($order)
                ->first();
        return $tmp;
    }


    /**
    *   Get all active cities
    *
    * @return array
    */
    public function getAllActiveCities()
    {
        $cities = $this->getCitiesData(
            [
                'NOT' => [
                    'disabled'=>1
                ]
            ],
            [
                'id',
                'name'
            ],
            [
                'sequence'
            ]
        );
        return $cities;
    }


    /**
    *   Get Trending Cities
    *
    * @return array
    */
    public function getTrendingCities()
    {
        $start_date = date('Y-m-d h:m:s',strtotime('-30 days'));
        $end_date = date('Y-m-d H:m:s');

        //Model->TableName
        $orders = TableRegistry::get('Orders');

        $tmp = $orders->find('all')
                ->where(
                    [
                        'Orders.status IN' => [1,2,3],
                        'Orders.modified >=' => $start_date,
                        'Orders.modified <=' => $end_date,
                        'Cities.disabled !=' => 1 //That city should not be disabled.
                    ]
                )
                ->select([
                    'orders_count' => $orders->find()->func()->count('*'),
                    'ShippingAddresses.id',
                    'Cities.id',
                    'Cities.name'
                ])
                ->group('Cities.id')
                ->limit(8)
                ->order(['orders_count DESC'])
                ->contain(
                    [
                        'ShippingAddresses',
                        'ShippingAddresses.Cities'
                    ]
                )
                ->toArray();

        $resultsArray = [];
        foreach ($tmp as $key => $value) {
            $temp = [];
            $temp['orders_count'] = $value['orders_count'];
            $temp['id'] = $value['shipping_address']['city']['id'];
            $temp['name'] = $value['shipping_address']['city']['name'];

            array_push($resultsArray, $temp);
        }

        return $resultsArray;
    }


}

?>
