<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   User
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * User Component
 *
 * @category Component
 * @package  User
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
class UserComponent extends Component
{
    //Load Other Components
    public $components = ['Special','Query'];

    /**
    *  Get User Exists Or not
    *
    * @param uuid $username User Name
    *
    * @return boolean
    * @author Mohammed Sufyan <sufyan297@gmail.com>
    */
    public function userExists($username = null)
    {
        $tmp = $this->Query->getDataById(
            'Users',
            [
                'username' => $username
            ],
            [
                'id',
                'username'
            ]
        );

        if(empty($tmp)) {
            return false;
        }

        return $tmp;
    }

    

    /**
    *   Create User / Get User
    *
    * @return object
    */
    public function createUser($username = null, $name = null, $mobile = null, $city = null, $is_guest = 1)
    {
        $chk = $this->userExists($username);
        if ($chk != false) {
            //User Already Exists.
            //No Need to create another user
            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            // $this->log("CHECK");
            // $this->log($chk);
            //But Just Update Current One
            $nchk = $this->Query->setData(
                'Users',
                [
                    'id' => $chk['id'],
                    'first_name' => $name,
                    'mobile' => $mobile,
                    'city' => $city
                ]
            );
            // $this->log($nchk);

            return $nchk; //This will have UserData
        } else {
            $data = [];
            $data['id'] = $this->Special->UUIDv4();
            $data['username'] = $username;
            $data['first_name'] = $name;
            $data['last_name'] = '';
            $data['mobile'] = $mobile;
            $data['city'] = $city;
            $data['is_guest'] = $is_guest;

            $ret_data = $this->Query->setData('Users',$data);
            //DEBUGGING PURPOSE ONLY
            $this->log($ret_data);
            return $ret_data;
        }
    }

    /**
    *   Create Address (always create new address)
    *
    * @return object
    */
    public function createAddress($user_id = null, $address = [])
    {
        $address['id'] = $this->Special->UUIDv4();
        $ret_data = $this->Query->setData('UserAddresses',$address);
        $this->log($ret_data);
        return $ret_data;
    }


    /**
     *  Change Profile Info.
     *
     * @param uuid  $user_id User ID
     * @param array $details Details to update
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function changeProfileInfo($user_id = null,$details = array())
    {
        if ($user_id != null) {
            //Model->TableName
            $users = TableRegistry::get('Users');

            //Creating User Info Entity
            $user = $users->newEntity($details);
            $user->id = $user_id;

            if ($users->save($user)) {
                return $this->getUserData($user_id, []);
            } else {
                return "Oops! Something went wrong. Please try again later.";
            }
        }
        return "Oops! Something went wrong. Please try again later.";
    }

    /**
     *   Change User Password
     *
     * @param uuid   $user_id      User ID
     * @param string $password     User Current Password
     * @param string $new_password New Password
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function changePassword($user_id = null, $password = null,
        $new_password = null
    ) {

        if ($user_id != null) {
            //Model->TableName
            $users = TableRegistry::get('Users');

            $password = $this->Special->encryptPassword($password);

            $tmp = $users->find()
                ->select(
                    [
                        'username',
                        'password'
                    ]
                )
                ->where(
                    [
                        'id' => $user_id,
                        'password' => $password
                    ]
                )
                ->first();
            if (empty($tmp) || !isset($tmp)) {
                return "Invalid current password. Please try again.";
            } else {
                //Creating User Address Entity
                $user = $users->newEntity();
                $user->id = $user_id;
                $user->password = $new_password;

                if ($users->save($user)) {
                    return true;
                } else {
                    return "Oops! Something went wrong. Please try again later.";
                }
            }
        }
        return "Oops! Something went wrong. Please try again later.";
    }

    /**
     *   Get User Data
     *
     * @param uuid  $user_id User ID
     * @param array $fields  Selected fields to retrieve
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getUserData($user_id = null, $fields = array())
    {
        //Model->TableName
        $users = TableRegistry::get('Users');

        $tmp = $users->find()
            ->select($fields)
            ->where(
                [
                    'id'=>$user_id
                ]
            )
            ->first();
        return $tmp;
    }

    /**
     *  Get User Exists Or not
     *
     * @param uuid $user_id User ID
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function userExistsByID($user_id = null)
    {
        //Model->TableName
        $users = TableRegistry::get('Users');
        //Must declare Array
        $tmp = $users->findById($user_id)->first();
        if (empty($tmp)) {
            return false;
        } else {
            return true;
        }
        return $tmp;
    }

    /**
     *   Verify Reset Password Token
     *
     * @param string $token Token to Reset Password
     *
     * @return boolean
     */
    public function verifyResetToken($token = null)
    {
        //Model->TableName
        $users = TableRegistry::get('Users');

        $tmp = $users->find()
            ->select(
                [
                            'id',
                            'reset_token',
                            'username',
                            'first_name',
                            'last_name'
                        ]
            )
            ->where(
                [
                            'reset_token' => $token
                        ]
            )
            ->first();
        if (!empty($tmp)) {
            return $tmp;
        } else {
            return false;
        }
    }
    /**
     *  Get User Exists Or not
     *
     * @param string $username Username/Email
     * @param array  $fields   Fields
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getUserDataByUsername($username = null, $fields = ['id'])
    {
        //Model->TableName
        $users = TableRegistry::get('Users');
        //Must declare Array
        $tmp = $users->findByUsername($username)
            ->select($fields)
            ->first();
        if (empty($tmp)) {
            return false;
        } else {
            return $tmp;
        }
        return $tmp;
    }

    /**
     *  Set(Save) User Data
     *
     * @param uuid  $user_id User ID
     * @param array $fields  Fields
     *
     * @return array
     */
    public function setUserData($user_id = null, $fields = [])
    {
        //Model->TableName
        $users = TableRegistry::get('Users');

        $user = $users->newEntity($fields);
        $user->id = $user_id;
        if ($resp = $users->save($user)) {
            return $resp;
        } else {
            return false;
        }
    }
}

?>
