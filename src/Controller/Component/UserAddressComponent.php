<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   UserAddress
 * @author    Mohammed Sufyan Shaikh <sufyan297@gmail.com>
 * @copyright 2017 Copyright (c) Getsip PTE. LTD.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * UserAddress Component
 *
 * @category Component
 * @package  UserAddress
 * @author   Mohammed Sufyan Shaikh <sufyan297@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.getsip.sg/
 */

class UserAddressComponent extends Component
{

    public $components = ['Query','Special'];

    /**
    *  Get User Exists Or not
    *
    * @param uuid $username User Name
    *
    * @return boolean
    * @author Mohammed Sufyan <sufyan297@gmail.com>
    */
    public function userExists($username = null)
    {
        $tmp = $this->Query->getDataById(
            'Users',
            [
                'username' => $username
            ],
            [
                'id',
                'username'
            ]
        );

        if(empty($tmp)) {
            return false;
        }

        return $tmp;
    }


    /**
    *   Get User addresses
    *
    * @return array
    */
    public function getUserAddresses($user_id = null)
    {
        $user_addresses = $this->Query->getAllData(
            'UserAddresses',
            [
                'UserAddresses.user_id' => $user_id,
                'UserAddresses.is_active' => 1
            ]
        );

        return $user_addresses;
    }

    /**
    *   Create Address
    * @return array
    */
    public function createAddress($address = [])
    {
        if (!isset($address['id'])) {
            $address['id'] = $this->Special->UUIDv4();
        }
        $ret_data = $this->Query->setData('UserAddresses',$address);
        $this->log($ret_data);
        return $ret_data;
    }
}
?>
