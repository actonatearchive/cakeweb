<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   Item
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * Special Component
 *
 * @category Component
 * @package  Item
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
class ItemComponent extends Component
{
    //Load Other Components
    public $components = ['Special'];


    /**
     *  Getter Method For Items Table
     *
     * @param $conditions array Pass conditions (where)
     * @param $fields     array Pass Fields (select)
     * @param $order      array Order By
     *
     * @return array
     */
    public function getItemsData($conditions = [], $fields = ['id'],
        $order = [], $limit = false
    ) {
        //Model->TableName
        $items = TableRegistry::get('Items');

        $query = $items->find();
        if ($limit !== false) {
            $query->limit($limit);
        }
        $tmp = $query->where($conditions)
                ->select($fields)
                ->order($order)
                ->toArray();
        return $tmp;
    }

}

?>
