<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Category
 * @package   Category
 * @author    Divya Khanani <divya.khanani@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Category Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Category
 * @package  Category
 * @author   Divya Khanani <divya.khanani@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.cakestudio.in/
 */
 class CategoryController extends AppController
 {
    public $components = ['City','Special','Query','CakeWebCategory'];

     /**
     *   Initialization
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();

    }


    /**
     * Index Method
     *
     * Used for Category Page View.
     *
     * @return void
     */
    public function index($category = null, $sub_cat = null)
    {
        
        $this->viewBuilder()->layout("base_layout");
        // pr($this->request->params);
        $sort_by = $this->request->getQuery('sort_by');

		if (empty($sort_by)) {
			$sort_by = "a_z";
        }

        
        $categories = $this->CakeWebCategory->getCategory($category);
        
        // pr($categories);
        // $pre_cakes = $this->CakeStudioHome->getFeaturedCakes('premium', $this->selectedCity['id']);
        // $birth_cakes = $this->CakeStudioHome->getFeaturedCakes('birthday', $this->selectedCity['id']);
        // $design_cakes = $this->CakeStudioHome->getFeaturedCakes('designer', $this->selectedCity['id']);

        $category_listing = $this->CakeWebCategory->getCategoryCakes($categories['id'], $this->selectedCity['id'], 16, 0, $sort_by);


        // pr($category_listing); die('ok');
        // $this->set('premium_cakes', $pre_cakes);
        // $this->set('birthday_cakes', $birth_cakes);
        // $this->set('designer_cakes', $design_cakes);
        // pr($category_listing);die();
        $this->set('category', $categories);
        $this->set('category_listing', $category_listing);
        $this->set('url_slag', $category);
        $this->set('sub_cat', $sub_cat);
        $this->set('sort_by', $sort_by);

        // $this->set('banners', $banners);
        $this->set('page_title', $categories['name']." in ".$this->selectedCity['name']);

        $this->set('page_title', 'Order Birthday, Wedding, Anniversary Cakes Online');
    }

    
 }
