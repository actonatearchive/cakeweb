<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Users
 * @package   Users
 * @author    Divya Khanani <divya.khanani@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * Users Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Users
 * @package  Users
 * @author   Divya Khanani <divya.khanani@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.cakeweb.in/
 */
 class UsersController extends AppController
 {
    public $components = ['CakeWebCart','Special','CakeWebHome','CakeWebCategory','Order'];

    /**
    *   Initialization
    *
    * @return void
    */
    public function initialize()
    {
        // Always enable the CSRF component.
        // $this->loadComponent('Csrf');
        parent::initialize();
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
        ]);
        $this->Auth->allow(
            [
                'login','register',
                'socialLogin',
                'subscribeUser',
                'cart',
                'addToCart',
                'addAddon',
                'account',
                'removeFromCart',
                'resetPasswordLink',
                'resetPassword'
            ]
        );

        //loadTheComponents
        $this->loadComponent('User');
        $this->loadComponent('Special');
        $this->loadComponent('Query');

    }

    public function account()
    {
        $this->viewBuilder()->layout("base_layout");

    }

    /**
    *   Cart
    *
    */
    public function cart()
    {
        $this->viewBuilder()->layout("base_layout");

        $cart_items = $this->CakeWebCart->getUserCart();
        // pr($cart_items);

        $cart_item_count = $this->CakeWebCart->getCartItemCount();

        if ($cart_item_count <= 0) {
            $this->Flash->error(
                __(
                    'No items in your cart.'
                )
            );

            return $this->redirect(['controller'=>'home','action'=>'index']);
        }
        $this->set('cart_items', $cart_items);
        $this->loadComponent('UserWallet');
        $this->set('points',$this->UserWallet->getUserPoints($this->user_id));
        // $tmp2 = $this->CakeWebCart->addItem('52f8e00e-248c-494c-9bad-30e5adc6fdca',1,'1'); //,'55a6d89e-5e44-48af-bedf-4ed96ab91d3e'
        //getAddOns - Flowers
        $addon_flowers = $this->CakeWebCategory->getCategoryCakes('5a4cb700-1d64-4549-b56b-43976aba72de', $this->selectedCity['id']);
        //getAddOns - Chocolates
        $addon_chocolates = $this->CakeWebCategory->getCategoryCakes('5a4cb77c-e504-4290-9b8b-4ca06aba72de', $this->selectedCity['id']);
        //getAddOns - Gifts
        $addon_gifts = $this->CakeWebCategory->getCategoryCakes('5a4cb843-a6d4-40dc-a590-41b66aba72de', $this->selectedCity['id']);

        //Addon Items
        $this->set('addon_flowers', $addon_flowers);
        $this->set('addon_chocolates', $addon_chocolates);
        $this->set('addon_gifts', $addon_gifts);


        // pr($tmp);
        // pr($tmp2);
        // die();
    }

    /**
    *   Add To Cart
    *
    * @return redirection
    */
    public function addToCart()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            //Check For Quantity
            //----------------------------------------------------
            if ($data['qty'] < 1) {
                $data['qty'] = 1;
            }

            $adata = [];
            $adata['delivery_date'] = $data['delivery_date'];
            $adata['delivery_message'] = $data['message_cake'];
            $adata['optional_message'] = $data['optional_message'];
            $adata['eggless'] = $data['egg_less'];
            $adata['midnight'] = $data['midnight'];

            $this->CakeWebCart->addItem($data['item_id'], $data['qty'], null,'0',$adata);

            // $tmp['qty'] = $tmp['temp_qty'];
            $this->Flash->success(
                __(
                    'Item added to cart.'
                )
            );
            return $this->redirect(
                ['controller' => 'users', 'action' => 'cart','#'=>'addons']
            );
        } else {
            return $this->redirect($this->referer());
        }
    }


    /**
    *   Add Addons
    *
    * @return json
    */
    public function addAddon()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $this->log($data['items']);

            $adata = [];
            $adata['delivery_date'] = null;
            $adata['delivery_message'] = null;
            $adata['optional_message'] = null;
            $adata['eggless'] = null;
            $adata['midnight'] = null;

            foreach ($data['items'] as $key => $value) {
                $this->CakeWebCart->addItem($value['item_id'], 1, null,'1', $adata);
            }

            $res = new ResponseObject();
            $res -> status = 'success' ;
            $res -> message = 'Add on added.' ;
            $this -> response -> body(json_encode($res));
            return $this -> response ;
        }
    }

    /**
    *   Remove From Cart
    *
    * @return redirection
    */
    public function removeFromCart($item_id = null)
    {
        $this->CakeWebCart->deleteItem($item_id, null);
        $this->Flash->success(
            __(
                'Item removed from cart.'
            )
        );
        return $this->redirect($this->referer());
    }


    /**
    *   Change Qty
    *
    */
    public function changeQty()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $cart = $this->Cart->addItem($data['item_id'], $data['qty']);//pass - user_id

            $cart_item = $this->Cart->getCartItemById($data['item_id']);//pass - user_id

            $cart_total = $this->Cart->getCartTotal(); //pass - user_id

            $res = new ResponseObject();
            $res -> status = 'success' ;
            $res -> data = $cart_item ;
            $res -> cart_total = $cart_total ;
            $res -> message = 'Cart qty changed.' ;
            $this -> response -> body(json_encode($res));
            return $this -> response ;
        }
    }

    /**
    *   Order Review
    *
    * @return redirect
    */
    public function orderReview()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $order_data = [];
            $order_data['id'] = $data['order_id'];
            $order_data['review'] = $data['review'];
            $this->Order->updateOrder($order_data);

            $this->Flash->success(
                __(
                    'Your feedback is submitted.'
                )
            );
            return $this->redirect(['controller'=>'home','action'=>'index']);
        }
    }


    /**
     * Login Method
     *
     * Used for Home Page View.
     *
     * @return void
     */
    public function login()
    {
        if ($this->user_id != null) {
            $this->Flash->error(__('You are already logged in!'));
            return $this->redirect([
                'controller' => 'home',
                'action' => 'index'
            ]);
        }
        //HeaderComponent
        $this->set('page_title',"Login");
        $this->viewBuilder()->layout("base_layout");
        // pr($this->Auth->user());
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                // return $this->redirect($this->Special->getSession('lastUrl'));


                return $this->redirect($this->Special->getSession('lastUrl'));
            }
            $this->Flash->error(__('Invalid username or password, try again'));
            return $this->redirect($this->referer().'#signin');
        }
    }

    public function index()
    {
        $this->viewBuilder()->layout("base_layout");
        $this->set('page_title',"User");

        // pr($this->Auth->user());
    }

    public function myOrders()
    {
        $this->viewBuilder()->layout("base_layout");
        $orders = $this->Order->getUserOrders($this->user_id);
        // pr($orders);die();
        $this->loadComponent('UserWallet');
        $this->set('points',$this->UserWallet->getUserPoints($this->user_id));
        $this->set('orders',$orders);
        
        $this->set('user',$this->user);
        $this->set('page_title',"User");

    }

    public function profile()
    {
        $this->viewBuilder()->layout("base_layout");
        
        $this->set('user',$this->user);
        $this->set('page_title',"User");

    }

    /**
     *   Change User Profile
     *
     * @return json
     */
    public function changeProfileInfo()
    {
        if ($this->request->is('post') && $this->user_id != null) {
            $data = $this->request->data;

            $this->loadComponent('User');
            $user = $this->User->changeProfileInfo($this->user_id, $data);
            $this->Auth->setUser($user);
            $this->Flash->success(__('Profile Info successfully changed'));
            $this->redirect($this->referer());
            
        } else {
            $this->Flash->error(__('Some error occured try latter.'));
            $this->redirect($this->referer());
        }
    }

    public function register()
    {
        if ($this->user_id != null) {
            $this->Flash->error(__('You are already logged in!'));
            return $this->redirect([
                'controller' => 'home',
                'action' => 'index'
            ]);
        }

        // $this->viewBuilder()->layout("base_layout");

        // $this->set('page_title',"Register");
        $user = $this->Users->newEntity($this->request->getData());

        $user->is_verified = 0;
        $user->ip_address = $this->request->clientIp();

        if ($this->request->is('post')) {
            if (!$this->User->userExists($user['username'])) {

                if ($resp = $this->Users->save($user)) {
                    $this->Auth->setUser($user->toArray());
                    $this->Flash->success(__('Account successfully created.'));


                    //Send Mail Here

                    //----------------------------
                    // return $this->redirect([
                    //     'controller' => 'home',
                    //     'action' => 'index'
                    // ]);
                    return $this->redirect($this->Special->getSession('lastUrl'));
                }
            } else {
                $this->Flash->error(__('Username already exists.'));
                return $this->redirect($this->referer().'#register');
            }
        }
        $this->set('user', $user);
    }

    public function subscribeUser()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->data;

            if (isset($data['email'])) {
                
                $subscriber = $this->Query->getDataById(
                    'Subscribers',
                    [
                        'Subscribers.email' => $data['email']
                    ]
                );

                if (!empty($subscriber)) {
                    $data['id'] = $subscriber['id'];
                } else {
                    $data['id'] = $this->Special->UUIDv4();
                }
                
                //Add This user to our subscribers list
                $this->Query->setData(
                    'Subscribers',
                    [
                        'id' => $data['id'],
                        'email' => $data['email'],
                        'user_id' => $this->user_id,
                        'is_active' => 1
                    ]
                );

                // $res = new ResponseObject();
                // $res -> status = 'success' ;
                // $res -> message = 'You are successfully subscribed.' ;
                // $this -> response -> body(json_encode($res));
                // return $this -> response ;
                $this->Flash->success(__('You are successfully subscribed.'));
                return $this->redirect($this->referer());
            }
            //ShowError for Username not defined.
        }
        $this->Flash->error(__('Oops! Something went wrong.'));
        return $this->redirect(['controller'=>'home','action'=>'index']);
    }

    public function socialLogin()
    {

        //Operation After Social Login
        //will be done here...
        return $this->redirect($this->Special->getSession('lastUrl'));
    }

    public function logout()
    {
        $this->Auth->logout();
        return $this->redirect(['controller' => 'home', 'action' => 'index']);
    }

    /**
     *   Change User Password
     *
     * @return boolean/error
     */
    public function changeUserPassword()
    {
        if ($this->request->is('post') && $this->user_id != null) {
            $data = $this->request->data;

            $this->loadComponent('User');
            $chk = $this->User->changePassword(
                $this->user_id,
                $data['password'],
                $data['newpassword']
            );

            if ($chk === true) {
                $res = new ResponseObject();
                $res -> status = 'success' ;
                $res -> code = 200;
                $res -> message = 'Password successfully changed.' ;
                $this -> response -> body(json_encode($res));
                return $this -> response ;
            } else {
                $res = new ResponseObject();
                $res -> status = 'error' ;
                $res -> code = $this->error_code;

                $res -> message = $chk ;
                $this -> response -> body(json_encode($res));
                return $this -> response ;
            }
        } else {
            $res = new ResponseObject();
            $res -> status = 'error' ;
            // $res -> data = $tmp;
            $res -> code = $this->error_code;

            $res -> message = 'INVALID METHOD.' ;
            $this -> response -> body(json_encode($res));
            return $this -> response ;
        }
    }


    /**
     *   Generate Reset Password Link
     *
     * @return boolean
     */
    public function resetPasswordLink()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $this->loadComponent('User');
            $this->loadComponent('Special');
            $user = false;
            $user = $this->User->getUserDataByUsername(
                $data['username'],
                [
                    'id',
                    'username',
                    'first_name',
                    'last_name',
                    'mobile',
                    'is_guest'
                ]
            );
            if (!empty($user) && empty($user['is_guest'])) {
                $token = $this->Special->generateString(36);
                //Generate Token and save to user account
                //Send Email To User with that Token

                $res = $this->User->setUserData(
                    $user['id'],
                    [
                        'reset_token' => $token
                    ]
                );
                $user['reset_token'] = $token;

                if ($res != false) {
                    $this->loadComponent('Email');
                    $link = 'https://www.cakeweb.in/users/resetPassword/';
                    $link .= $user['reset_token'];
                    $user['link'] = $link;
                    $this->Email->send(
                        'forgetpassword',
                        $user['username'],
                        'Reset Password',
                        [
                            'user'=>$user
                        ]
                    );
                    if ($user['mobile'] != null) {
                        $this->loadComponent('SMS');
                        $user['link'] = $this->Special->getShortUrl($link);
                        $this->SMS->sendResetPassword($user);

                    }
                    $this->Flash->success(
                        __(
                            "Reset password link has been sent to your mail
                            and mobile."
                        )
                    );
                    return $this->redirect(
                        [
                        'controller' => 'Users',
                        'action' => 'account#resetLink'
                        ]
                    );
                } else {
                    $this->Flash->error(
                        __('Oops! Something went wrong. Please try again later.')
                    );
                    return $this->redirect(
                        [
                        'controller' => 'Users',
                        'action' => 'account#forgetpassword'
                        ]
                    );
                }

            } else {
                $this->Flash->error(
                    __('No account associated with that Email address.')
                );
                return $this->redirect(
                    [
                    'controller' => 'Users',
                    'action' => 'account#forgetpassword'
                    ]
                );
            }
        }
    }



    /**
     *   Forgot Password
     *
     * @param string $reset_token Reset Token
     *
     * @return void
     */
    public function resetPassword($reset_token = null)
    {
        if ($reset_token != null) {
            $user = $this->User->verifyResetToken($reset_token);

            if ($user != false) {
                $this->viewBuilder()->layout("base_layout");

                if ($this->request->is('post')) {
                    $data = $this->request->data;

                    if ($data['password'] == $data['rtpassword']) {
                        $this->loadComponent('User');

                        $res = $this->User->setUserData(
                            $user['id'],
                            [
                                'password' => $data['password']
                            ]
                        );

                        if ($res != false) {
                            $this->loadComponent('Email');

                            $this->Email->send(
                                'resetpassword',
                                $user['username'],
                                'Password has been reset',
                                [
                                    'user'=>$user
                                ]
                            );

                            $this->Flash->success(
                                __(
                                    "Password has been reset.
                                What are you waiting for? Login Now!!"
                                )
                            );
                            return $this->redirect(
                                [
                                'controller' => 'Users',
                                'action' => 'account#sigin'
                                ]
                            );
                        } else {
                            $this->Flash->error(
                                __(
                                    'Oops! Something went wrong.
                                Please try again later.'
                                )
                            );
                            return $this->redirect(
                                [
                                'controller' => 'Users',
                                'action' => 'account#sigin'
                                ]
                            );
                        }
                    }
                }
            } else {
                $this->Flash->error(
                    __('Oops! Something went wrong. Please try again later.')
                );
                return $this->redirect(
                    [
                    'controller' => 'Home',
                    'action' => 'index'
                    ]
                );
            }
        } else {
            return $this->redirect(
                [
                'controller' => 'Home',
                'action' => 'index'
                ]
            );
        }
    }
}
