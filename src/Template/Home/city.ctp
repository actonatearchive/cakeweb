<?= $this->Flash->render() ?>
<style>
  
  .cities_list_div_all {
    overflow: scroll;
    height: 150px;
  }
  </style>
  <script>
    var cities = JSON.parse('<?= json_encode($cities) ?>');
    
    function selectCity(event) {
      // event.preventDefault();
      var city_name = document.getElementById('city_name').value;
      
      for(i=0;i<cities.length;i++){
        
        if(cities[i].name == city_name){
          setCity(cities[i].id);
          break;
        }
        
      }
    }
  </script>
    
<div class="city_main_sec">
    <div class="city_main_container w-container">
      <div class="c_logo_div">
        <?= $this->Html->image('cakeWeb_Logo_New_08.png', [
          'alt' => 'CakeWeb','width'=>'180',
          'srcset'=> $this->request->webroot.'img/cakeWeb_Logo_New_08-p-500.png 500w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08-p-800.png 800w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08-p-1080.png 1080w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08-p-1600.png 1600w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08.png 1775w', 
          'sizes'=>'(max-width: 479px) 29vw, (max-width: 991px) 140px, 180px',
          'class'=>"c_top_logo"
          
          ]) 
        ?>
      
      </div>
      <div class="text-block-12">Hi welcome, please select a city you wish to deliver the cake.</div>
      <div class="find_text_div">
          <?= $this->Html->image('001-placeholder-1.svg', ['alt' => 'CakeWeb','class'=>'image-14','width'=>'40']) ?>
          
        <div class="city_text">find your city</div>
      </div>
      <div class="seach_form_blk w-form">
        <form id="email-form" name="email-form" data-name="Email Form" class="city_blk">
          <div class="adm-inputbox-div">
            <div class="search_icon_div">
                <?= $this->Html->image('search-black.svg', ['alt' => 'CakeWeb','class'=>'image-13','width'=>'25']) ?>
                
            </div>
            <input type="text"  list="city" class="adm-login-inpbox w-input"  autocomplete="off"  maxlength="256" name="name-2" data-name="Name 2" placeholder="Search for your city" id="city_name" required>
            
            <input type="hidden" name="city_id" id="city_id">
            
            <input type="submit" value="GO" class="go_btn w-button" onClick="selectCity(event)">
            </div>
        
            <datalist id="city">
            <?php foreach ($cities as $key => $value): ?>
              <option title="<?= $value['id'] ?>" value="<?= $value['name'] ?>" >
            <?php endforeach; ?>   
            </datalist>
        </form>
        <!-- <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form.</div>
        </div> -->
      </div>
      <div class="find_text_div">
          <?= $this->Html->image('002-placeholder.svg', ['alt' => 'CakeWeb','class'=>'image-15','width'=>'40']) ?>

        <div class="city_text">popular cities</div>
      </div>
      <div class="cities_list_div">
        
        <?php foreach ($cities as $key => $value): ?>
            <div class="city_name_div">
               <div class="text-block-13">
                <span class="pointer"  onClick="setCity('<?= $value['id'] ?>')"><?= $value['name'] ?></span>
              </div>
            </div>
        <?php endforeach; ?>
        
      </div>
      <div class="view_more_cake_div">
          <a href="#" class="view_more_button w-button">view more cities</a>
    </div>
    </div>
    <!-- <div class="c_bg_align_div">
        <?= $this->Html->image('elements.png', [
                    'alt' => 'CakeWeb','class'=>'image-12',
                    'srcset'=> $this->request->webroot."img/elements-p-500.png 500w, ".$this->request->webroot."img/elements.png 628w",
                    'size' => '100vw'
                    
                ]
            ) 
        ?>

    </div> -->
  </div>

  <script>
  $('.view_more_cake_div').click(function () {
    $(this).hide();
    $('.cities_list_div').addClass('cities_list_div_all');
  });
  </script>