<!-- <div class="header_features">
  <div class="header_features_container w-container">
    <div class="header_availability_div">
      <div class="header_feature_tittle">availbality</div>
      <div class="header_features_info">12 x 7 | 9 am - 9 pm</div>
    </div>
    <div class="features_contact_div">
      <div class="header_feature_tittle">contact us</div>
      <div class="header_features_info">72028 73333</div>
    </div>
    <div class="header_availability_div">
      <div class="header_feature_tittle">free standard delivery</div>
      <div class="header_features_info">12 pm - 8 pm</div>
    </div>
  </div>
</div> -->
  <div data-animation="slide" data-duration="500" data-infinite="1" class="banner_slider w-slider mt-130">
    <div class="w-slider-mask">
    <?php foreach ($banners as $key => $value): ?>

      <div class=" slide1 w-slide" style="background-image: url(<?= $value['full_image']?>);"></div>
    <?php endforeach; ?>
      <!-- <div class="w-slide"></div> -->
    </div>
    <div class="w-slider-arrow-left">
      <div class="w-icon-slider-left"></div>
    </div>
    <div class="w-slider-arrow-right">
      <div class="w-icon-slider-right"></div>
    </div>
    <div class="slide-nav w-slider-nav w-round"></div>
  </div>
  <div class="hm_main_section">
    <div class="hm_main_container w-container">
      <div class="trending_div">
        <div class="trending_text">trending cakes</div>
      </div>
      <div class="tab_section">
        <div data-duration-in="300" data-duration-out="100" class="trending_tabs w-tabs">
          <div class="trending_tabs_menu w-tab-menu">
            <a data-w-tab="Tab 1" class="cake_category w-inline-block w-tab-link">
              <div class="text-block-3">Featured</div>
            </a>
            <a data-w-tab="Tab 2" class="cake_category w-inline-block w-tab-link w--current">
              <div class="text-block-4">special</div>
            </a>
            <a data-w-tab="Tab 3" class="cake_category w-inline-block w-tab-link">
              <div class="text-block-5">best sellers</div>
            </a>
            <!-- <a data-w-tab="Tab 4" class="cake_category w-inline-block w-tab-link">
              <div class="text-block-6">gift &amp; flowers</div>
            </a> -->
          </div>
          <div class="w-tab-content">
            <div data-w-tab="Tab 1" class="w-tab-pane w--tab-active">
              <div class="card_trending_div">
                <?php foreach($featured as $item): ?>
                  
                  <?= $this->element('item_box', ['item'=>$item]) ?>

                <?php endforeach; ?>
                <div class="view_more_cake_div"><a href="#" class="view_more_button w-button"  onclick="form_submit('#search-form')">view more cakes</a></div>
              </div>
            </div>
            <div data-w-tab="Tab 2" class="w-tab-pane ">
              <div class="card_trending_div">
                <?php foreach($special as $item): ?>
                  
                  <?= $this->element('item_box', ['item'=>$item]) ?>

                <?php endforeach; ?>
                <div class="view_more_cake_div"><a href="#" class="view_more_button w-button"  onclick="form_submit('#search-form')">view more cakes</a></div>
              </div>
            </div>
            <div data-w-tab="Tab 3" class="w-tab-pane">
              <div class="card_trending_div">
                <?php foreach($best_seller as $item): ?>
                  
                  <?= $this->element('item_box', ['item'=>$item]) ?>

                <?php endforeach; ?>
                <div class="view_more_cake_div"><a href="#" class="view_more_button w-button"  onclick="form_submit('#search-form')">view more cakes</a></div>
              </div>
            </div>
            <!-- <div data-w-tab="Tab 4" class="w-tab-pane">
              <div class="card_trending_div">
                
                <div class="view_more_cake_div"><a href="#" class="view_more_button w-button" onclick="form_submit('#search-form')">view more cakes</a></div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>


<?= $this->element('subscribe') ?>
<?= $this->element('benefits') ?>
