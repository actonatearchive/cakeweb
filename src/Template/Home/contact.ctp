  <div class="contaxt_sec">
    <div class="con_container w-container">
      <div class="con_tittle">
          <?= $this->Html->image('001-operator.svg', [
                'alt' => 'CakeWeb',
                'class'=>"image-16",
                'width'=>'50'            
                ]) 
            ?>
        <div class="text-block-14">Help us to understand your concern</div>
      </div>
      
      <!-- <div class="w-form">
        <form id="email-form-2" name="email-form-2" data-name="Email Form 2">
          <div class="con_state_div">
            <div class="sh_city_input_div"><input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="John" id="name-4">
              <div class="first_name_latel">Name *</div>
            </div>
            <div class="sh_city_input_div"><input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="98545 11111" id="name-4">
              <div class="first_name_latel">Phone *</div>
            </div>
            <div class="sh_city_input_div"><input type="email" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="john@example.com" id="name-4">
              <div class="first_name_latel">email*</div>
            </div>
          </div><textarea id="field" name="field" maxlength="5000" placeholder="Message" class="con_msg_box w-input"></textarea></form>
      </div> -->

      <!-- <div class="view_more_cake_div"><a href="#" class="con_submit_button w-button">Send Message</a></div>
       -->
      <div class="con_div">
        <div class="con_call_card_design">
          <div class="con_card_type_img_div">
              <!-- <img src="images/002-technical-support-line.svg" class="con_call_card_img"> -->
            <?= $this->Html->image('002-technical-support-line.svg', [
                'alt' => 'CakeWeb',
                'class'=>"con_call_card_img"            
                ]) 
            ?>

            <div class="con_call_text">
              Call  Us
            </div>
            </div>
            <a href="tel:07202873333" class="con_call_no">72028 73333</a>
                <br>
            <a href="tel:7202843333" class="con_call_no">9am - 11pm </a>
            
        </div>
        <div class="con_call_card_design">
          <div class="con_card_type_img_div">
            <?= $this->Html->image('001-operator.svg', [
                'alt' => 'CakeWeb',
                'class'=>"con_call_card_img"            
                ]) 
            ?>
            <div class="con_call_text">Address</div>
          </div>
          <div class="con_call_no">

                SB-3, Janmotri Appartment,<br>
                Opp. Panchratne Building,<br>
                Ellora Park, <br>
                Vadodara-390023
            </div>
        </div>
         <div class="con_call_card_design">
          <div class="con_card_type_img_div">
            <?= $this->Html->image('24-hours.svg', [
                'alt' => 'CakeWeb',
                'class'=>"con_call_card_img"            
                ]) 
            ?>
            <div class="con_call_text">Mail Us</div>
          </div>
          <div class="con_call_no"> info@cakeweb.in </div>
          
          <!-- <div class="con_call_no">12Hr x 7D <br> 9am - 9pm</div> -->
        </div>
      </div>
    </div>
  </div>