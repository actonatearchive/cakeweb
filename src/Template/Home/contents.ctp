<div class="policy_pages_msin_section">
    <div class="policy_pages_container w-container">
      <div class="tab_section">
        <div data-duration-in="300" data-duration-out="100" class="trending_tabs w-tabs">
          <div class="trending_tabs_menu w-tab-menu">
            <a data-w-tab="Tab about-us" class="cake_category w-inline-block w-tab-link" href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','about-us'])?>">
              <div class="text-block-3">About Us</div>
            </a>
            <a data-w-tab="Tab faqs" class="cake_category w-inline-block w-tab-link" href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','faqs'])?>">
              <div class="text-block-4">FAQs</div>
            </a>
            <a data-w-tab="Tab privacy-policy" class="cake_category w-inline-block w-tab-link" href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','privacy-policy'])?>">
              <div class="text-block-5">Privacy</div>
            </a>
            <a data-w-tab="Tab terms-conditions" class="cake_category w-inline-block w-tab-link" href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','terms-conditions'])?>">
              <div class="text-block-6">T&amp;C</div>
            </a>
          </div>
          
          <div class="tabs-content-2 w-tab-content">
            <div data-w-tab="Tab <?= $content['alias'] ?>" class="tab-pane w-tab-pane w--tab-active">
              <div class="about_tab_div-copy">
                <div class="policy_pages_tittle"><?= $content['title'] ?></div>
                
                <div class="about_pages_info"><?=$content['description'] ?></div>
                </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>