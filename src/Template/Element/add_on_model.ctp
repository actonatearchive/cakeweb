  <div class="addons_wrapper_div " id="cs-addons" style="display:none">
    <div class="sdd_on_div">
      
      <a href="#" class="close_link" data-ix="close-addon">X</a>
      <div class="tab_section">
        <div data-duration-in="300" data-duration-out="100" class="addons_tabs w-tabs">
          <div class="trending_tabs_menu w-tab-menu">
            <a data-w-tab="Tab 1" class="cake_category w-inline-block w-tab-link w--current">
              <div class="text-block-3">Flowers</div>
            </a>
            <a data-w-tab="Tab 2" class="cake_category w-inline-block w-tab-link">
              <div class="text-block-4">Chocolate</div>
            </a>
            <a data-w-tab="Tab 3" class="cake_category w-inline-block w-tab-link ">
              <div class="text-block-5">Gifts</div>
            </a>
          </div>
          <div class="tabs-content-2 w-tab-content">
            <div data-w-tab="Tab 1" class="tab-pane w-tab-pane w--tab-active">
              <div class="owl-carousel">
                <?php
                  foreach($addon_flowers as $key => $item): ?>
                    <div class="w-checkbox p-30 pl-0 t_cen"> 
                      <label for="addon_<?= $item['child_items'][0]['id']?>" class="field-label w-form-label">
                        <?= $this->Html->image($item['medium_image'], ['alt' => 'flower','class'=>'ad_img']) ?> 
                        <div class="ad_item_name" > <?= $item['name'] ?> </div>
                        <div class="ad_item_price"><span class="rup_10"> ₹</span> <?=$item['child_items'][0]['discount_price']?> </div>
                        <input type="checkbox" id="addon_<?= $item['child_items'][0]['id']?>" name="checkbox" data-name="Checkbox" class="w-checkbox-input" ng-click="toggleAddOn('<?= $item['child_items'][0]['id']?>')">
                        <span class="checkmark ad-chk"></span> 
                      </label>
                      
                    </div>
                <?php
                  endforeach;
                ?>
                <!-- <div> 2 </div>
                <div> 3 </div>
                <div> 4 </div>
                <div> 5 </div>
                <div> 6 </div>
                <div> 7 </div> -->
              </div>
             
            </div>
            <div data-w-tab="Tab 2" class="tab-pane-2 w-tab-pane"> 
              <div class="owl-carousel">
                <?php
                  foreach($addon_chocolates as $key => $item): ?>
                    <div class="w-checkbox p-30 pl-0 t_cen"> 
                      <label for="addon_<?= $item['child_items'][0]['id']?>" class="field-label w-form-label">
                        <?= $this->Html->image($item['medium_image'], ['alt' => 'flower','class'=>'ad_img']) ?> 
                        <div class="ad_item_name" > <?= $item['name'] ?> </div>
                        <div class="ad_item_price"><span class="rup_10"> ₹</span> <?=$item['child_items'][0]['discount_price']?> </div>
                        <input type="checkbox" id="addon_<?= $item['child_items'][0]['id']?>" name="checkbox" data-name="Checkbox" class="w-checkbox-input" ng-click="toggleAddOn('<?= $item['child_items'][0]['id']?>')">
                        <span class="checkmark ad-chk"></span> 
                      </label>
                      
                    </div>
                <?php
                  endforeach;
                ?>
                <!-- <div> 2 </div>
                <div> 3 </div>
                <div> 4 </div>
                <div> 5 </div>
                <div> 6 </div>
                <div> 7 </div> -->
              </div>
            </div>
            <div data-w-tab="Tab 3" class="tab-pane-3 w-tab-pane ">
            <div class="owl-carousel">
                <?php
                  foreach($addon_gifts as $key => $item): ?>
                    <div class="w-checkbox p-30 pl-0 t_cen"> 
                      <label for="addon_<?= $item['child_items'][0]['id']?>" class="field-label w-form-label">
                        <?= $this->Html->image($item['medium_image'], ['alt' => 'flower','class'=>'ad_img']) ?> 
                        <div class="ad_item_name" > <?= $item['name'] ?> </div>
                        <div class="ad_item_price"><span class="rup_10"> ₹</span> <?=$item['child_items'][0]['discount_price']?> </div>
                        <input type="checkbox" id="addon_<?= $item['child_items'][0]['id']?>" name="checkbox" data-name="Checkbox" class="w-checkbox-input" ng-click="toggleAddOn('<?= $item['child_items'][0]['id']?>')">
                        <span class="checkmark ad-chk"></span> 
                      </label>
                      
                    </div>
                <?php
                  endforeach;
                ?>
                <!-- <div> 2 </div>
                <div> 3 </div>
                <div> 4 </div>
                <div> 5 </div>
                <div> 6 </div>
                <div> 7 </div> -->
              </div>
            
            
            </div>

          </div>
          <div class="view_more_cake_div">
            <a id="add_to_cart_addon" href="#" class="cs-link view_more_button w-button ad_button" data-toggle="modal" data-target="#cs-addons" ng-click="addAddon()">proceed to cart</a>

          </div>
        </div>
      </div>
    </div>
  </div>