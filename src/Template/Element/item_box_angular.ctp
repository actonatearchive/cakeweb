 <div class="cake_card_div_blk" ng-repeat="cat in categories">
    <div class="card_cake_div_blk">
    <a href="{{base_url}}items/index/{{cat.url_slag}}" class="card_cake_img_link_blk w-inline-block">
       
            <div class="disc_tag_div" ng-if="{{cat.discount_percentage}}">
                <?= $this->Html->image('commerce-tag.svg', ['alt' => 'CakeWeb','class'=>'discount_tag_img','width'=>'70','height'=>'50']) ?>
                <div class="disc_text">{{cat.discount_percentage}}%</div>
            </div>
        
        <div class="card_cake_img_div">
            <img src="{{cat.medium_image}}" alt = 'CakeWeb'  class='card_cake_img'>
        </div>
    </a>
    </div>
    <div class="card_cake_info">
        <a href="{{base_url}}items/index/{{cat.url_slag}}" class="cake_name_link">
        
            {{cat.name}}
        </a>
    <div class="card_price_div">
        <div class="pd_disc_price cs-prolist_col__price">₹{{cat.price}}</div>
        <div class="card_cake_fianl_price cs-prolist_col__del">₹<span class="text-span">{{cat.discount_price}}</span></div>
    </div>
    <div class="form-block-3 w-form">

        <!-- <form id="email-form-2" name="email-form-2" data-name="Email Form 2" class="card_cake_form">
            <input type="submit" value="view details" data-wait="Please wait..." class="card_cake_view_detail w-button">
        </form> -->
        <a href="<?= $this->Url->build(['action'=>'index','controller'=>'items',$item['url_slag']]); ?>" >
            <input type="button" value="view details"  class="card_cake_view_detail w-button">
            
        </a>
        
        
    </div>
    </div>
</div>
