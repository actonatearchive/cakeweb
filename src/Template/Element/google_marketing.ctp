<?php
    $controller = strtolower($this->request->controller);
    $action = strtolower($this->request->action);
    // pr($controller);
    // pr($action);
?>

<?php
    if ($controller == 'checkout' && $action == 'success'):
?>
<!-- Google Ad Words -->

<!-- Google Code for 1.Leads Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 973359726;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "mHFUCPLtjQgQ7pSR0AM";
var google_remarketing_only = false;
/* ]]> */
</script>


<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/973359726/?label=mHFUCPLtjQgQ7pSR0AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Over here -->

<script>
    var order_items = <?= json_encode($order_items) ?> ;

    ga('send', 'pageview', '/success' );
    ga('require', 'ecommerce');
    ga('ecommerce:addTransaction', {
      'id': '<?= $order['code']?>',
      'affiliation': 'CakeStudio',
      'revenue': '<?= $order['grand_total']?>',
      'shipping': '<?= $order['additional_charges']?>',
      'currency': 'INR'
    });

    order_items.forEach(function(row) {
        var tmp_item_name = row.item_name.replace(/'/g, "\'");
        ga('ecommerce:addItem', {
          'id': '<?= $order['code']?>',
          'name': tmp_item_name,
          'sku': row.item_id,
          'price': row.price,
          'quantity': row.quantity,
          'currency': 'INR' // local currency code.
        });
    });
    ga('ecommerce:send');
</script>
<?php endif; ?>
