  <div class="benefits_section">
    <div class="benefits_div_footer">
      <div class="benefits_container w-container">
        <div class="benefits_text">benefits <span class="text-span-2">  on </span>  cake web</div>
        <div class="benefits_img_div">
          <div class="benefits_div">
            <?= $this->Html->image('004-scooter.svg', ['alt' => 'CakeWeb','class'=>'benefits_imgs','width'=>'120']) ?>

            <div class="benefits_text_div">
              <div class="benefit_info">special Midnight delivery</div>
              <div class="benefits_info1"> </div>
              <div class="benefits_info1">  </div>
            </div>
          </div>
          <div class="benefits_div">
            <?= $this->Html->image('001-free-delivery.svg', ['alt' => 'CakeWeb','class'=>'benefits_imgs','width'=>'120']) ?>
          
            <div class="benefits_text_div">
              <div class="benefit_info">Available across 50+ cities</div>
              <!-- <div class="benefits_info1">12 pm to 8 pm</div> -->
            </div>
          </div>
          
          <div class="benefits_div">
            <?= $this->Html->image('candy.svg', ['alt' => 'CakeWeb','class'=>'benefits_imgs','width'=>'120']) ?>
            
            <div class="benefits_text_div">
              <div class="benefit_info">Freshly Baked Cakes</div>
              <!-- <div class="benefits_info1">add chocolates</div> -->
            </div>
          </div>
          <!--
          <div class="benefits_div">
            <?= $this->Html->image('002-bouquet.svg', ['alt' => 'CakeWeb','class'=>'benefits_imgs','width'=>'120']) ?>
            
            <div class="benefits_text_div">
              <div class="benefit_info">make  it  beautiful</div>
              <div class="benefits_info1">9 pm to 12 am</div>
            </div>
          </div>
          <div class="benefits_div">
            <?= $this->Html->image('003-teddy-bear.svg', ['alt' => 'CakeWeb','class'=>'benefits_imgs','width'=>'120']) ?>
            
            <div class="benefits_text_div">
              <div class="benefit_info">make it  special</div>
              <div class="benefits_info1">9 pm to 12 am</div>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>