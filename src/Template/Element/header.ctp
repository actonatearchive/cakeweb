<div class="heaser_div">
  <div class="hm_header_section">
    <div class="hm_header_container w-container">
      <div class="custom_header_container">
        <a href="<?= $this->Url->build(['controller'=>'home','action'=>'city']) ?>" class="location_link_block w-inline-block">
          <div class="city_name"><?= $selectedCity['name'] ?></div>
          <?= $this->Html->image('draw.svg', ['alt' => 'CakeWeb','width'=>'13']) ?>
        </a>
        <a href="<?= $this->Url->build(['controller'=>'home','action'=>'index']) ?>" class="header_logo_link w-inline-block">
          <?= $this->Html->image('cakeWeb_Logo_New_08.png', [
          'alt' => 'CakeWeb','width'=>'180',
          'srcset'=> $this->request->webroot.'img/cakeWeb_Logo_New_08-p-500.png 500w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08-p-800.png 800w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08-p-1080.png 1080w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08-p-1600.png 1600w, '.$this->request->webroot.'img/cakeWeb_Logo_New_08.png 1775w', 
          'sizes'=>'(max-width: 479px) 29vw, (max-width: 991px) 140px, 180px',
          'class'=>"header_logo"
          
          ]) ?>
        </a>
        <div class="header_signup_cart_div">
          <?php if ($user_id == null): ?>

          <a href="<?= $this->Url->build(['controller'=>'users','action'=>'account']) ?>" class="signup_login_link">Sign up or Login</a>
          <a href="<?= $this->Url->build(['controller'=>'users','action'=>'account']) ?>" class="signup_login_link_blk w-inline-block">
            <?= $this->Html->image('002-social-1.svg', ['alt' => 'CakeWeb','class'=>'signup_login_icon','width'=>'50']) ?>
          </a>
          <?php else: ?>

          <div class="filter_block">
            <div data-delay="0" class="dropdown w-dropdown">
                <div class="dropdown-toggle w-dropdown-toggle">
                    <div class="icon w-icon-dropdown-toggle"></div>
                    <div class="signup_login_link"><?= $user['first_name'] ?></div>
                  
                    <div class="signup_login_link_blk w-inline-block">
                      <?= $this->Html->image('002-social-1.svg', ['alt' => 'CakeWeb','class'=>'signup_login_icon','width'=>'50']) ?>
                    </div>
                    
                </div>
                <nav class="w-dropdown-list">
                    <a href="<?= $this->Url->build(['controller'=>'users','action'=>'profile'])?>" class="dropdown-link w-dropdown-link">My Account</a>
                    <a href="<?= $this->Url->build(['controller'=>'users','action'=>'my-orders'])?>" class="dropdown-link w-dropdown-link">My Orders</a>
                    <a href="<?= $this->Url->build(['controller'=>'users','action'=>'logout'])?>" class="dropdown-link w-dropdown-link">Logout</a>
                </nav>
            </div>
            </div>
          <!-- <a href="<?= $this->Url->build(['controller'=>'users','action' => 'my-account']) ?>" class="signup_login_link">Wellcome ( <?= $user['first_name'] ?> )</a> -->
          
          <!-- <a href="<?= $this->Url->build(['controller'=>'users','action' => 'logout']) ?>" class="signup_login_link">Logout</a> -->
          <?php endif; ?>
          <?php if( $cart_item_count > 0):?>
            <div class="cart_items_div">
              <a href="<?= $this->Url->build(['controller'=>'users','action'=>'cart']) ?>">
                <?= $this->Html->image('cake-cart.svg', ['alt' => 'CakeWeb','class'=>'cart_img']) ?>
                <div class="items_count"><?= $cart_item_count ?></div>
              </a>
            </div>
            <div data-delay="0" class="cart_dropdown w-dropdown">
              <div class="dropdown_cart w-dropdown-toggle">
                <div class="w-icon-dropdown-toggle"></div>
                <div class="cart_price">₹<?= $cart_total['grand_total'] ?></div>
              </div>
              <nav class="drop_down_cart w-dropdown-list">
                <?php foreach ($cart_items as $key => $value): ?>

                <div class="cart_order_list_div">
                  <!-- <div class=""> -->
                    <a class="cart_item_img_div-copy" href='<?= $this->Url->build(["controller" => "items","action" => "index",$value['url_slag']]);?>'>
                      <?= $this->Html->image($value['small_image'], ['alt' => 'CakeWeb','class'=>'image-11']) ?>
                    </a>
                  <!-- </div> -->
                  <div class="cart_item_name_div">
                    <div class="cart_item_name_txt-copy"><?= $value['name']?></div>
                    <?php if (!empty($value['variant_name'])): ?>
                      <div class="cart_item_qty_txt-copy"><?= $value['variant_name']?></div>
                    <?php endif; ?>
                    <div>
                        <?php if (!empty($value['item_params']['delivery_message'])): ?>
                        <span class="text-span-12">Message</span>:
                        "<?= $value['item_params']['delivery_message'] ?>"
                        <?php endif; ?>
                    </div>
                  </div>
                  <div class="cart_qty_form_block w-form">
                    <form id="email-form" name="email-form" data-name="Email Form" class="cart_qty_form-copy">
                      <label for="email" class="cart_form_qty_label">Qty: <?= $value['qty']?></label>
                    </form>
                    <div class="w-form-done">
                      <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                      <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                  </div>
                  <div class="cart_item_amount">
                    <div class="cart_amt_txt"><span class="text-span-11">₹</span><?= $value['price']?></div>
                  </div>
                </div>
              <?php endforeach; ?>    
            

                <div class="cart_pay_button_align">
                  <div class="cart_btn_arrow_align">
                    <a href="<?= $this->Url->build(["controller" => "users","action" => "cart"]);?>" class="cart_pay_button w-button">
                      view cart 
                      <span class="text-span-8">|</span>
                    </a>
                    <?= $this->Html->image('right-arrow--white.svg', ['alt' => 'CakeWeb','class'=>'image-7','width'=>'30']) ?>
                    
                  </div>
                </div>
              </nav>
            </div>
          <?php endif; ?>



        </div>
      </div>
    </div>
  </div>
  <div data-collapse="medium" data-animation="over-left" data-duration="400" class="hm_navbar w-nav">
    <div class="_w-container w-container">
    <!-- <a href="#" class="brand w-nav-brand">
      <img src="images/001-american-express.svg" width="50" class="image">
    </a> -->
      <nav  role="navigation" class="nav-menu w-nav-menu">
        <a href="<?= $this->Url->build(['controller'=>'home','action'=>'city']) ?>" class="nav_link your_city w-nav-link">change city</a>
      <a href="<?= $this->Url->build(['controller'=>'home','action'=>'index']) ?>" class="nav_link w-nav-link">Home</a>
      <?php foreach($menu_list as $menu) : ?>
        <a href="<?= $this->Url->build(['controller'=>'category','action'=>'index',$menu['url_slag']]) ?>" class="nav_link w-nav-link">
        <?= $menu['name']?>
        </a>
      <?php endforeach; ?>
     
      
      <span id="search_icon" href="#" class="nav_link search_nav w-nav-link pointer" onclick="showbox('#search_in')" >
        <!-- code for dropdown search -->

        <!-- <input type="text" autocomplete="on" style="left: -90px;padding: 4px;width: 150px;border-radius: 10px;position: absolute;display:none;" id="search_in" > -->

      </span>
      <div id="searchbar" class="clearfix">
        <form id="searchform" method="get">
          <button type="submit" id="searchsubmit" class="fa fa-search fa-4x"></button>
          <input type="search" name="s" class="s" id="search_in" placeholder="Keywords..." autocomplete="off">     
        </form>
      </div>
    </nav>

    </div>
    <div class="menu-button-2 w-nav-button">
      <div class="w-icon-nav-menu"></div>
    </div>
    <?= $this->Form->create(null, ['url' => ['controller'=>'items','action' => 'search'],'class'=>"search",'id'=>'search-form']) ?>

    <?php if (isset($search) && $search != null): ?>
          <input type="text" class="search-input w-input" id="org_search" placeholder="Search for your cake"  name="search_name" value="<?= $search ?>">
    <?php else: ?>
          <input type="text" class="search-input w-input"  id="org_search" placeholder="Search for your cake"  name="search_name">
    <?php endif; ?>
      <input type="submit" value="." class="header_search_button w-button " id="search-btn"> 
    </form>  
  </div>
</div>

<script>
 $(document).ready(function(){
  var $searchlink = $('#searchtoggl i');
  var $searchbar  = $('#searchbar');
  
  $('#search_icon').on('click', function(e) {
    e.preventDefault();
    console.log("SEARCH_ICON_CLICKED");
    // if($(this).attr('id') == 'searchtoggl') {
      // if(!$searchbar.is(":visible")) { 
      //   // if invisible we switch the icon to appear collapsable
      //   $searchlink.removeClass('fa-search').addClass('fa-search-minus');
      // } else {
      //   // if visible we switch the icon to appear as a toggle
      //   $searchlink.removeClass('fa-search-minus').addClass('fa-search');
      // }
      
      $searchbar.slideToggle(300, function(){
        // callback after search bar animation
      });
    // }
  });
  
  $('#searchform').submit(function(e){
    e.preventDefault(); // stop form submission
  });
});
</script>
<?= $this->Flash->render() ?>

