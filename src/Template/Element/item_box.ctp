 <div class="cake_card_div_blk">
    <div class="card_cake_div_blk">
    <a href="<?= $this->Url->build(['action'=>'index','controller'=>'items',$item['url_slag']]); ?>" class="card_cake_img_link_blk w-inline-block">
        <?php if ($item['discount_percentage'] > 0) :?>
            <div class="disc_tag_div">
                <?= $this->Html->image('commerce-tag.svg', ['alt' => 'CakeWeb','class'=>'discount_tag_img','width'=>'70','height'=>'50']) ?>
                <div class="disc_text"><?= $item['discount_percentage']?>%</div>
            </div>
        <?php endif?>
        <div class="card_cake_img_div">
            <img src="<?= $item['medium_image'] ?>" alt = 'CakeWeb'  class='card_cake_img'>
        </div>
    </a>
    </div>
    <div class="card_cake_info">
        <a href="<?= $this->Url->build(['action'=>'index','controller'=>'items',$item['url_slag']]); ?>" class="cake_name_link">
            <?= $item['name']?>
        </a>
    <div class="card_price_div">
        <div class="pd_disc_price">₹<?= $item['price']?></div>
        <div class="card_cake_fianl_price">₹<span class="text-span"><?= $item['discount_price']?></span></div>
    </div>
    <div class="form-block-3 w-form">

        <!-- <form id="email-form-2" name="email-form-2" data-name="Email Form 2" class="card_cake_form">
            <input type="submit" value="view details" data-wait="Please wait..." class="card_cake_view_detail w-button">
        </form> -->
        <a href="<?= $this->Url->build(['action'=>'index','controller'=>'items',$item['url_slag']]); ?>" >
            <input type="button" value="view details"  class="card_cake_view_detail w-button">
            
        </a>
        
        
    </div>
    </div>
</div>