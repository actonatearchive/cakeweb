<div class="footer_section">
    <div class="footer_container w-container">
      <div class="footer_tittle">Secured online payments</div>
      <div class="payments_option">
        <?= $this->Html->image('001-american-express.svg', ['alt' => 'CakeWeb','class'=>'payment_img','width'=>'50']) ?>
        
        <?= $this->Html->image('002-visa.svg', ['alt' => 'CakeWeb','class'=>'payment_img','width'=>'50']) ?>
        
        <?= $this->Html->image('003-mastercard.svg', ['alt' => 'CakeWeb','class'=>'payment_img','width'=>'50']) ?>

      </div>
      <div class="footer_nav_div">
        <a href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','about-us'])?>" class="footer_nav_link">About Us</a>
        <a href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','faqs'])?>" class="footer_nav_link">
          FAQs
        </a>
        <a href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','privacy-policy'])?>" class="footer_nav_link">
          privacy
        </a>
        <a href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','terms-conditions'])?>" class="footer_nav_link">Terms</a>
        <a href="<?= $this->Url->build(['action'=>'contents','controller'=>'home','refund-policy'])?>" class="footer_nav_link">refund</a>
        <a href="<?= $this->Url->build(['action'=>'contact','controller'=>'home'])?>" class="footer_nav_link">contact us</a>
        <a href="#" class="footer_nav_link">sitemap</a></div>
      <div class="social_div">
        <a href="https://www.facebook.com/cakewebindia/" target="_blank"><?= $this->Html->image('002-facebook.svg', ['alt' => 'CakeWeb','class'=>'social_icon','width'=>'25']) ?></a>
        <a href="https://twitter.com/@cakeweb_" target="_blank"><?= $this->Html->image('001-twitter.svg', ['alt' => 'CakeWeb','class'=>'social_icon','width'=>'25']) ?> </a>
        <a href="https://www.instagram.com/cakeweb_/?hl=en" target="_blank"> <?= $this->Html->image('005-instagram-logo.svg', ['alt' => 'CakeWeb','class'=>'social_icon','width'=>'30']) ?> </a>
        <a href="https://plus.google.com/106185328883715119026" target="_blank"> <?= $this->Html->image('004-social-media.svg', ['alt' => 'CakeWeb','class'=>'social_icon','width'=>'30']) ?> </a>
        <a href="tel:91 72028 73333"> <?= $this->Html->image('003-clock.svg', ['alt' => 'CakeWeb','class'=>'social_icon','width'=>'30']) ?>
                
      </div>
      <div class="copyright_text">© 2017 cakeweb.in, all rights reserved</div>
    </div>
  </div>