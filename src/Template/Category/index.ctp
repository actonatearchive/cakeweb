<div class="category_main_section" ng-controller="CategoryController">


     <input id="category_id" type="hidden" value="<?= $category['id'] ?>" />
    <input id="page_type" type="hidden" value="category" />
    <input id="category_name" type="hidden" value="<?= $url_slag ?>" />
    <input id="subcategory" type="hidden" value="<?= $sub_cat ?>" />
    <input id="sort_by" type="hidden" value="<?= $sort_by ?>" />

    
    <div class="cat_main_container w-container">
        <div class="cat_breadcrumb_div">
            <div class="breadcrumb_div_blk">
                <a href="<?= $this->Url->build(['controller'=>'home','action'=>'index']) ?>" class="breadcrumb_text">home</a>
                <?= $this->Html->image('right-arrow.svg', ['alt' => 'CakeWeb','class'=>'breadcrumb_arrow','width'=>'12']) ?>
                <a href="#" class="breadcrumb_text current">
                    <?= $category['name']?>
                </a>
            </div>
            <div class="filter_block">
            <div class="sortby">Sort by</div>
            <div data-delay="0" class="dropdown w-dropdown">
                <div class="dropdown-toggle w-dropdown-toggle">
                    <div class="icon w-icon-dropdown-toggle"></div>
                    <div>Name</div>
                </div>
                <nav class="w-dropdown-list">
                    <a href="<?= $this->Url->build(['controller'=>'category','action'=>'index',$category['url_slag'],'?'=>['sort_by'=>'a_z']])?>" class="dropdown-link w-dropdown-link">name A-Z</a>
                    <a href="<?= $this->Url->build(['controller'=>'category','action'=>'index',$category['url_slag'],'?'=>['sort_by'=>'z_a']])?>" class="dropdown-link w-dropdown-link">name Z-A</a>
                    <a href="<?= $this->Url->build(['controller'=>'category','action'=>'index',$category['url_slag'],'?'=>['sort_by'=>'high_low']])?>" class="dropdown-link w-dropdown-link">high to low</a>
                    <a href="<?= $this->Url->build(['controller'=>'category','action'=>'index',$category['url_slag'],'?'=>['sort_by'=>'low_high']])?>" class="dropdown-link w-dropdown-link">low to high</a>
                </nav>
            </div>
            </div>

            <div data-collapse="medium" data-animation="over-right" data-duration="400" class="navbar_filter w-nav">
                <div class="filter_container w-container">
                    <nav role="navigation" class="nav-menu-2 w-nav-menu">
                        <div class="cat_filter_mobile_div">
                            <div class="cat_price_div">
                                <div class="price_text">price</div>
                                <a href="#" class="cat_arrow_link_blk w-inline-block">
                                    <img src="images/32195.svg" width="15" class="down_arrow_img">
                                </a>
                            </div>
                            <div class="cat_price_filter_div"></div>
                            <?php if(sizeof($category['child_categories']) > 0 ) :?>
                                <div class="cat_filter_seperator"></div>
                                <div class="cat_category_div">
                                    <div class="category_text">shop by category</div>
                                </div>

                                <a href="#" class="cat_category_list_link_div w-inline-block" data-ix="show">
                                    <div class="cat_filter_category_name">
                                        <?= $category['name'] ?>
                                    </div>
                                    <img src="images/32195.svg" width="15" class="down_arrow_img">
                                </a>

                                <div class="cat_category_items_list_div_1" data-ix="new-interaction">
                                    <?php foreach($category['child_categories'] as $cat) : ?>
                                        <div class="cat_ckeckbox_form_block w-form">
                                            <form id="email-form-3" name="email-form-3" data-name="Email Form 3" class="cat_checkbox_form">
                                                <div class="w-checkbox">
                                                    <input type="checkbox" id="checkbox-2" name="checkbox-2" data-name="Checkbox 2" class="w-checkbox-input">
                                                    <label for="checkbox-6" class="filter_checkbox_label w-form-label">
                                                        <?= $cat['name'] ?>
                                                    </label>
                                                </div>
                                                <div class="cat_filter_category_count">56</div>
                                            </form>
                                            <div class="w-form-done">
                                                <div>Thank you! Your submission has been received!</div>
                                            </div>
                                            <div class="w-form-fail">
                                                <div>Oops! Something went wrong while submitting the form.</div>
                                            </div>
                                        </div>
                                        <?php endforeach;?>

                                </div>
                        <?php endif;?>

                        </div>
                    </nav>
                <div class="menu-button w-nav-button">
                    <?= $this->Html->image('multimedia.svg', ['alt' => 'CakeWeb','height'=>'20','width'=>'20']) ?>

                </div>
            </div>
        </div>
    </div>

  
    
    <div class="cat_main_div">
        <!-- <div class="cat_filter_div">
            <div class="cat_price_div">
                <div class="price_text">price</div>
                <a href="#" class="cat_arrow_link_blk w-inline-block">
                    <?= $this->Html->image('32195.svg', ['alt' => 'CakeWeb','class'=>'down_arrow_img','width'=>'15']) ?>

                </a>
            </div>
            <div class="cat_price_filter_div"></div>
       
            
            <?php if(sizeof($category['child_categories']) > 0 || true ) :?>

                <div class="cat_filter_seperator"></div>
                <div class="cat_category_div">
                    <div class="category_text">shop by category</div>
                </div>
                <a href="#" class="cat_category_list_link_div w-inline-block" data-ix="show">
                    <div class="cat_filter_category_name">
                        <?= $category['name'] ?>
                    </div>
                    <?php if(sizeof($category['child_categories']) >0) : ?>
                    
                    <?= $this->Html->image('32195.svg', ['alt' => 'CakeWeb','class'=>'down_arrow_img','width'=>'15']) ?>
                    <?php endif; ?>
                </a>
                <div class="cat_category_items_list_div_1" data-ix="new-interaction">
                    <?php foreach($category['child_categories'] as $cat) : ?>

                        <div class="cat_ckeckbox_form_block w-form">
                            <form id="email-form-3" name="email-form-3" data-name="Email Form 3" class="cat_checkbox_form">
                                <div class="w-checkbox">
                                    <input type="checkbox" id="checkbox-2" name="checkbox-2" data-name="Checkbox 2" class="w-checkbox-input">
                                    <label for="checkbox-2" class="field-label-2 w-form-label">
                                        <?= $cat['name']?>
                                    </label>
                                </div>
                                <div class="cat_filter_category_count">78</div>
                            </form>
                            <div class="w-form-done">
                                <div>Thank you! Your submission has been received!</div>
                            </div>
                            <div class="w-form-fail">
                                <div>Oops! Something went wrong while submitting the form.</div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
                <?php endif; ?>
        </div> -->
        <div class="cat_card_div" style="width: 100%;margin:0px">
            <?php if (sizeof($category_listing) > 0 ) : ?>
              <?php foreach($category_listing as $item): ?>
                <?= $this->element('item_box', ['item'=>$item]) ?>
              <?php endforeach; ?>
              <?= $this->element('item_box_angular', ['item'=>$item]) ?>
            <?php else : ?>
                <div class=" d-flex align-items-center justify-content-center cs-infotext mt-4 mb-5 t_cen" ng-if="page.disabled == true">
                <!-- <= $this->Html->image('sad-face.svg',['alt'=>'loader']) ?>  -->
                <div class="thats_all"> No Products. </div> 
            </div>

            <?php endif; ?>
            </div>
        </div>
            <br>
            <div class=" d-flex align-items-center justify-content-center t_cen" ng-hide="page.disabled " ng-show="page.disabled">
                <?= $this->Html->image('spinner.svg',['alt'=>'loader']) ?>
            </div>
            <!-- INFINITE SCROLL -->
            <div infinite-scroll="getNextPage()" infinite-scroll-distance="1" ng-if="page.disabled == false"></div>
            <!-- INFINITE SCROLL ENDS HERE -->
            <div class=" d-flex align-items-center justify-content-center cs-infotext mt-4 mb-5 t_cen" ng-if="page.disabled == true">
                <!-- <= $this->Html->image('sad-face.svg',['alt'=>'loader']) ?>  -->
                <!-- <div class="thats_all"> Thanks Folks! That's all for now. </div>  -->
            </div>

        
    
</div>
</div>

<?= $this->element('subscribe') ?>