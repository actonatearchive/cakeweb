<div class="adm_section mt-130">
    <div id="signin" class="sign_in_container w-container">
      <div class="sign-in-form-block w-form">
        <form id="email-form" name="email-form" data-name="Email Form" class="sign-in-form"><label for="name" class="adm-page-tittle">sign in</label>
          <div class="adm-inputbox-div"><input type="email" class="adm-login-inpbox w-input" maxlength="256" name="name" data-name="Name" placeholder="Email Address" id="name">
         
            <?= $this->Html->image('user.svg', ['alt' => 'CakeWeb','class'=>'login-logo']) ?>

          </div>
          <div class="adm-inputbox-div"><input type="password" class="adm-login-inpbox w-input" maxlength="256" name="name-2" data-name="Name 2" placeholder="Password" id="name-2">
            
            <?= $this->Html->image('lock-icon.svg', ['alt' => 'CakeWeb','class'=>'password-logo']) ?>
            
          </div>
          <div class="adm-rem-forgot-div">
            <div class="adm-rem-chkbox w-checkbox"><input type="checkbox" id="checkbox" name="checkbox" data-name="Checkbox" class="w-checkbox-input"><label for="checkbox" class="adm_remember_text w-form-label">Remember me</label></div><a href="#" class="adm_forgot-text">forgot password?</a></div><input type="submit" value="sign in" data-wait="Please wait..." class="login_button w-button">
          <div class="adm-or-div">
            <div class="adm--or-text">or sign in with</div>
          </div><input type="submit" value="google" data-wait="Please wait..." class="google_button w-button"><input type="submit" value="facebook" data-wait="Please wait..." class="facebook_button w-button">
          <div class="adm_footer_div">
            <div class="sign_up_msg">Don&#x27;t have an account?</div><a id="sign-up-link" href="#signup" class="adm_sign_up_link">sign up</a></div>
        </form>
        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form.</div>
        </div>
      </div>
    </div>
    <div id="signup" class="sign_up_container w-container">
      <div class="sign-up-form-block w-form">
        <form id="email-form" name="email-form" data-name="Email Form" class="sign-up-form"><label for="name-3" class="adm-page-tittle">sign up</label><input type="text" class="adm-email-inpbox w-input" maxlength="256" name="name-3" data-name="Name 3" placeholder="First Name" id="name-3"><input type="text" class="adm-email-inpbox w-input" maxlength="256" name="name-3" data-name="Name 3" placeholder="Last Name" id="name-3"><input type="email" class="adm-email-inpbox w-input" maxlength="256" name="name-3" data-name="Name 3" placeholder="Email Address" id="name-3"><input type="password" class="adm-password-inpbox w-input" maxlength="256" name="name-2" data-name="Name 2" placeholder="Password" id="name-2"><input type="password" class="adm-password-inpbox w-input" maxlength="256" name="name-2" data-name="Name 2" placeholder="Retype Password" id="name-2"><input type="submit" value="sign up" data-wait="Please wait..." class="signup_button w-button">
          <div class="adm_footer_div">
            <div class="sign_up_msg">Already have an account?</div><a id="sign-in-link" href="#signin" class="adm_sign_up_link">sign in</a></div>
        </form>
        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form.</div>
        </div>
      </div>
    </div>
  </div>