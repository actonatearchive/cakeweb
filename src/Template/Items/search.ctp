
<div class="category_main_section" ng-controller="CategoryController">
 <input id="category_id" type="hidden" value="<?= $search ?>" />
    <input id="page_type" type="hidden" value="search" />
    <div class="cat_main_container w-container">
    <div class="cat_breadcrumb_div padding-top-20" >
            <div class="breadcrumb_div_blk">
                <span class="breadcrumb_text">
                
                Showing result for "<span style="color:red"><?= $search ?></span>"
                
                </span>
                    
            </div>
    </div>


<?php if(sizeof($category_listing) > 0):?>
<div class="card_trending_div">
    <?php foreach($category_listing as $item): ?>
        
        <?= $this->element('item_box', ['item'=>$item]) ?>

    <?php endforeach; ?>
        <?= $this->element('item_box_angular', ['item'=>$item]) ?>
        
</div>
<?php else: ?>
<div class="cat_breadcrumb_div padding-top-20  ">
            <div class="breadcrumb_div_blk">
                <span class="breadcrumb_text">
                
                    No products found.
                
                </span>
                    
            </div>
    </div>
<?php endif; ?>


<div class="col col-12 d-flex align-items-center justify-content-center t_cen" ng-hide="page.disabled " ng-show="page.disabled">
    <?= $this->Html->image('spinner.svg',['alt'=>'loader']) ?>
</div>
<!-- INFINITE SCROLL -->
<div infinite-scroll="getNextPage()" infinite-scroll-distance="1" ng-if="page.disabled == false"></div>
<!-- INFINITE SCROLL ENDS HERE -->
<div class="col col-12 d-flex align-items-center justify-content-center cs-infotext mt-4 mb-5 t_cen" ng-if="page.disabled == true">
    <!-- <= $this->Html->image('sad-face.svg',['alt'=>'loader']) ?>  -->
    <div class="thats_all"> Thanks Folks! That's all for now. </div> 
</div>

</div>

<?= $this->element('subscribe') ?>
