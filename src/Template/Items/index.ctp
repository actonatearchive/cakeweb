<style>
textarea{
  resize: none;
}
</style>
<!-- Hidden Fields -->
<input id="parent_item_id" type="hidden" value="<?= $item['id']?>">
<input id="before_item_id" type="hidden" value="<?= $item['id']?>">

<input id="default_id" type="hidden" value="<?= $default_id ?>"> <!-- View Purpose only -->
<input id="default_price" type="hidden" value="<?= $default_price ?>"> <!-- View Purpose only -->
<input id="default_discount_price" type="hidden" value="<?= $default_discount_price ?>"> <!-- View Purpose only -->

<input id="base_time_left" type="hidden" value="<?= $today_time_remain ?>" />

    <form action="/search" class="search w-form">
    <input type="search" class="search-input w-input" maxlength="256" name="query" placeholder="Search…" id="search" required=""><input type="submit" value="." class="header_search_button w-button"></form>
  </div>
  <div class="pd_main_section">
    <div class="pd_main_container w-container">
      <div class="pd_cake_img_div">
        <div class="pd_stock_div">
          <div class="in_stock_text">in stock</div>
            <?= $this->Html->image('001-mark.svg', ['alt' => 'CakeWeb','class'=>'image-5','width'=>'15']) ?>

        </div>
            <!-- <= $this->Html->image('001-american-express.svg', ['alt' => 'CakeWeb','class'=>'image','width'=>'50']) ?> -->
            
        <div class="pd_stock_out_div">
          <div class="stock_out_text">stock out</div>
            <?= $this->Html->image('002-close.svg', ['alt' => 'CakeWeb','class'=>'image-6','width'=>'15']) ?>
            
          </div>
            <?= $this->Html->image($item['full_image'], ['alt' => 'CakeWeb','class'=>'pd_cake_img']) ?>     
        </div>
      <div class="pd_cake_info_div">
        <div class="pd_breadcrumb_div">
          <?= $this->Html->link('Home',['controller'=>'home','action'=>'index'],['class'=>'breadcrumb_text']) ?>
          <?= $this->Html->image('right-arrow.svg', ['alt' => 'CakeWeb','class'=>'breadcrumb_arrow','width'=>'12']) ?>
          
          <?= $this->Html->link($item['category']['name'],['controller'=>'category','action'=>'index',$item['category']['url_slag']],['class'=>'breadcrumb_text']) ?>
          <?= $this->Html->image('right-arrow.svg', ['alt' => 'CakeWeb','class'=>'breadcrumb_arrow','width'=>'12']) ?>
          
          <a href="#" class="pd_breadcrumb_text current"><?= $item['name']?></a>
        </div>
        <div class="pd_cake_name"><?= $item['name']?></div>
        <div class="pd_cake_info"><?= $item['short_desc'] ?></div>
        <div class="pd_cake_weight_div pointer">
          <?php foreach ($item['child_items'] as $key => $value): ?>
          
            <div id="variant_<?= $value['id'] ?>" whenclicked="reply_click(this.id)" class="pd_cake_weight" 
            onclick="changeVariant('<?= $value['id'] ?>','<?=$value['price']?>','<?=$value['discount_price']?>')" >
              <!-- 0.5 kg -->
              <?= $value['variant_name'] ?>
            </div>
          <?php endforeach; ?>
          
         
        </div>

        <!-- <div class="mb-3">
            <ul class="cs-kgvarnts">
                <?php foreach ($item['child_items'] as $key => $value): ?>
                        <li id="variant_<?=$value['id']?>"><a href="javascript:void(0)" onclick="changeVariant('<?= $value['id'] ?>','<?=$value['price']?>','<?=$value['discount_price']?>')"><?= $value['variant_name'] ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div> -->

        <div class="pd_price_div">
          <div class="pd_disc_price" >₹<span id="item_price"><?= $item['price'] ?></span></div>
          <div class="pd_cake_fianl_price" >₹<span class="text-span" id="item_discount_price"><?= $item['discount_price'] ?></span></div>
          <!-- <div class="taxes">+ Taxes</div> -->
        </div>
        <div class="pd_add_ons">add ons</div>
        <div class="w-form">
          <form id="email-form" name="email-form" data-name="Email Form">
            <div class="pd_addons_checkbox w-checkbox">
              <label for="checkbox" class="field-label w-form-label">
                <input type="checkbox" id="checkbox" name="checkbox" data-name="Checkbox" class="w-checkbox-input" onchange="checkEggLess(event)"><span class="checkmark"></span>
                 I want an eggless cake <span class="text-span-6">+ ₹100.00</span>
              </label>
            </div>
            <div class="pd_addons_checkbox w-checkbox">
              <label for="checkbox-3" class="field-label w-form-label">
                <input type="checkbox" id="checkbox-3" name="checkbox-3" data-name="Checkbox 3" class="w-checkbox-input" onchange="checkMidNight(event)"><span class="checkmark"></span>
                Midnight delivery (9pm -12am) <span class="text-span-6">+ ₹250.00</span>
              </label>
            </div>
            <!-- <div class="pd_addons_checkbox w-checkbox">
              <input type="checkbox" id="checkbox-2" name="checkbox-2" data-name="Checkbox 2" class="w-checkbox-input" >
              <label for="checkbox-2" class="field-label w-form-label">I want a heart shape cake <span class="text-span-6">+ ₹75.00</span></label>
            </div> -->
          </form>
          <div class="w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail">
            <div>Oops! Something went wrong while submitting the form.</div>
          </div>
        </div>
      </div>
      <?php
            $hour = date('H');
        ?>
      <div class="pd_delivery_div">
        <?php if ($hour < $today_time_remain): ?>

        <div class="pd_del_align_div">
          <div class="pd_delivery_details_text">same day delivery</div>
          <div class="pd_del_time_div">
            <div class="pd_ends_text">ends in:</div>
            <div class=" pd_time_left" id="time_left" >00 : 35 : 20</div>
          </div>
        </div>
        <?php endif; ?>

        <div class="pd_delivery_details_text">cake delivery details</div>
        
        <div class="date_msg_form_block w-form">
          <form id="email-form-2" name="email-form-2" data-name="Email Form 2" class="pd_form_del">
           
            <?php if ($hour < $today_time_remain){ 
                $day="+0d"; $date=date('Y-m-d');
              } else{
                  $day="+1d"; $date=date('Y-m-d',strtotime('+1 day'));
              }                 
            ?>
            
            <input type="date" id="delivery_date" class="pd_date_input w-input "  placeholder="Select date"  data-date-start-date="<?= $day ?>" min="<?= $date ?>" value="<?= $date ?>" onchange="changeDeliveryDate(event)" >
            <div class="pd_cake_msg_rel_blk">
              <textarea id="message_on_cake" name="field" maxlength="5000" placeholder="eg : happy b&#x27;day  &quot;mom&quot;  " class="pd_textarea_cake_msg w-input" onchange="changeMessageOnCake(event)"></textarea>
              <div class="pd_textarea_msg">Message on the cake</div>
            </div>
            <div class="pd_special_msg_rel_blk">
              <textarea id="optional_specs" name="field-2" maxlength="5000" placeholder="eg : avoid peanut as allergic" data-name="Field 2" class="pd_textarea_specialmsg w-input" onchange="changeOptionalMessage(event)"></textarea>
              <div class="pd_textarea_special_msg">Any special note?</div>
            </div>
          </form>
          <div class="w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail">
            <div>Oops! Something went wrong while submitting the form.</div>
          </div>
        </div>
        <div class="pd_cart_button_align"><a  href="javascript:void(0)" class="pd_add_cart_button w-button jq-add-to-cart"  id="add_to_cart" data-id="<?= $item['id']?>" >Add to Cart</a></div>
      </div>
    </div>
  </div>
  <?= $this->element('benefits') ?>
  <!-- Script -->
<script>
$(document).ready(function() {
    var data_id = $('#default_id').val();
    var data_price = $('#default_price').val();
    var data_discount_price = $('#default_discount_price').val();

    changeVariant(data_id,data_price,data_discount_price);
    $(".pd_cake_img").elevateZoom(); 
});

</script>
