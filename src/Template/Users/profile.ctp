<div class="yc_main_section">
    <div class="w-container">
      <div class="tab_section">
        <div data-duration-in="300" data-duration-out="100" class="my_account_tabs w-tabs">
          <div class="my_account_tabs_menu-copy w-tab-menu">
            <a data-w-tab="Tab 1" class="my_acc_category w-inline-block w-tab-link" href="<?= $this->Url->build(['controller'=>'users','action'=>'my-orders']) ?>" id="my-orders">
              <div class="text-block-3">order details</div>
            </a>
            <a data-w-tab="Tab 2" class="my_acc_category w-inline-block w-tab-link w--current"  href="#profile" id="profile" >
              <div class="text-block-4">change personal details</div>
            </a>
          </div>
          <div class="tabs-content-2 w-tab-content">
            
            <div data-w-tab="Tab 2" class="w-tab-pane tab-pane w--current" >
              <div class="card_trending_div">
                <div class="edit_personal_details_div">
                  <div class="personal_detail_tittle">
                    <?= $this->Html->image('social-user.svg', ['alt' => 'CakeWeb','class'=>'image-10','width'=>'40']) ?>
                      
                      
                    <div class="edit_profile_txt">Personal Details</div>
                  </div>
                </div>
                <div class="new_edit_form_block-copy w-form">
                    <?= $this->Form->create(
                          null,
                          [
                            'url' => [
                              'controller'=>'users','action' => 'change-profile-info'
                            ],
                            'class'=>'new_addr_form'
                          ]
                        ) 
                    ?>
                      <div class="sh_name_div">
                        <div class="sh_first_name_input_div">
                          <input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="first_name" data-name="Name" id="name" value="<?= $user['first_name']?>">
                          <div class="first_name_latel">first name</div>
                        </div>
                        <div class="sh_last_name_input_div">
                          <input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="last_name" data-name="Name 2" value="<?= $user['last_name']?>" id="name-2">
                          <div class="first_name_latel">Last name</div>
                        </div>
                      </div>
                      <div class="sh_contact_div">
                        
                        <div class="sh_last_name_input_div">
                          <input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="mobile" data-name="Name 2" placeholder="9891234567" id="name-2" value="<?= $user['mobile']?>">
                          <div class="first_name_latel">phone number</div>
                        </div>
                      </div>
                      
                     
                      <div class="sh_pay_button_align">
                        <div class="save_changes_div">
                          <button href="#" class="save_changes_button w-button">save changes</button></div>
                      </div>
                      <hr>
                    </form>


                  
                <div class="edit_personal_details_div" style="padding-bottom:20px">
                  <div class="personal_detail_tittle">
                    <?= $this->Html->image('social-user.svg', ['alt' => 'CakeWeb','class'=>'image-10','width'=>'40']) ?>
                      
                      
                    <div class="edit_profile_txt">Change Password</div>
                            
                  </div>
                  
                </div>
                <div class="edit_state_div">
                    <div class="sh_city_input_div">
                        <input type="password" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="Enter"  id="jq-password">
                        <div class="first_name_latel">old password</div>
                      </div>  
                    <div class="edit_new_pass_div">
                        <input type="password" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="Enter"  id="jq-newpassword">
                        <div class="first_name_latel">New password</div>
                      </div>
                      <div class="edit_confirm_pass"><input type="password" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="Enter"  id="jq-retypepassword">
                        <div class="first_name_latel">confirm password</div>
                      </div>
                      
                    </div>
                    <div class="sh_pay_button_align">
                        <div class="save_changes_div">
                          <button href="#" class="save_changes_button w-button" onclick="changePassword()">Change password</button></div>
                      </div>
                </div>
                

                <!-- <div class="sh_addr_div">
                    <div class="sh_address_input_div"><input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder=" 196 Woodside Circle Mobile" id="name-4">
                      <div class="first_name_latel">Address</div>
                    </div>
                    <div class="sh_zipcode_input_div"><input type="text" class="sh_zipcode_inpbox w-input" maxlength="256" autofocus="true" name="name-2" data-name="Name 2" placeholder="100001" id="name-2">
                      <div class="first_name_latel">Zipcode</div>
                    </div>
                  </div>
                  <div class="sh_state_div-copy">
                    <div class="sh_city_input_div"><input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="Chicago" id="name-4">
                      <div class="first_name_latel">City</div>
                    </div>
                    <div class="sh_city_input_div"><input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="Illinois" id="name-4">
                      <div class="first_name_latel">state</div>
                    </div>
                    <div class="sh_city_input_div"><input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" name="name-4" data-name="Name 4" placeholder="United States" id="name-4">
                      <div class="first_name_latel">country</div>
                    </div>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>