<div class="adm_section">
    <div id="signin" class="sign_in_container w-container jq-account">
        <div class="sign-in-form-block w-form">
            <?= $this->Form->create(null, ['url' => ['controller' => 'users', 'action' => 'login'],'class' => 'my-2']) ?>

                <label for="name" class="adm-page-tittle">sign in</label>
                <div class="adm-inputbox-div">
                    <input type="email" class="adm-login-inpbox w-input" placeholder="Email Address" id="inputLoginUsername" name="username"  required>

                    <?= $this->Html->image('user.svg', ['alt' => 'CakeWeb','class'=>'login-logo']) ?>

                </div>
                <div class="adm-inputbox-div">
                    <input type="password" class="adm-login-inpbox w-input"  id="inputLoginPassword" name="password" placeholder="Password" required>

                    <?= $this->Html->image('lock-icon.svg', ['alt' => 'CakeWeb','class'=>'password-logo']) ?>

                </div>
                <div class="adm-rem-forgot-div">
                    <div class="adm-rem-chkbox w-checkbox">
                        <label for="checkbox" class="adm_remember_text w-form-label">
                          <input type="checkbox" id="checkbox" name="checkbox" data-name="Checkbox" class="w-checkbox-input"><span class="checkmark "></span> 
                          Remember me
                        </label>
                    </div>
                    
                    <a href="#forgot_password" id="forgot_password_link" class="adm_forgot-text">forgot password?</a>
                    
                    </div>
                <input type="submit" value="sign in" data-wait="Please wait..." class="login_button w-button">
                <?= $this->Form->end() ?>
                
                <div class="adm-or-div">
                    <div class="adm--or-text">or sign in with</div>
                </div>

                <?= $this->Form->create(null, [
                'url' => [
                    'plugin' => 'ADmad/SocialAuth',
                    'controller' => 'Auth',
                    'action' => 'login',
                    'provider' => 'google',
                    '?' => ['redirect' => $this->request->getQuery('redirect')]
                ]
                ])?>
                <input type="submit" value="google" class="google_button w-button">
                <?= $this->Form->end() ?>
                
                <?= $this->Form->create(null, [
                    'url' => [
                        'plugin' => 'ADmad/SocialAuth',
                        'controller' => 'Auth',
                        'action' => 'login',
                        'provider' => 'facebook',
                        '?' => ['redirect' => $this->request->getQuery('redirect')]
                    ]
                ])?>
                
                    <input type="submit" value="facebook" class="facebook_button w-button">
                
                <?= $this->Form->end() ?>
                
                
                <div class="adm_footer_div">
                    <div class="sign_up_msg">Don&#x27;t have an account?</div><a id="sign-up-link" href="#signup" class="adm_sign_up_link">sign up</a></div>
            <?= $this->Form->end() ?>

        </div>
    </div>
    <div id="signup" class="sign_up_container w-container jq-account">
        <div class="sign-up-form-block w-form">
            <?= $this->Form->create(null, ['url' => ['controller' => 'users', 'action' => 'register'],'class' => 'my-2']) ?>
                <label for="name-3" class="adm-page-tittle">sign up</label>
                <input type="text" class="adm-email-inpbox w-input" maxlength="256" name="first_name" placeholder="First Name" id="inputFirstName">
                <input type="text" class="adm-email-inpbox w-input" maxlength="256" name="last_name" placeholder="Last Name" id="inputLastName">
                <input type="email" class="adm-email-inpbox w-input" maxlength="256" name="username" placeholder="Email Address" id="inputEmailAddress">
                <input type="password" class="adm-password-inpbox w-input" maxlength="256" name="password" placeholder="Password" id="inputPassword">
                <input type="password" class="adm-password-inpbox w-input" maxlength="256" name="mobile" placeholder="Retype Password" id="inputMobileNo">
                <input type="submit" value="sign up" data-wait="Please wait..." class="signup_button w-button">
                <div class="adm_footer_div">
                <div class="sign_up_msg">Already have an account?</div>
                    <a id="sign-in-link" href="#signin" class="adm_sign_up_link">sign in</a>
                </div>
            <?= $this->Form->end() ?>

            
        </div>
    </div>
    
    <div id="forgot_password" class=" w-container hidden jq-account" >
        <div class="sign-up-form-block w-form">
            <div class="loginBox ls-signup xs-mtop10">
                <?= $this->Form->create(null,['id'=>'forgetPasswordForm','url' => ['controller' => 'Users','action' => 'resetPasswordLink']]) ?>
                <label for="name-3" class="adm-page-tittle" style="margin-bottom:0px;">Forgot Password?</label>
                <div class="" >
                
                    <div class="adm--or-text" style="margin-bottom:40px;">
                        We will mail you a link to reset password
                    </div>
                </div>
                <input type="email" class="adm-email-inpbox w-input" maxlength="256" name="username" placeholder="Email Address" >
                
                <input type="submit" value="SEND RESET LINK" data-wait="Please wait..." class="login_button w-button">
                
                    <!-- <p class="text-center"><a class="hov" href="#login" onClick="showDiv('login')">Login</a></p> -->
                <?= $this->Form->end() ?>
                <div class="adm_footer_div">
                    <div class="sign_up_msg">Already have an account?</div>
                    <a id="sign-in-link2" href="#signin" class="adm_sign_up_link">sign in</a></div>
            </div>
        </div>
    </div>
</div>

