<div class="yc_main_section">
    <div class="w-container">
      <div class="tab_section">
        <div data-duration-in="300" data-duration-out="100" class="my_account_tabs w-tabs">
          <div class="my_account_tabs_menu-copy w-tab-menu">
            <a data-w-tab="Tab 1" class="my_acc_category w-inline-block w-tab-link w--current" href="#my-orders" id="my-orders">
              <div class="text-block-3">order details</div>
            </a>
            <a data-w-tab="Tab 2" class="my_acc_category w-inline-block w-tab-link" href="<?= $this->Url->build(['controller'=>'users','action'=>'profile']) ?>" id="profile" >
              <div class="text-block-4">change personal details</div>
            </a>
          </div>
          <div class="tabs-content-2 w-tab-content">
            <div data-w-tab="Tab 1" class="tab-pane w-tab-pane " >
              <div class="order_details_div">
                <div class="user_detail_div">
                  <div class="yc_name_email_div">
                    <div class="yc_name"><?= $user['first_name']." ".$user['last_name']?></div>
                    <div class="yc_email tt-none"><?= $user['username']?></div>
                  </div>
                  <div class="yc_credit_div">
                    <div class="yc_credit_txt_div">
                        <?= $this->Html->image('001-rupee-1.svg', ['alt' => 'CakeWeb','class'=>'ccke_web_credit_logo','width'=>'20']) ?>

                      <div class="yc_credit-text">cake web credits</div>
                    </div>
                    <div class="yc_credit_amt-text">
                      <span class="text-span-10"><?= $points?> </span>
                      (Every ₹100 spent = 2 Credit)
                    </div>
                  </div>
                </div>
              </div>
              <div class="yc_ord_det-tittle">
                  <?= $this->Html->image('009-signs.svg', ['alt' => 'CakeWeb','class'=>'image-9','width'=>'50']) ?>
                  
                <div class="yc_ord_det_text">Order Details</div>
              </div>
              <div class="ord_detail_tittle_div">
                <div class="order_desc_tittle">
                  <div class="order_tittle_text">order id &amp; product description</div>
                </div>
                <div class="order_date_tittle">
                  <div class="order_tittle_text">date</div>
                </div>
                <div class="order_status_tittle">
                  <div class="order_tittle_text">status</div>
                </div>
                <div class="order_total_tittle">
                  <div class="order_tittle_text">total</div>
                </div>
              </div>
              <?php foreach ($orders as $order) : ?>
                <div class="order_info_div">
                <div class="order_text_div">
                  <div class="yc_order_id">#<?= $order['code'] ?></div>
                  <div class="text-block-11">
                     <?php foreach ($order['order_items'] as $key => $item) {

                       echo $item['quantity']." ".$item['item_name']." ".$item['item_varaint_name'];
                       if ($key != sizeof($order['order_items'])-1 ) {
                         echo ", ";
                       }

                     }?> 
                  
                  
                  
                    </div>
                </div>
                <div class="ord_date_div">
                  <div class="yc_order_date"><?= date("M d, Y",strtotime($order['created'])) ?></div>
                </div>
                <?php if ($order['status'] > 0 ) :?>
                  <div class="status_div">
                      <?= $this->Html->image('successful_mark.svg', ['alt' => 'CakeWeb','class'=>'successful_icon','width'=>'20']) ?>
                      
                    <div class="msg_status_div">
                      <div class="successful_text">Successful</div>
                      <?php if ($order['status'] == 2 ) :?>
                        <div class="status_text">delivered</div>
                      <?php endif;?>
                    </div>
                  </div>
                <?php else : ?>
                  <div class="status_div">
                      <?= $this->Html->image('002-close.svg', ['alt' => 'CakeWeb','class'=>'successful_icon','width'=>'20']) ?>
                      
                    <div class="msg_status_div">
                      <div class="successful_text">unsuccessful</div>
                      <div class="status_text">failed</div>
                    </div>
                  </div>
                <?php endif; ?>
                
                <div class="total_div">
                  <div class="yc_order_total">₹<?= $order['grand_total']?></div>
                </div>
              </div>
              <?php endforeach;?>
              
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>