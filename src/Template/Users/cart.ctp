
<div class="container-fuild cs-hpro-area__chhkout" ng-controller="ProductController">
<?= $this->element('add_on_model') ?>

  <div class="payment_tab_section">
    <div class="payment_tab_container w-container">
        <a href="#" class="payment_tab_link payment_active">shopping cart</a>
        <?= $this->Html->image('right-arrow-black.svg', ['alt' => 'CakeWeb','class'=>'arrow_black_img','width' => '25']) ?>
        <a href="#" class="payment_tab_link " disabled>shipping</a>
        <?= $this->Html->image('right-arrow-black.svg', ['alt' => 'CakeWeb','class'=>'arrow_black_img','width' => '25']) ?>
        <a href="#" class="payment_tab_link" disabled>payment</a>
    </div>
  </div>
  <div class="shopping_cart_sec " >
    <div class="sc_container w-container" style="width:60%">
        <?php foreach ($cart_items as $key => $value): ?>
        <div class="sc_order_list_div">
            <div class="sc_item_img_div">
              <a href="<?= $this->Url->build(["controller" => "items","action" => "index",$value['url_slag']]);?>" class="sc_item_img_div"> 
              
                <?= $this->Html->image($value['small_image'], ['alt' => 'CakeWeb','class'=>'image-11']) ?>
              </a>
              </div>
            <div class="sc_mob_align">
            <div class="rem_items_mob_close_div">
                <?= $this->Html->image('close.svg', ['alt' => 'CakeWeb','class'=>'','width' => '10']) ?>
                
            </div>
            <div class="cart_item_name_div">
                <div class="sc_item_name_txt"><?= $value['name']?></div>
                <?php if (!empty($value['variant_name'])): ?>
                    <div class="sc_item_qty_txt"><?= $value['variant_name']?></div>
                <?php endif; ?>
                <div class="sc_msg_div">

                    <div>
                        <?php if (!empty($value['item_params']['delivery_message'])): ?>
                        <span class="text-span-12">Message</span>:
                        "<?= $value['item_params']['delivery_message'] ?>"
                        <?php endif; ?>
                    </div>
                    
                    <?php if ($value['item_params']['delivery_date'] != null): ?>
                        <div class="sc_del_date">
                            <span class="text-span-13">
                            Delivery on 
                            </span>
                            <?= date('d-M-Y',strtotime($value['item_params']['delivery_date'])) ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($value['item_params']['eggless'] == 1): ?>
                        <div>
                        <!-- <= $this->Html->image('veg-symbol.png') ?>  -->
                        Eggless (₹100 Extra per Kg)
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- <div class="qty_form_block w-form">
                <form id="email-form" name="email-form" data-name="Email Form" class="qty_form">
                    <input type="submit" value="-" data-wait="Please wait..." class="minus_button w-button">
                <label for="email" class="form_qty_label">Qty : <?= $value['qty'] ?></label>
                
                <input type="submit" value="+" data-wait="Please wait..." class="minus_button w-button">
                </form>
                <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form.</div>
                </div>
            </div> -->
            <!-- <div class="sc_item_amount">
                <div class="sc_amt_txt"><span class="text-span-11">Qty : </span><?= $value['qty']?></div>
            </div> -->
            <div class="sc_item_amount">
                <div class="sc_amt_txt"><span class="text-span-11">₹</span><?= $value['price']?></div>
            </div>
            </div>
            <div class="rem_items_div">
              <a href="<?= $this->Url->build(["controller" => "users","action" => "remove_from_cart",$value['item_id']]);?>">

                <?= $this->Html->image('close.svg', ['alt' => 'CakeWeb','class'=>'','width' => '10']) ?>
              </a>              
            </div>
        </div>
        <?php endforeach;?>
      <?php if( $cart_total['eggless'] > 0 || $cart_total['delivery_charges'] > 0 ) : ?>
      
      <div class="sc_summary_list_div">
        <div class="sc_item_summary_div">
          <?php if( $cart_total['eggless'] > 0 ) : ?>
          
          <div class="sc_eggless_txt">Eggless charges</div>
          <?php endif; ?>
          <?php if( $cart_total['delivery_charges'] > 0 ) : ?>
          <div class="sc_eggless_txt">Shipping charges</div>
          <?php endif; ?>
          
        </div>
        <div class="sc_summary_amount">
          <?php if( $cart_total['eggless'] > 0 ) : ?>
          
          <div class="sc_summary_amt_txt"><span class="text-span-11">₹</span><?= $cart_total['eggless'] ?></div>
          <?php endif; ?>
          
          
          <?php if( $cart_total['delivery_charges'] > 0 ) : ?>
          
          <div class="sc_amt_txt"><span class="text-span-11">₹</span><?= $cart_total['delivery_charges'] ?></div>
          <?php endif; ?>
          
        </div>
      </div>
      <?php endif; ?>
      
      <?php if( $cart_total['discount'] > 0) : ?>
      <div class="sc_summary_list_div">
        <div class="sc_item_summary_div">
          <div class="sc_eggless_txt">discount</div>
        </div>
        <div class="sc_summary_amount">
          <div class="sc_summary_amt_txt"><span class="text-span-11">₹</span><?= $cart_total['discount'] ?></div>
          
        </div>
      </div>
      <?php endif; ?>
      
      <div class="sc_subtotal_list_div">
        <div class="sc_item_summary_div">
          <div class="sc_subtotal_txt">subtotal</div>
        </div>
        <div class="sc_summary_amount">
          <div class="sc_summary_amt_txt"><span class="text-span-11">₹</span><?= $cart_total['grand_total']?></div>
        </div>
      </div>

      
      <div class="sh_pay_button_align">
        <div class="sh_btn_arrow_align" >
            <a href="<?= $this->Url->build(["controller" => "checkout","action" => "process"]);?>" id="proceed_link" class="sh_pay_button w-button" >
            Shipping 
                <span class="text-span-8">|</span></a>
            
            <?= $this->Html->image('right-arrow--white.svg', ['alt' => 'CakeWeb','class'=>'image-7','width' => '30']) ?>
            
        </div>
      </div>
    </div>
  </div>
  </div>
<script>
$(document).ready(function() {
    if(window.location.href.indexOf('#addons') != -1) {
      // alert();
        // $('#cs-addons').css('display');
        $('#cs-addons').css('justify-content','center');
        $('#cs-addons').css('display','flex');
        
        
    }
});
</script>
