<?php if (env('LESS') != 'true'): ?>
    <?= $this->Html->css('signup-style') ?>
<?php endif; ?>
<?= $this->Flash->render() ?>
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center form-title ls-mtop80">
 			<h2  class="ls-pB0">Reset Password</h2>
            <p>Please enter new password</p>
           <hr class="hidden-xs">
		</div>
	</div>


	<div class="row">
		<div class="col-md-12 ls-signup">
        <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">

            <?= $this->Form->create() ?>

				<div class="group">
    				<input name="password" type="password" class="noFocus" required><span class="highlight"></span><span class="bar"></span>
   						<label>Password</label>
 				 </div>
 				<div class="group">
   					 <input name="rtpassword" type="password" required><span class="highlight"></span><span class="bar"></span>
    					<label>Retype Password</label>
  				</div>

  				<button type="submit" class="button buttonOrange">Reset Password
    			<div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
 			    </button>
                <?= $this->Form->end() ?>

 				<p class="text-center">Remember Password? <a class="hov" href="<?= $this->Url->build(['controller'=>'Users','action'=>'account','#login']) ?>">Login</a></p>
            </div>
            </div>
		</div>
	</div>
</div>
