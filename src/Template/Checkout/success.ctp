<div class="success_section">
    <div class="succ_cont w-container">
        <?= $this->Html->image('Group-21.png',["alt" => "Cake web","class"=>"succ_call_card_img"]) ?>

        <div class="succ_text">success</div>
        <div class="well_done_txt">Well done! for successfully placing your order</div>
        <div class="text-block-15">Your order is currently being processed. Your order ID is 
            <span class="text-span-14"><?= $order['code']?>.</span></div>
        <div class="text-block-16">You will also receive the conformation via mail and SMS shortly.</div>
        <div class="succ_transaction_div">
            <div class="exp_del_div">
                <div class="success_div_txt">Order Id</div>
                <div class="success_div_txt_red"><?= $order['code']?></div>
            </div>
            <div class="exp_del_div">
                <div class="success_div_txt">total amount</div>
                <div class="success_div_txt_red">₹<?= $order['grand_total']?></div>
            </div>
            <div class="exp_del_div">
                <div class="success_div_txt">payment mode</div>
                <div class="success_div_txt_red"><?= $order['payment_type']?></div>
            </div>
        </div>
    </div>
</div>