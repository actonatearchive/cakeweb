<div class="success_section">
    <div class="fail_cont w-container">
        <?= $this->Html->image('Group-23.png',["alt" => "Cake web","class"=>"succ_call_card_img"]) ?>

        <div class="fail_text">Error</div>
        <div class="well_done_txt">Oops! something went wrong</div>
        <div class="text-block-15">Sorry, please  <a href="<?= $this->Url->build(['controller'=>'users','action'=>'cart'])?>" class="err_shopping"> try again</a>. Continue 
            <a href="<?= $this->Url->build(['controller'=>'home','action'=>'index'])?>" class="err_shopping">shopping</a></div>
    </div>
</div>