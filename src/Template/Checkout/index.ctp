  <div class="payment_tab_section">
    <div class="payment_tab_container w-container">
      <a href="<?= $this->Url->build(["controller" => "users","action" => "cart"]);?>" class="payment_tab_link">shopping cart</a>
        <?= $this->Html->image('right-arrow-black.svg', ['alt' => 'CakeWeb','class'=>'arrow_black_img','width' => '25']) ?>

      <a href="#" class="payment_tab_link payment_active">shipping</a>
        <?= $this->Html->image('right-arrow-black.svg', ['alt' => 'CakeWeb','class'=>'arrow_black_img','width' => '25']) ?>   
      <a href="#" class="payment_tab_link" disabled>payment</a></div>
  </div>

  <div class="sh_main_section">
    <div class="sh_main_container w-container" style="">
      <?php if($user_addresses != null) : ?>
        <div class="sh_address_list_div">
          <div class="sh_addr_tittle">saved address</div>
        

          <?php foreach($user_addresses as $key => $val) :?>

            <div class="sh_address_div jq-address-box" id="address_box_<?= $val['id'] ?>" onclick="selectAddress('<?= $val['id'] ?>')" >
            `<label class="sh_addr_align_div" for="checkbox-<?= $val['id']?>">
                <div class="form-block-4 w-form">
                  <!-- <form id="email-form" name="email-form" data-name="Email Form"> -->
                    <div class="w-checkbox">
                      <input type="radio" id="checkbox-<?= $val['id']?>" name="checkbox-2" data-name="Checkbox 2" class="w-checkbox-input">
                      <span class="checkdot"></span>
                    </div>
                  <!-- </form> -->
                </div>
                <div class="">
                  <div class="sh_addr_name"><?= $val['name']?></div>
                  <div class="sh_addr_addr"><?= $val['line1'] ?>,<?= $val['landmark'] ?> <br>
                    <?= $val['city_name'] ?>,<?= $val['state_name'] ?> - 
                    <?= $val['pin_code'] ?></div>
                  <div class="sh_addr_mob"><?= $val['mobile']?></div>
                    <textarea id="address_<?= $val['id'] ?>" class="hidden">
                      <?= json_encode($val) ?>
                    </textarea>

                  <a class="mr-3 add-link" href="javascript:void(0)" onclick="editAddress('<?= $val['id'] ?>')">Edit</a>
                  <a class="add-link" href="<?= $this->Url->build(['controller' => 'checkout','action' => 'delete-address', $val['id']]) ?>">Delete</a>
                </div>
              </label>
            </div>
            
          <?php endforeach; ?>
           <div class="sh_addr_div " >
          
          <button type="button" class="sh_pay_button w-button"  id="show-new-address">
            Add New Address 
          
          </button>
          </div>

        </div>
      <!-- <div class="sh_or_text">or</div> -->
     
    <?php endif; ?>


      <div class="sh_new_addr_text jq-address" <?php if($user_addresses != null){ echo 'style="display:none;"'; }?>>add new address</div>
      <div class="new_addr_form_block w-form jq-address" <?php if($user_addresses != null){ echo 'style="display:none;"'; }?>>
       <?= $this->Form->create(null, ['id'=>'address_form','url' => ['controller'=>'checkout','action' => 'add-new-address'],'onsubmit'=>'return checkDeliveryAddress(this)']) ?>
          <input id="inputAddressID" name="address_id" type="hidden" value="" />

          <div class="sh_name_div">
            <div class="sh_first_name_input_div">
              <input type="text" class="sh_first_inpbox w-input" maxlength="256" autofocus="true" id="inputFullName" name="name"  data-name="Name" placeholder="" required >
              <div class="first_name_latel">full name</div>
            </div>
            <div class="sh_last_name_input_div">
              <input type="text" class="sh_first_inpbox w-input" id="inputPhoneNumber" name="mobile" placeholder="" required maxlength="10">
              <div class="first_name_latel">phone number</div>
            </div>
          </div>
          
          <div class="sh_addr_div">
            <div class="sh_address_input_div">
              <input type="text" class="sh_first_inpbox w-input"  autofocus="true"  placeholder="" required id="inputAddress1" name="line1">
              <div class="first_name_latel">Address</div>
            </div>
            <div class="sh_zipcode_input_div">
              <input type="text" class="sh_zipcode_inpbox w-input" id="inputLandmark" name="landmark" placeholder="" required >
              <div class="first_name_latel">Landmark</div>
            </div>
          </div>
          <div class="sh_state_div">
            <div class="sh_city_input_div">
              <input type="text" class="sh_first_inpbox w-input" id="inputPincode" name="pin_code" placeholder="" required maxlength="6" onkeypress="return isNumberKey(event)" onkeyup="getStateAndCity()" >
              <div class="first_name_latel">Pincode</div>
            </div>
            <div class="sh_city_input_div">
              <input type="text" class="sh_first_inpbox w-input" readonly id="inputCity" name="city_name" >
              <div class="first_name_latel">City</div>
            </div>
            <div class="sh_city_input_div">
              <input type="text" class="sh_first_inpbox w-input"  readonly id="inputState" name="state_name" >
              <div class="first_name_latel">state</div>
            </div>
            
          </div>
          <div class="sh_addr_div " style="justify-content: center;">
          
          <button type="submit" class="sh_pay_button w-button"  id="address_submit_btn" >
            Save New Address 
          
          </button>
          </div>
          
          
          <!-- <div class="sh_delivert_selection">
            <div class="radio-button-field w-radio"><input type="radio" id="radio" name="radio" value="Radio" data-name="Radio" class="w-radio-input"><label for="radio" class="field-label-4 w-form-label"></label>
              <div class="radio_info_div">
                <div>Standard ( <span class="text-span-7">free </span>)</div>
                <div class="sel_time">12 pm- 9 pm</div>
              </div>
            </div>
            <div class="radio-button-field w-radio"><input type="radio" id="radio-2" name="radio" value="Radio" data-name="Radio 2" class="w-radio-input"><label for="radio-2" class="field-label-4 w-form-label"></label>
              <div class="radio_info_div">
                <div>Midnight ( ₹ 250<span class="text-span-7"> </span>)</div>
                <div class="sel_time">9 pm- 12 am</div>
              </div>
            </div>
          </div> -->
        
        </form>

          
      </div>
      <br>

      <?= $this->Form->create(null, ['url' => ['controller'=>'checkout','action' => 'updateAddress']]) ?>
      
      <?php if($points > 0) : ?>
      <div class="sc_credits_div">
        <div class="sc_credit_tittle">
          <div class="sc_credit_text">cake web credits</div>
        </div>
        <div class="sc_credit_div">
                <?= $this->Html->image('001-rupee-1.svg', ['alt' => 'CakeWeb','class'=>'','width' => '25']) ?>
                
          <div class="sc_points"><?= $points?></div>
          <div class="form-block-5 w-form">
              <div class="radio-button-field-3 w-radio">
                <input type="checkbox" id="apply-points" name="apply_points"  class="w-radio-input"
                  <?= ($order['reward_points'] > 0) ? 'checked' : '' ; ?>
                >
                <label for="apply-points" class="w-form-label">Use Credit Balance</label>
              </div>
               <?php $grand_total = ($order['reward_points'] > 0) ? $order['grand_total']+$order['discount_value'] : $order['grand_total'] ; ?>
              
              <input type="hidden" data="<?= $order['reward_points'] ?>" id="temp_link" value="<?= $this->Url->build(["controller" => "checkout","action" => "update-address"]);?>">
              <input type="hidden" data="<?= $points ?>" id="temp_grand_total" value="<?= $grand_total ?>">
          </div>
        </div>
      </div>
      <?php endif;?>
      <div class="sh_total_text">Total </div>
          <div class="sh_total_price_text" >₹<span id="grand_total"> <?= $order['grand_total'] ?> </span> </div>
          <div class="sh_pay_button_align">
            <div class="sh_btn_arrow_align">
              <input type="hidden" name="shipping_address_id" id="final_address_id" />
              <input type="hidden" name="order_id" id="final_order_id" value="<?= $order['id'] ?>" />  
              <button class="sh_pay_button w-button" >
                payment 
                <span class="text-span-8">|</span>
              <?= $this->Html->image('right-arrow--white.svg', ['class'=>'image-7','width'=>'30']) ?>
              
              </button>
            
                
            </div>
          </div>

    </div>
            <?= $this->Form->end();?>
    
  </div>
<script>
   $('#apply-points').click(function () {
      
      if($('#temp_link').attr('data') == 0){
        $('#temp_link').attr('data',1);
      $('#grand_total').html($('#temp_grand_total').val()-$('#temp_grand_total').attr('data'));
        
      } else {
        $('#temp_link').attr('data',0);
        $('#grand_total').html($('#temp_grand_total').val());
      }

    });
  
  </script>