  <div class="payment_tab_section">
    <div class="payment_tab_container w-container">
      <a href="<?= $this->Url->build(["controller" => "users","action" => "cart"]);?>" class="payment_tab_link">shopping cart</a>
        <?= $this->Html->image('right-arrow-black.svg', ['alt' => 'CakeWeb','class'=>'arrow_black_img','width' => '25']) ?>

      <a href="#" class="payment_tab_link">shipping</a>
        <?= $this->Html->image('right-arrow-black.svg', ['alt' => 'CakeWeb','class'=>'arrow_black_img','width' => '25']) ?>

      <a href="#" class="payment_tab_link payment_active" disabled>payment</a></div>
  </div>


<div class="py_main_container">
    <div class="py_main_container w-container">
        <div class="">
          <div class="py_mode_sel_div ">
              <div class="py_mode_tittle py_trans_id_div" style="    color: #474747;padding: 10px;">
                <div class="py_transaction_id" style="float:left">
                    transaction iD : <?= $order['code']?>
                </div>
          
                <div class="py_transaction_id" style="float:right">
                    payable amount : 
                    <span class="text-span-9">₹<?= $order['grand_total']?></span>
                </div>
                </div>
          </div>
        </div>
        <div class="py_main_div">
            <div class="py_mode_sel_div">
                <div class="py_mode_tittle">Select your payment method</div>
                <!-- onClick="showbox('.py_card_layout_div')" -->
                <a href="#" class="py_credit_link_div py_active cod w-inline-block" onClick="form_submit('#offline_pay_form')" >
                    <?= $this->Html->image('cod.svg', ['alt' => 'CakeWeb','class'=>'image-8','width'=>'40']) ?>
                    <div class="py_cord_text">cash on delivery</div>
                </a>
                <a href="#" class="py_credit_link_div paytm w-inline-block" onClick="form_submit('#pay_paytm_form_id')">
                    <?= $this->Html->image('money.svg', ['alt' => 'CakeWeb','class'=>'image-8','width'=>'40']) ?>
                    <div class="py_cord_text">paytm</div>
                </a>
            </div>
            <div class="py_card_layout_div hidden">
                <div class="py_card_form_block w-form">
                    <form id="email-form" name="email-form" data-name="Email Form" class="py_card_form">
                        <div class="py_card_design" >
                            <!-- <div class="py_font">Enter your mobile Number to verify</div> -->
                            <!-- <div class="otp_text padding-top-20"> Sorry, last attempt was unsuccessful, please try again </div> -->
                            <div class="otp_text padding-top-10"></div>
                            
                            <div class="py_mob_div" id="mobile-box">
                                <div class="otp_text"> Verify your mobile number before sending the otp. </div>
                            
                              <div class="py_card_no_input_div py_display_inlin">
                                <input type="text" class="py_card_no_inpbox w-input" maxlength="10" autofocus="true"  placeholder="+91 99999 999 99" id="mobile" value="<?= $user['mobile']?>" >
                                <div class="first_name_latel">Mobile No</div>
                              </div>
                              <div class="sh_pay_button_align py_display_inlin wdh_30">
                                <div class="sh_btn_arrow_align">
                                    <a href="#" class="sh_pay_button w-button" onClick="showbox('#otp-box');hidebox('#mobile-box');"> Verify </a>
                                </div>
                              </div>
                            </div>
                            <div class="non hidden" id="otp-box">
                              <div class="otp_text"> OTP has been sent to your mobile number </div>
                              <center>
                                <div class="py_otp_div">
                                  <div class="sh_last_name_input_div w-80 ">
                                    <input type="text" class="sh_first_inpbox w-input mar-0" maxlength="256" autofocus="true" name="name-2" data-name="Name 2" placeholder="Enter otp" id="name-2">
                                    <div class="first_name_latel">OTP</div>
                                  </div>
                                </div>
                              </center>
                              <div class="sh_pay_button_align">
                                <div class="sh_btn_arrow_align martp-30_mob"><a href="#" class="sh_pay_button w-button" onClick="form_submit('#offline_pay_form')"> Confirm Order <span class="text-span-8">|</span></a>
                                  <?= $this->Html->image('right-arrow--white.svg', ['alt' => 'CakeWeb','class'=>'image-7','width'=>'30']) ?>
                                </div>
                              </div>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
  echo $this->Form->create('cod', array(
      'url' =>['controller' =>'checkout','action' => 'processCod',$order['id']],
      'id' => 'offline_pay_form','autocomplete' => 'off')
  );

  echo $this->Form->input('status',array('type' => 'hidden','value' => 'cod'));
  echo $this->Form->input('order_id',array('type' => 'hidden','value' => $order['id']));
  
  echo $this->Form->end();
?>


<form method="post" action="<?= $paytmOutObj['PAYTM_TXN_URL'] ?>" name="f1" id="pay_paytm_form_id">
    <?php
        foreach($paytmOutObj as $name => $value) {
        echo '<input type="hidden" name="' . $name .'" value="' . $value . '">';
        }

    ?>
    <!-- <input type="hidden" name="CALLBACK_URL" value="<?= $this->Url->build(['action'=>'processPaytm','controller'=>'checkout']) ?>"> -->
</form>
