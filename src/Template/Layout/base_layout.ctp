<!DOCTYPE html>
<html data-wf-page="5a1cff608802490001d43ee7" data-wf-site="5a1cff608802490001d43ee6">
<head>
    <meta charset="utf-8">
    <title><?= $page_title ?> - CakeWeb</title>
    <meta name="description" content="<?= $meta_description ?>">
    <meta name="keywords" content="<?= $meta_keywords ?>" />
    <meta name="author" content="Actonate.com">
    <meta name="theme-color" content="#F75D59">

    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="<?= $this->Url->image('fav/favicon-16x16.png') ?>" rel="shortcut icon" type="image/x-icon">
    <!-- MEtA -->
   <link rel="apple-touch-icon" sizes="57x57" href="<?= $this->Url->image('fav/apple-icon-57x57.png') ?>">
   <link rel="apple-touch-icon" sizes="60x60" href="<?= $this->Url->image('fav/apple-icon-60x60.png') ?>">
   <link rel="apple-touch-icon" sizes="72x72" href="<?= $this->Url->image('fav/apple-icon-72x72.png') ?>">
   <link rel="apple-touch-icon" sizes="76x76" href="<?= $this->Url->image('fav/apple-icon-76x76.png') ?>">
   <link rel="apple-touch-icon" sizes="114x114" href="<?= $this->Url->image('fav/apple-icon-114x114.png') ?>">
   <link rel="apple-touch-icon" sizes="120x120" href="<?= $this->Url->image('fav/apple-icon-120x120.png') ?>">
   <link rel="apple-touch-icon" sizes="144x144" href="<?= $this->Url->image('fav/apple-icon-144x144.png') ?>">
   <link rel="apple-touch-icon" sizes="152x152" href="<?= $this->Url->image('fav/apple-icon-152x152.png') ?>">
   <link rel="apple-touch-icon" sizes="180x180" href="<?= $this->Url->image('fav/apple-icon-180x180.png') ?>">
   <link rel="icon" type="image/png" sizes="192x192"  href="<?= $this->Url->image('fav/android-icon-192x192.png') ?>">
   <link rel="icon" type="image/png" sizes="32x32" href="<?= $this->Url->image('fav/favicon-32x32.png') ?>">
   <link rel="icon" type="image/png" sizes="96x96" href="<?= $this->Url->image('fav/favicon-96x96.png') ?>">
   <link rel="icon" type="image/png" sizes="16x16" href="<?= $this->Url->image('fav/favicon-16x16.png') ?>">
   <link rel="manifest" href="<?= $this->request->webroot."img/fav/manifest.json" ?>">
   <meta name="msapplication-TileColor" content="#4c1d16">
   <meta name="msapplication-TileImage" content="<?= $this->Url->image('fav/ms-icon-144x144.png') ?>">
   <meta name="theme-color" content="#4c1d16">
   <!-- ENDS HERE -->

    <!-- Dependencies CSS -->
    <?= $this->Html->css(['mystyle','owl.carousel.min','owl.theme.default.min']) ?>
    <?= $this->Html->css(['normalize','webflow','cake-web.webflow','bootstrap-datepicker.min']) ?>
    <!-- Custom CSS -->
    <?= $this->Html->css('fixes.css') ?>

    <!-- <link rel="stylesheet" href="owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="owlcarousel/owl.theme.default.min.css"> -->



    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Work Sans:300,regular,500,600,700","Rancho:regular"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    
        <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    
    <style>
        .w-slider-dot {
        background: #febbbd;
        }
        .w-slider-dot.w-active {
        background:#ff676b;
        }
        .w-container,
        .site-container {
        max-width:1170px;
        }
        ._w-container {
        max-width: 1170px;
        width: 1170px;
        }
        @media screen and (max-width: 991px) {
        ._w-container {
            width: 0px;
        }
        }
        label{
            cursor : pointer;
        }
        .tt-none{
                text-transform: none;
        }
        /*new class 1*/
.py_pay_amnt_div {
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin: 0 auto;
  }

   /*new class 2*/
   .py_otp_div {
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
     display: inline-block;
  }

  /*new class 3*/
  .py_font {
    font-size: 18px;
    margin-top: 10px;
    margin-bottom: 10px;
  }

  /*new class 4*/
  .py_active {
    background-color: #fff;
  }

    </style>
    <noscript>
        <h1>Please Enable JavaScript in order to use Website properly.</h1>
    </noscript>

    <script>
        // var baseUrl = "/";

        var baseUrl = '<?= $this->request->webroot ?>';
    </script>
    <!-- Dependencies -->
    <?php if ($script_configs['GOOGLE_ANALYTICS']): ?>
        <?= $this->element('google_analytics') ?>
    <?php endif; ?>
    <!-- Zendesk -->
    <?php if ($script_configs['ZEN_DESK']): ?>
        <?= $this->element('zendesk') ?>
    <?php endif; ?>
     <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <!-- Dependencies -->
    <?= $this->Html->script(['jquery-3.1.1.slim.min','https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js','tether.min']) ?>
    <?= $this->Html->script('bootstrap.min') ?>

    <?= $this->Html->script('angular.min.js')?>
    <!-- Custom scripts -->
    <!-- Library -->
    <?= $this->Html->script(['library/q','library/services','bootstrap-datepicker.min','jquery-payment']) ?>

    <?= $this->Html->script(['scripts','app','controllers/CategoryController','controllers/ProductController']) ?>

    <?= $this->Html->script(['webflow','ng-infinite-scroll.min']) ?>
    <?= $this->Html->script('jquery.elevatezoom'); ?>
    <?= $this->Html->script('owl.carousel.min'); ?>
    <!-- // <script src="owlcarousel/owl.carousel.min.js"></script> -->


</head>
<body class="body" ng-app="cakeweb">
  <style>
    a {
        text-decoration : none !important;
    }
    a:hover {
        text-decoration : none;
    }
    .pointer {
        cursor : pointer;
    }
  </style>
 <?php
        echo $this->Form->create(null, ['url' => ['controller'=>'users','action' => 'add_to_cart'],'id'=>'atc-form']);
        echo $this->Form->control('item_id',['type'=>'hidden','id'=>'atc-item-id']);
        echo $this->Form->control('qty',['type'=>'hidden','value'=>1]);
        echo $this->Form->control('delivery_date',['type'=>'hidden','value'=>date('Y-m-d'),'id'=>'base_delivery_date']);
        echo $this->Form->control('message_cake',['type'=>'hidden','value'=>'','id'=>'base_message_cake']);
        echo $this->Form->control('optional_message',['type'=>'hidden','value'=>'','id'=>'base_optional_message']);
        echo $this->Form->control('egg_less',['type'=>'hidden','value'=>0,'id'=>'base_egg_less']);
        echo $this->Form->control('midnight',['type'=>'hidden','value'=>0,'id'=>'base_midnight']);
        echo $this->Form->end();


       
    ?>


<?php if($this->request->action != 'city') : ?>
<?= $this->element('header') ?>
<?php endif; ?>


<?= $this->fetch('content') ?>
<?php if($this->request->action != 'city') : ?>

<?= $this->element('footer',['cache'=>true]) ?>
<?php endif; ?>



<script>
    $(document).ready(function() {
        $( "#datepicker-1" ).datepicker();
        $( ".datepicker" ).datepicker();
        var page = window.location.hash.substr(1);
        if(page == null || page == '' ){
            page = 'signin';
        }
        divLoad(page);
        // $('#'+page).click(function () {
        //     alert();
        //     $('.jq-account').css('display','none');
        //     $('#'+page).css('display','block');
            
        // });


        console.log(page);
    });
    function divLoad(page) {
        $('.jq-account').css('display','none');
            $('#'+page).css('display','block');
    }
  $(document).ready(function(){
      $("#sign-up-link").click(function() {
        $("#signin").css('display','none');
        $("#forgot_password").css('display','none');
        
        $("#signup").css('display','block');
      });
      $("#sign-in-link").click(function() {
      	$("#signup").css('display','none');
        $("#signin").css('display','block');
        $("#forgot_password").css('display','none');
        
      });
      $("#sign-in-link2").click(function() {
      	$("#signup").css('display','none');
        $("#signin").css('display','block');
        $("#forgot_password").css('display','none');
        
      });
      $("#forgot_password_link").click(function() {
        //   alert('sad');
      	$("#signup").css('display','none');
        $("#signin").css('display','none');
        $("#forgot_password").css('display','block');
        
      });
      $("#show-new-address").click(function () {
          $('.jq-address').css('display','block');
      });

  });
  function getStateAndCity()
    {
        var pincode = document.getElementById('inputPincode').value;
        console.log("Pincode: ", pincode);
        console.log(pincode.length);
        if (pincode.length == 6) {
            var url = "https://maps.googleapis.com/maps/api/geocode/json?&address="+pincode+"&components=country:IN";

            RequestGet(url,'text')
                .then(function(data) {
                    console.log("Response: ", data);

                    var mArr = data.results[0].address_components;
                    if(mArr && mArr[mArr.length - 2].long_name !== undefined) {
                        var state = mArr[mArr.length - 2].long_name;
                        var city = mArr[mArr.length - 3].long_name;

                        // console.log(state);
                        // console.log(city);
                        if(state !== "India") {
                            // $('.jq-state').val(state);
                            document.getElementById('inputState').value = state;
                            document.getElementById('inputCity').value = city;
                            // $('.jq-city').val(city);
                        }
                    }
                })
                .catch(function(err) {
                    console.log("Error: ", err);
                });
        }
    }

    function selectAddress(address_id)
    {
        $('.jq-address-box').removeClass('active');
        $('#address_box_'+address_id).addClass('active');

        //Selected address
        document.getElementById('final_address_id').value = address_id;
    }

    function resetForm()
    {
        document.getElementById("address_form").reset();
        document.getElementById('inputAddressID').value = '';
        document.getElementById('address_submit_btn').innerHTML = "Add New Address";

    }
    function editAddress(address_id) {
        console.log("Address: ", address_id);
        var address = JSON.parse(document.getElementById('address_'+address_id).innerHTML);

        if (address_id === address.id) {
            document.getElementById('inputFullName').focus();
            document.getElementById('inputAddressID').value = address_id;
            document.getElementById('inputFullName').value = address.name;
            document.getElementById('inputPhoneNumber').value = address.mobile;
            document.getElementById('inputAddress1').value = address.line1;
            // document.getElementById('inputAddress2').value = address.line2;
            document.getElementById('inputLandmark').value = address.landmark;
            document.getElementById('inputPincode').value = address.pincode;
            document.getElementById('inputCity').value = address.city;
            document.getElementById('inputState').value = address.state;
            document.getElementById('address_submit_btn').innerHTML = "Modify Address";
        }
        console.log("Address: ", address);


    }
</script>

<script>
function showbox(params) {
  $(params).show();   
}
function hidebox(params) {
  $(params).hide();   
}
function form_submit(params) {
    $(params).submit();
}

$('#search_in').keypress(function (event) {
  if (event.keyCode == 10 || event.keyCode == 13) {
    $('#org_search').val( $(this).val() );
    $('#search-form').submit();
  }
});

// owl carousel for add on model 
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});

$('.owl-carousel').owlCarousel({
  loop:true,
  margin:10,
  nav: false,
  navText: ['',''],
  responsiveClass:true,
  responsive:{
    0:{
        items:1,
        nav:true
    },
    600:{
        items:2,
        nav:false
    },
    1000:{
        items:4,
        nav:true,
        loop:false
    }
  }
});

</script>
</body>

</html>
