
<tr>
<td bgcolor="#ffffff" align="center" style="padding: 20px 15px 10px 0px;" class="section-padding">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">

  <tr>

  </tr>
  <tr>
      <td style="width:5%"></td>
      <td style="font-size:18px;font-family:Helvetica,Arial,sans-serif;color:#333333;padding-top:30px">
          <br>
          <font style="font-size:15px;line-height: 23px;">Thank you for your order. We are working to get your order to you as soon as possible. We'll send the confirmation email when your order is shipped.</font><br>
          <font style="font-size:15px;line-height: 23px;">Please take few moments to have a look at your items.</font>
          <br>
          <br>
      </td>
      <td style="width:5%"></td>
  </tr>
  <tr>
      <td style="width:5%"></td>
      <td style="font-family:Arial;">
          <table width="100% ">
              <tr>
                  <th style="border-top:1px solid #d2d0d0;border-right:1px solid #d2d0d0;text-align:center;padding: 2% 0px;">ORDER ID</th>
                  <th style="border-top:1px solid #d2d0d0;text-align:center;padding: 2% 0px;">ORDER DATE</th>
              </tr>
              <tr>
                  <td style="border-bottom:1px solid #d2d0d0;border-right:1px solid #d2d0d0;text-align:center;padding: 2% 0px;border-top: 1px solid #d2d0d0;"><?= $order['code'] ?></td>
                  <td style="border-bottom:1px solid #d2d0d0;text-align:center;padding: 2% 0px;border-top: 1px solid #d2d0d0;"><?= date('dS F Y, l',strtotime($order['order_placed_at']))?></td>
              </tr>
          </table>
      </td>
      <td style="width:5%"></td>
  </tr>

  <tr>
      <td style="width:5%"></td>
      <td align="center" style="padding: 8px 14px;font-family: Arial;font-size: 15px;font-weight: bold;">
          <br> HERE'S YOUR ORDER SUMMARY<br>
      </td>
      <td style="width:5%"></td>
  </tr>
  <tr>
      <td style="width:5%"></td>
      <td style="background-color:#ececec;font-family: Arial;font-size: 14px;color: #616060;line-height: 20px;">
          <table style="margin: 00px auto;width: 665px;">
              <tr style="background: #fff;border: 1px solid #ccc;width: 800px;height: 40px;color:#000;text-align: center;font-weight: bold;">
                  <td>
                      <p style="font-size: 15px;">PRODUCT</p>
                  </td>
                  <td>
                      <p style="font-size: 15px;">DESCRIPTION</p>
                  </td>
                  <td>
                      <p style="font-size: 15px;">AMOUNT</p>
                  </td>
              </tr>
              <?php foreach ($order['order_items'] as $key => $value): ?>
                  <tr style="background: #fff;border: 1px solid #ccc;width: 800px;height: 40px;color:#000;text-align: center;font-weight: bold;">
                      <td align="center" style="font-size: 15px; font-family: Verdana, sans-serif; color: #333333; padding: 5px 15px 5px 15px" class="padding">
                          <img src="<?= $value['item_image'] ?>">
                      </td>
                      <td align="center" style="font-size: 15px; font-family: Verdana, sans-serif; color: #333333; padding: 5px 15px 5px 15px" class="padding">
                          <b style="font-size: 15px;text-align:center;display: block;"><?= $value['item_name'] ?></b>
                          <!-- <span style="color:#00a8e0;font-weight:bold;"><?= $value['short_desc'] ?></span><br/> -->
                          Quantity: <?= $value['quantity'] ?>
                      </td>
                      <td align="center" style="font-size: 15px; font-family: Verdana, sans-serif; color: #333333; padding: 5px 15px 5px 15px" class="padding">
                          Rs. <?= $value['price'] * $value['quantity'] ?>
                          <?php
                              $order['subtotal'] += ($value['price'] * $value['quantity']);
                          ?>
                      </td>
                  </tr>
              <?php endforeach; ?>
              <tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                  <td></td>
                  <td style="background: #fff;text-align: center;color:#000;font-size: 12px;text-transform: uppercase;">Subtotal</td>
                  <td style="text-align:center;background: #fff;"><b>Rs. <?= $order['subtotal'] ?></b></td>
              </tr>

              <tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                  <td></td>
                  <td style="background: #fff;text-align: center;color:#000;font-size: 12px;text-transform: uppercase;">Additional charges</td>
                  <td style="background: #fff;text-align:center;">
                      <b>
                          <center>Rs.<?= $order['additional_charges'] ?></center>
                      </b>
                  </td>
              </tr>

              <?php if ($order['eggless'] > 0): ?>
                  <tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                      <td></td>
                      <td style="background: #fff;text-align: center;color:#000;font-size: 12px;text-transform: uppercase;">Eggless</td>
                      <td style="background: #fff;text-align:center;">
                          <b>
                              <center>Rs.<?= $order['eggless'] ?></center>
                          </b>
                      </td>
                  </tr>
              <?php endif; ?>

              <?php //if (($order['discount_value'] + $order['extra_discount']) > 0): ?>
              <!-- <tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                  <td></td>
                  <td style="background: #fff;text-align: center;color:#000;font-size: 12px;text-transform: uppercase;">Discount</td>
                  <td style="background: #fff;text-align:center;">
                      <b>
                          <center>Rs.<?= $order['discount_value'] + $order['extra_discount'] ?></center>
                      </b>
                  </td>
              </tr> -->
              <?php //endif; ?>



              <tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                  <td></td>
                  <td style="background: #fff;text-align: center;color:#000;font-size: 12px;text-transform: uppercase;">total</td>
                  <?php
                      $grand_total = $order['total_amount']+$order['additional_charges']+$order['eggless'];
                  ?>
                  <td style="background: #fff;text-align:center;"><b>Rs. <?= $grand_total ?></b></td>
              </tr>
          </table>
      </td>
      <td style="width:5%"></td>
  </tr>
  <tr>
      <td style="width:5%"></td>
      <td align="center" style="padding: 8px 14px;font-family: Arial;font-size: 15px;font-weight: bold;">
          <br>
          <br> YOUR ORDER IS BEING SHIPPED TO
          <br>
      </td>
      <td style="width:5%"></td>
  </tr>
  <tr>
      <td style="width:5%"></td>
      <td style="background-color:#ececec;font-family: Arial;padding: 8px 0px 8px 12px;font-size: 14px;color: #616060;line-height: 20px;" align="center">
          <b><?= $order['shipping_address']['name']?></b><br/>
          <?= $order['shipping_address']['line1']
          ." ".$order['shipping_address']['line2']." ".$order['shipping_address']['landmark']." ".$order['shipping_address']['city_name']." - ".$order['shipping_address']['pin_code']
          ?>
      </td>
      <td style="width:5%"></td>
  </tr>



</table>
</td>
</tr>
