<tr>
    <td bgcolor="#ffffff" align="center" style="padding: 20px 15px 10px 0px;" class="section-padding">
        <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
            <tr>
                <td>
                    <!-- chimp IMAGE -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="padding: 20px 30px 20px 30px">
                                <!-- see you soon -->
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="font-size: 60px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px; padding-bottom: 5px; font-weight:bold; border-top: 3px solid #000000; border-bottom: 3px solid #000000" class="padding">Oops!</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php
                            $baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] &&
                            !in_array(strtolower($_SERVER['HTTPS']), array('off','no')))
                            ? 'https' : 'http';
                            $baseUrl .= '://'.$_SERVER['HTTP_HOST'].$this->request->webroot;
                        ?>
                        <tr>
                            <td align="center">
                                <!-- hello asawri -->
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <!-- COPY -->
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding">Dear <?= $user['first_name']." ".$user['last_name'] ?>,</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="padding: 10px 0 30px 0; font-size: 15px; line-height: 25px; font-family: Verdana, sans-serif; color: #666666;" class="padding">We heard that you forgot your password. Don't worry we'll make it right for you. Please reset password using the link provided below : <br/>
                                                    <a href="<?= $user['link'] ?>"> Reset Password Link </a>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
    </td>
</tr>
