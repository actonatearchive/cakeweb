<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
*   ItemCategories Model
*/
class ItemCategoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('item_categories');
        $this->primaryKey('id');

        $this->addBehavior(
            'Timestamp', [
                'events' => [
                    'Model.beforeSave' => [
                        'created' => 'new',
                        'modified' => 'always'
                    ]
                ]
            ]
        );
       

        $this->belongsTo(
            'ParentCategory', [
                'className' => 'ItemCategories',
                'through' => 'ItemCategories',
                'foreignKey' => 'item_category_id'
                // 'bindingKey' => 'item_id'
            ]
        );

        $this->belongsToMany(
            'ChildCategories', [
                'className' => 'ItemCategories',
                'through' => 'ItemCategories',
                'foreignKey' => 'item_category_id',
                'targetForeignKey' => 'id',
            ]
        );
        
    }
}
