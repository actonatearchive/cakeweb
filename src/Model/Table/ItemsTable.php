<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
*   Items Model
*/
class ItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('items');
        $this->primaryKey('id');

        $this->addBehavior(
            'Timestamp', [
                'events' => [
                    'Model.beforeSave' => [
                        'created' => 'new',
                        'modified' => 'always'
                    ]
                ]
            ]
        );
        //Relations goes here....
        $this->belongsTo(
            'ItemCategories', [
                'foreignKey' => 'item_category_id',
                'joinType' => 'INNER'
            ]
        );

        $this->belongsTo(
            'ParentItem', [
                'className' => 'Items',
                'through' => 'Items',
                'foreignKey' => 'item_id'
                // 'bindingKey' => 'item_id'
            ]
        );

        $this->belongsToMany(
            'ChildItems', [
                'className' => 'Items',
                'through' => 'Items',
                'foreignKey' => 'item_id',
                'targetForeignKey' => 'id',
            ]
        );
        $this->hasOne(
            'ItemCities', [
                'foreignKey' => 'item_id'
            ]
        );

        $this->hasOne(
            'ItemCitiesCopy', [
                'foreignKey' => 'item_id'
            ]
        );

        $this->hasOne(
            'ItemStocks', [
                'foreignKey' => 'item_id'
            ]
        );
        // $this->belongsToMany(
        //     'ChildItems', [
        //         'className' => 'Items',
        //         'through' => 'Items',
        //         'foreignKey' => 'item_id',
        //         'targetForeignKey' => 'id',
        //     ]
        // );


        // $this->belongsTo(
        //     'ItemCategories', [
        //         'foreignKey' => 'item_category_id',
        //         'joinType' => 'INNER'
        //     ]
        // );


        //Product Page
        // $this->hasMany(
        //     'ItemGalleryPhotos', [
        //         'foreignKey' => 'item_id',
        //         'joinType' => 'INNER'
        //     ]
        // );
        // $this->hasMany(
        //     'ItemDescriptionPhotos', [
        //         'foreignKey' => 'item_id',
        //         'joinType' => 'INNER'
        //     ]
        // );

    }
}
