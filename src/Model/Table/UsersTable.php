<?php
namespace App\Model\Table;

use Cake\Utility\Security;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\EventManager;
use Cake\ORM\TableRegistry;

/**
*   Users Model
*
*/
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp', [
                'events' => [
                    'Model.beforeSave' => [
                        'created' => 'new',
                        'modified' => 'always'
                    ]
                ]
        ]);
        //Relations goes here....
        //which is hasMany, belongsTo & many more
    }

    public function beforeSave(Event $event)
    {
       $entity = $event->getData('entity');
		//Hash the password
        if ($entity->isNew() || isset($entity->password)) {
            // pr('in if');
            $entity->password = Security::hash($entity->password, 'sha1', true);
//       pr($entity);die();
            
        }
    //    pr($entity);die();
        
       // Make a password for digest auth.
       return true;
    }


    public function getUser(\Cake\Datasource\EntityInterface $profile) {
        // Make sure here that all the required fields are actually present
        if (empty($profile->email)) {
            throw new \RuntimeException('Could not find email in social profile.');
        }
        // $this->log("PROFILE:");
        // $this->log($profile);
        // Check if user with same email exists. This avoids creating multiple
        // user accounts for different social identities of same user. You should
        // probably skip this check if your system doesn't enforce unique email
        // per user.
        $user = $this->find()
            ->where(['username' => $profile->email])
            ->first();

        if ($user) {
            return $user;
        }

        // Create new user account
        $user = $this->newEntity(
            [
                'username' => $profile->email,
                'first_name' => $profile->first_name,
                'last_name' => $profile->last_name,
                'is_verified' => 1
            ]
        );
        $user = $this->save($user);

        if (!$user) {
            throw new \RuntimeException('Unable to save new user');
        }

        return $user;
    }


}
?>
