<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
*   ItemCitiesCopy Model
*/
class ItemCitiesCopyTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('item_cities_copy');
        $this->primaryKey('id');

        $this->addBehavior(
            'Timestamp', [
                'events' => [
                    'Model.beforeSave' => [
                        'created' => 'new',
                        'modified' => 'always'
                    ]
                ]
            ]
        );
        //Relations goes here....
        $this->belongsTo(
            'Items', [
                'foreignKey' => 'item_id',
                'joinType' => 'INNER'
            ]
        );

        $this->belongsTo(
            'Cities', [
                'foreignKey' => 'city_id',
                'joinType' => 'INNER'
            ]
        );

        $this->hasOne(
            'ItemStocks', [
                'foreignKey' => 'item_id', //ItemCities
                'bindingKey' => 'item_id'//ItemStocks
            ]
        );


        // $this->belongsToMany(
        //     'ChildItems', [
        //         'className' => 'Items',
        //         'through' => 'Items',
        //         'foreignKey' => 'item_id',
        //         'targetForeignKey' => 'id',
        //     ]
        // );


        // $this->belongsTo(
        //     'ItemCategories', [
        //         'foreignKey' => 'item_category_id',
        //         'joinType' => 'INNER'
        //     ]
        // );


        //Product Page
        // $this->hasMany(
        //     'ItemGalleryPhotos', [
        //         'foreignKey' => 'item_id',
        //         'joinType' => 'INNER'
        //     ]
        // );
        // $this->hasMany(
        //     'ItemDescriptionPhotos', [
        //         'foreignKey' => 'item_id',
        //         'joinType' => 'INNER'
        //     ]
        // );

    }
}
