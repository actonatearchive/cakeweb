<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
*   Orders Model
*/
class OrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders');
        $this->primaryKey('id');

        $this->addBehavior(
            'Timestamp', [
                'events' => [
                    'Model.beforeSave' => [
                        'created' => 'new',
                        'modified' => 'always'
                    ]
                ]
            ]
        );
        //Relations goes here....
        $this->belongsTo(
            'ShippingAddresses', [
                'className' => 'UserAddresses',
                'targetForeignKey' => 'id',
                'foreignKey' => 'shipping_address_id',
                'joinType' => 'INNER'
            ]
        );
        $this->belongsTo(
            'Users', [
                'foreignKey' => 'user_id',
                'joinType' => 'INNER'
            ]
        );

        $this->hasMany(
           'OrderItems', [
               'foreignKey' => 'order_id',
               'joinType' => 'INNER'
           ]
       );
    }
}
