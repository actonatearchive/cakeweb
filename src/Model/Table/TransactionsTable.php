<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
*   Transactions Model
*/
class TransactionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('transactions');
        $this->primaryKey('id');

        $this->addBehavior(
            'Timestamp', [
                'events' => [
                    'Model.beforeSave' => [
                        'created' => 'new',
                        'modified' => 'always'
                    ]
                ]
            ]
        );
        //Relations goes here....
        // $this->belongsTo(
        //     'Users', [
        //         'foreignKey' => 'user_id',
        //         'joinType' => 'INNER'
        //     ]
        // );

    }
}
